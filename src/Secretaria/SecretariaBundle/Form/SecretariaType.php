<?php

namespace Secretaria\SecretariaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SecretariaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('municipio')
            ->add('departamento')
            ->add('nombre')
            ->add('secretariaGeneral')
            ->add('descripcion')
            ->add('encargado')
            ->add('status')
            ->add('secretaria')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Secretaria\SecretariaBundle\Entity\Secretaria'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'secretaria_secretariabundle_secretaria';
    }
}
