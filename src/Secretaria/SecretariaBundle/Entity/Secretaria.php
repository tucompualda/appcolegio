<?php

namespace Secretaria\SecretariaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Secretaria
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Secretaria\SecretariaBundle\Entity\SecretariaRepository")
 */
class Secretaria
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="municipio", type="string", length=255)
     */
    private $municipio;

    /**
     * @var string
     *
     * @ORM\Column(name="departamento", type="string", length=255)
     */
    private $departamento;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="encargado", type="string", length=255)
     */
    private $encargado;
    
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Secretaria\SecretariaBundle\Entity\Secretaria")
     */
    private $secretaria;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Secretaria\SecretariaBundle\Entity\SecretariaGeneral")
     */
    private $secretariaGeneral;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set municipio
     *
     * @param string $municipio
     * @return Secretaria
     */
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;
    
        return $this;
    }

    /**
     * Get municipio
     *
     * @return string 
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Set departamento
     *
     * @param string $departamento
     * @return Secretaria
     */
    public function setDepartamento($departamento)
    {
        $this->departamento = $departamento;
    
        return $this;
    }

    /**
     * Get departamento
     *
     * @return string 
     */
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Secretaria
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    
    
    /**
     * Set encargado
     *
     * @param string $encargado
     * @return Secretaria
     */
    public function setEncargado($encargado)
    {
        $this->encargado = $encargado;
    
        return $this;
    }

    /**
     * Get encargado
     *
     * @return string 
     */
    public function getEncargado()
    {
        return $this->encargado;
    }

    /**
     * Set secretaria
     *
     * @param string $secretaria
     * @return Secretaria
     */
    public function setSecretaria(\Secretaria\SecretariaBundle\Entity\Secretaria $secretaria)
    {
        $this->secretaria = $secretaria;
    
        return $this;
    }

    /**
     * Get secretaria
     *
     * @return string 
     */
    public function getSecretaria()
    {
        return $this->secretaria;
    }

    /**
     * Set secretariaGeneral
     *
     * @param string $secretariaGeneral
     * @return Secretaria
     */
    public function setSecretariaGeneral(\Secretaria\SecretariaBundle\Entity\SecretariaGeneral $secretariaGeneral)
    {
        $this->secretariaGeneral = $secretariaGeneral;
    
        return $this;
    }

    /**
     * Get secretariaGeneral
     *
     * @return string 
     */
    public function getSecretariaGeneral()
    {
        return $this->secretariaGeneral;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Secretaria
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Secretaria
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    public function __toString() {
        return $this->getNombre();
    }
}
