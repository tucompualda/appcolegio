<?php

namespace Secretaria\SecretariaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SecretariaGeneral
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Secretaria\SecretariaBundle\Entity\SecretariaGeneralRepository")
 */
class SecretariaGeneral
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="encargado", type="string", length=255)
     */
    private $encargado;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return SecretariaGeneral
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return SecretariaGeneral
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set encargado
     *
     * @param string $encargado
     * @return SecretariaGeneral
     */
    public function setEncargado($encargado)
    {
        $this->encargado = $encargado;
    
        return $this;
    }

    /**
     * Get encargado
     *
     * @return string 
     */
    public function getEncargado()
    {
        return $this->encargado;
    }
    
    public function __toString() {
        return $this->getNombre();
    }
}
