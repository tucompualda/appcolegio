<?php

namespace Secretaria\SecretariaBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * SecretariaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SecretariaRepository extends EntityRepository
{
}
