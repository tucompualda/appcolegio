<?php

namespace Secretaria\SecretariaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Secretaria\SecretariaBundle\Entity\SecretariaGeneral;
use Secretaria\SecretariaBundle\Form\SecretariaGeneralType;

/**
 * SecretariaGeneral controller.
 *
 */
class SecretariaGeneralController extends Controller
{

    /**
     * Lists all SecretariaGeneral entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SecretariaBundle:SecretariaGeneral')->findAll();

        return $this->render('SecretariaBundle:SecretariaGeneral:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new SecretariaGeneral entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new SecretariaGeneral();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('secretariageneral_show', array('id' => $entity->getId())));
        }

        return $this->render('SecretariaBundle:SecretariaGeneral:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a SecretariaGeneral entity.
    *
    * @param SecretariaGeneral $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(SecretariaGeneral $entity)
    {
        $form = $this->createForm(new SecretariaGeneralType(), $entity, array(
            'action' => $this->generateUrl('secretariageneral_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new SecretariaGeneral entity.
     *
     */
    public function newAction()
    {
        $entity = new SecretariaGeneral();
        $form   = $this->createCreateForm($entity);

        return $this->render('SecretariaBundle:SecretariaGeneral:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a SecretariaGeneral entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SecretariaBundle:SecretariaGeneral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SecretariaGeneral entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SecretariaBundle:SecretariaGeneral:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing SecretariaGeneral entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SecretariaBundle:SecretariaGeneral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SecretariaGeneral entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SecretariaBundle:SecretariaGeneral:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a SecretariaGeneral entity.
    *
    * @param SecretariaGeneral $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(SecretariaGeneral $entity)
    {
        $form = $this->createForm(new SecretariaGeneralType(), $entity, array(
            'action' => $this->generateUrl('secretariageneral_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing SecretariaGeneral entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SecretariaBundle:SecretariaGeneral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SecretariaGeneral entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('secretariageneral_edit', array('id' => $id)));
        }

        return $this->render('SecretariaBundle:SecretariaGeneral:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a SecretariaGeneral entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SecretariaBundle:SecretariaGeneral')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SecretariaGeneral entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('secretariageneral'));
    }

    /**
     * Creates a form to delete a SecretariaGeneral entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('secretariageneral_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
