<?php

namespace Secretaria\SecretariaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Secretaria\SecretariaBundle\Entity\Secretaria;
use Secretaria\SecretariaBundle\Form\SecretariaType;

/**
 * Secretaria controller.
 *
 */
class SecretariaController extends Controller
{

    /**
     * Lists all Secretaria entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SecretariaBundle:Secretaria')->findAll();

        return $this->render('SecretariaBundle:Secretaria:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Secretaria entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Secretaria();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('secretaria_show', array('id' => $entity->getId())));
        }

        return $this->render('SecretariaBundle:Secretaria:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Secretaria entity.
    *
    * @param Secretaria $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Secretaria $entity)
    {
        $form = $this->createForm(new SecretariaType(), $entity, array(
            'action' => $this->generateUrl('secretaria_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Secretaria entity.
     *
     */
    public function newAction()
    {
        $entity = new Secretaria();
        $form   = $this->createCreateForm($entity);

        return $this->render('SecretariaBundle:Secretaria:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Secretaria entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SecretariaBundle:Secretaria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Secretaria entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SecretariaBundle:Secretaria:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Secretaria entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SecretariaBundle:Secretaria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Secretaria entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SecretariaBundle:Secretaria:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Secretaria entity.
    *
    * @param Secretaria $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Secretaria $entity)
    {
        $form = $this->createForm(new SecretariaType(), $entity, array(
            'action' => $this->generateUrl('secretaria_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Secretaria entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SecretariaBundle:Secretaria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Secretaria entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('secretaria_edit', array('id' => $id)));
        }

        return $this->render('SecretariaBundle:Secretaria:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Secretaria entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SecretariaBundle:Secretaria')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Secretaria entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('secretaria'));
    }

    /**
     * Creates a form to delete a Secretaria entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('secretaria_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
