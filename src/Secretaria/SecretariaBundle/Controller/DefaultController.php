<?php

namespace Secretaria\SecretariaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('SecretariaBundle:Default:index.html.twig');
    }
}
