<?php

namespace Colegio\DocenteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class NotaLogroType extends AbstractType
{
	public function __construct($id)
	{
		$this->id = $id;	
	}
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$self = $this; 
        $builder
            ->add('calificacion')
            ->add('descripcion')
            ->add('matricula','entity',array(
                'class' => 'ColegioEstudianteBundle:GrupoEstudiante',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('u')
                                ->where('u.id = :id')
                                ->setParameter('id', $self->id);
                },
                 'label'=>'Matricula',
                 'required'=> true,
            ))
            ->add('logro')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\EstudianteBundle\Entity\NotaLogro'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_estudiantebundle_notalogro';
    }
}
