<?php

namespace Colegio\DocenteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ObservacionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('periodo')
            //->add('estudiante')
            ->add('calificacion','number',array(
                'label'=>'Calificación de Comportamiento',
                'attr'=>array('class'=>'form-control','placeholder'=>'Escriba un número de 0 a 5')
            )) 
            ->add('descripcion','textarea', array(
                    'required'=>true,
                    'attr' => array(
                        'class' => 'tinymce',
                        'data-theme' => 'simple', 
                            ),
                    
                    )
                ) 
                        
            //->add('fechaCreacion')
            //->add('fechaModificacion')
            //->add('creador')
            ->add('estado',null,array('required'=>false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\DocenteBundle\Entity\Observacion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_docentebundle_observacion';
    }
}
