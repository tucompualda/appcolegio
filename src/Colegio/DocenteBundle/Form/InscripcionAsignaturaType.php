<?php

namespace Colegio\DocenteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class InscripcionAsignaturaType extends AbstractType
{
	public function __construct($matricula,$asignatura)
	{
		$this->matricula = $matricula;
		$this->asignatura = $asignatura;
	}
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$self = $this;
        $builder
           // ->add('fechaInscripcion')
            ->add('matricula','entity',array(
                'class' => 'ColegioEstudianteBundle:GrupoEstudiante',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('u')
                                ->where('u.id = :matricula')
                                ->setParameter('matricula', $self->matricula);
                },
                 'label'=>'Matricula',
                 'required'=> true,
            ))
            ->add('grupoAsignatura','entity',array(
                'class' => 'ColegioGrupoBundle:GrupoAsignatura',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('u')
                                ->where('u.id = :asignatura')
                                ->setParameter('asignatura', $self->asignatura);
                },
                 'label'=>'Grupo Asignatura',
                 'required'=> true,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\BoletinBundle\Entity\InscripcionAsignatura'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_boletinbundle_inscripcionasignatura';
    }
}
