<?php

namespace Colegio\DocenteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class TrabajosLogrosType extends AbstractType
{
	public function __construct($docente,$colegio,$grupoAsignatura,$periodo,$asignatura)
	{
	        $this->docente = $docente;
            $this->colegio = $colegio;
            $this->grupoAsignatura = $grupoAsignatura;
            $this->periodo = $periodo;
            $this->asignatura = $asignatura;
            $this->hoy = new \DateTime('now');  
	}
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$self = $this;
        $builder
            ->add('estado',null,array(
            		'required'=>false,
            		'label'=>'Publica tu Trabajo',
            		'attr'=>array('class'=>'checkbox'),
            ))
            ->add('tipoTrabajo','entity',array(
        		'class'=>'ColegioDocenteBundle:PorcentajeAsignaturaTrabajo',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('p')
                                ->join('p.grupoAsignatura','g')
                                ->where('p.grupoAsignatura = :grupoAsignatura AND p.periodo = :periodo AND g.colegio = :colegio')
                                ->setParameter('grupoAsignatura', $self->grupoAsignatura)
                                ->setParameter('periodo', $self->periodo)
                                ->setParameter('colegio', $self->colegio);
                               
                },
                 'label'=>'Tipo Trabajo',
                 'empty_value'=>'Escoge ...',
                 'required'=> true,
                    'attr'=>array('class'=>'form-control')
            ))
          
            ->add('fechaEntrega',null,array(
                'attr'=>array('class'=>'checkbox'),
            ))
            ->add('detalle','textarea', array(
            		'required'=>true,
            		'attr' => array(
        				'class' => 'tinymce',
        				'data-theme' => 'advanced', 
    						),
            		
    				)
            	)
            ->add('logros','entity',array(
                'class'=>'ColegioBoletinBundle:Logro',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('l')
                                ->join('l.idBoletin','b') 
                                ->join('b.gruposBoletin','g')                               
                                ->where('b.colegio = :colegio AND b.periodo = :periodo AND g.grupoAsignatura = :asignatura')
                                ->setParameter('colegio', $self->colegio)
                                ->setParameter('periodo', $self->periodo)
                                ->setParameter('asignatura', $self->grupoAsignatura);//////////aca
                               
                },
                 'label'=>'Logro',
                 'empty_value'=>'Escoge ...',
                 'required'=> false,
                 'multiple'=> true,
                 'attr'=>array('class'=>'form-control')
            ))              
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\DocenteBundle\Entity\Trabajos'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_docentebundle_trabajos';
    }
}
