<?php

namespace Colegio\DocenteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PorcentajeAsignaturaTrabajoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nuevoPorcentaje')
            ->add('estado')
            
            ->add('grupoAsignatura')
            ->add('porcentajeTrabajo')
            ->add('periodo')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\DocenteBundle\Entity\PorcentajeAsignaturaTrabajo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_docentebundle_porcentajeasignaturatrabajo';
    }
}
