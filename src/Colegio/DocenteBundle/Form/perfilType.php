<?php

namespace Colegio\DocenteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class PerfilType extends AbstractType
{

     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
            //->add('numeroDocumento')
        $builder
            ->add('telefono',null,array(
                'attr'=>array('class'=>'form-control'),
                'label'=>'Teléfono'
            ))
            //->add('email')
            ->add('email2','email',array(
                    'label'=>'Email para notificaciones',
                    'required'=>true,
                    'attr'=>array('type'=>'email','class'=>'form-control'),

                ))
            ->add('numeroIdentificacion',null,array(
                    'attr'=>array('type'=>'number','class'=>'form-control'),
                    'label'=>'Número de identificación'
                ))
            //->add('nuip')
            
            ->add('password','password',array(
                    'required'=>false,
                    'attr'=>array('class'=>'form-control','placeholder'=>'Deje vacío sino quiere modificar la contraseña...')
                    ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\DocenteBundle\Entity\Docente'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_docentebundle_perfil';
    }
}
