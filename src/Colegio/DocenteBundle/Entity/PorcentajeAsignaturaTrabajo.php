<?php

namespace Colegio\DocenteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PorcentajeAsignaturaTrabajo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\DocenteBundle\Entity\PorcentajeAsignaturaTrabajoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PorcentajeAsignaturaTrabajo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\GrupoBundle\Entity\GrupoAsignatura")
     */
    private $grupoAsignatura;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\PorcentajeTrabajo")
     */
    private $porcentajeTrabajo;

    /**
     * @var float
     *
     * @ORM\Column(name="nuevoPorcentaje", type="float")
     */
    private $nuevoPorcentaje;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaCreacion", type="datetime", nullable=true)
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaModificacion", type="datetime", nullable=true)
     */
    private $fechaModificacion;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\BoletinBundle\Entity\Periodo")
     */
    private $periodo;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set grupoAsignatura
     *
     * @param string $grupoAsignatura
     * @return PorcentajeAsignaturaTrabajo
     */
    public function setGrupoAsignatura(\Colegio\GrupoBundle\Entity\GrupoAsignatura $grupoAsignatura)
    {
        $this->grupoAsignatura = $grupoAsignatura;
    
        return $this;
    }

    /**
     * Get grupoAsignatura
     *
     * @return string 
     */
    public function getGrupoAsignatura()
    {
        return $this->grupoAsignatura;
    }

    /**
     * Set porcentajeTrabajo
     *
     * @param string $porcentajeTrabajo
     * @return PorcentajeAsignaturaTrabajo
     */
    public function setPorcentajeTrabajo(\Colegio\AdminBundle\Entity\PorcentajeTrabajo $porcentajeTrabajo)
    {
        $this->porcentajeTrabajo = $porcentajeTrabajo;
    
        return $this;
    }

    /**
     * Get porcentajeTrabajo
     *
     * @return string 
     */
    public function getPorcentajeTrabajo()
    {
        return $this->porcentajeTrabajo;
    }

    /**
     * Set nuevoPorcentaje
     *
     * @param float $nuevoPorcentaje
     * @return PorcentajeAsignaturaTrabajo
     */
    public function setNuevoPorcentaje($nuevoPorcentaje)
    {
        $this->nuevoPorcentaje = $nuevoPorcentaje;
    
        return $this;
    }

    /**
     * Get nuevoPorcentaje
     *
     * @return float 
     */
    public function getNuevoPorcentaje()
    {
        return $this->nuevoPorcentaje;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return PorcentajeAsignaturaTrabajo
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return PorcentajeAsignaturaTrabajo
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;
    
        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     * @return PorcentajeAsignaturaTrabajo
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;
    
        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime 
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function setCreatedValue()
    {
        $this->fechaCreacion = new \DateTime();
        $this->fechaModificacion = new \DateTime();
    }
    
    /** 
     * @ORM\PreUpdate 
     */  
    public function setUpdatedAt()  
    {  
        $this->fechaModificacion = new \DateTime();  
    }  

    /**
     * Set periodo
     *
     * @param \Colegio\BoletinBundle\Entity\Periodo $periodo
     * @return PorcentajeAsignaturaTrabajo
     */
    public function setPeriodo(\Colegio\BoletinBundle\Entity\Periodo $periodo = null)
    {
        $this->periodo = $periodo;
    
        return $this;
    }

    /**
     * Get periodo
     *
     * @return \Colegio\BoletinBundle\Entity\Periodo 
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }
    
    public function __toString()
    {
        return $this->getPorcentajeTrabajo()->getTipoTrabajo()->getDetalle();
    }
}