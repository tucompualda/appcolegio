<?php

namespace Colegio\DocenteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Trabajos
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\DocenteBundle\Entity\TrabajosRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Trabajos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Colegio\GrupoBundle\Entity\GrupoAsignatura", inversedBy="trabajos", cascade={"persist"})
     * @ORM\JoinColumn(name="grupoasignatura_id", referencedColumnName="id")
     */
    private $grupoAsignatura;

     /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Colegio\BoletinBundle\Entity\Periodo")
     */
    private $periodo;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaedicion", type="datetime")
     */
    private $fechaEdicion;    
    
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaentrega", type="datetime")
     */
    private $fechaEntrega;

    /**
     * @var string
     * @Assert\NotNull(message="No debe estar vacío el mensaje de la tarea")
     * @ORM\Column(name="detalle", type="text")
     */
    private $detalle;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="Colegio\DocenteBundle\Entity\PorcentajeAsignaturaTrabajo")
     */
    private $tipoTrabajo;
    

     /**
     * @ORM\OneToMany(targetEntity="Colegio\DocenteBundle\Entity\NotaTrabajo", mappedBy="trabajo", cascade={"persist","remove"})
     * @Assert\Valid()    
     */
    private $notasTrabajos;

    /**
     * @ORM\ManyToMany(targetEntity="Colegio\BoletinBundle\Entity\Logro", inversedBy="trabajos")
     * @ORM\JoinTable(name="trabajos_logros",
     *          joinColumns={@ORM\JoinColumn(name="trabajos_id", referencedColumnName="id")},
     *          inverseJoinColumns={@ORM\JoinColumn(name="logro_id", referencedColumnName="id")}
     *  )*/
    private $logros;

     /**
     * @ORM\OneToMany(targetEntity="Colegio\EstudianteBundle\Entity\TrabajoMatricula", mappedBy="trabajo", cascade={"persist","remove"})
     * @Assert\Valid()    
     */
    private $trabajoMatriculas;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notasTrabajos = new ArrayCollection();
        $this->logros = new ArrayCollection();
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set grupoAsignatura
     *
     * @param \Colegio\GrupoBundle\Entity\GrupoAsignatura $grupoAsignatura
     * @return Trabajos
     */
    public function setGrupoAsignatura(\Colegio\GrupoBundle\Entity\GrupoAsignatura $grupoAsignatura)
    {
        $this->grupoAsignatura = $grupoAsignatura;
    
        return $this;
    }

    /**
     * Get grupoAsignatura
     *
     * @return \Colegio\GrupoBundle\Entity\GrupoAsignatura
     */
    public function getGrupoAsignatura()
    {
        return $this->grupoAsignatura;
    }
    
    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Trabajos
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set fechaEntrega
     *
     * @param \DateTime $fechaEntrega
     * @return Trabajos
     */
    public function setFechaEntrega($fechaEntrega)
    {
        $this->fechaEntrega = $fechaEntrega;
    
        return $this;
    }

    /**
     * Get fechaEntrega
     *
     * @return \DateTime 
     */
    public function getFechaEntrega()
    {
        return $this->fechaEntrega;
    }

    /**
     * Set detalle
     *
     * @param string $detalle
     * @return Trabajos
     */
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;
    
        return $this;
    }

    /**
     * Get detalle
     *
     * @return string 
     */
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return Trabajos
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Set tipoTrabajo
     *
     * @param \Colegio\DocenteBundle\Entity\PorcentajeAsignaturaTrabajo $tipoTrabajo
     * @return Trabajos
     */
    public function setTipoTrabajo(\Colegio\DocenteBundle\Entity\PorcentajeAsignaturaTrabajo $tipoTrabajo)
    {
    	$this->tipoTrabajo = $tipoTrabajo;
    
    	return $this;
    }
    
    /**
     * Get tipoTrabajo
     *
     * @return \Colegio\AdminBundle\Entity\PorcentajeTrabajo
     */
    public function getTipoTrabajo()
    {
    	return $this->tipoTrabajo;
    }
    
    
    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set periodo
     *
     * @param \Colegio\BoletinBundle\Entity\Periodo $periodo
     * @return Trabajos
     */
    public function setPeriodo(\Colegio\BoletinBundle\Entity\Periodo $periodo = null)
    {
        $this->periodo = $periodo;
    
        return $this;
    }

    /**
     * Get periodo
     *
     * @return \Colegio\BoletinBundle\Entity\Periodo 
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }

    /**
     * Set fechaEdicion
     *
     * @param \DateTime $fechaEdicion
     * @return Trabajos
     */
    public function setFechaEdicion($fechaEdicion)
    {
        $this->fechaEdicion = $fechaEdicion;
    
        return $this;
    }

    /**
     * Get fechaEdicion
     *
     * @return \DateTime 
     */
    public function getFechaEdicion()
    {
        return $this->fechaEdicion;
    }
    
 
    /**
     * Add notasTrabajos
     *
     * @param \Colegio\DocenteBundle\Entity\NotaTrabajo $notasTrabajos
     * @return Trabajos
     */
    public function addNotasTrabajo(\Colegio\DocenteBundle\Entity\NotaTrabajo $notasTrabajos)
    {
        $this->notasTrabajos[] = $notasTrabajos;
    
        return $this;
    }

    /**
     * Remove notasTrabajos
     *
     * @param \Colegio\DocenteBundle\Entity\NotaTrabajo $notasTrabajos
     */
    public function removeNotasTrabajo(\Colegio\DocenteBundle\Entity\NotaTrabajo $notasTrabajos)
    {
        $this->notasTrabajos->removeElement($notasTrabajos);
    }

    /**
     * Get notasTrabajos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotasTrabajos()
    {
        return $this->notasTrabajos;
    }

    /**
     * Add logros
     *
     * @param \Colegio\BoletinBundle\Entity\Logro $logros
     * @return Trabajos
     */
    public function addLogro(\Colegio\BoletinBundle\Entity\Logro $logros)
    {
        $this->logros[] = $logros;
    
        return $this;
    }

    /**
     * Remove logros
     *
     * @param \Colegio\BoletinBundle\Entity\Logro $logros
     */
    public function removeLogro(\Colegio\BoletinBundle\Entity\Logro $logros)
    {
        $this->logros->removeElement($logros);
    }

    /**
     * Get logros
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLogros()
    {
        return $this->logros;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedValue()
    {
        $this->fecha = new \DateTime("now");
        $this->fechaEdicion = new \DateTime("now");
    }
    
    /** 
     * @ORM\PreUpdate 
     */  
    public function setUpdatedAt()  
    {  
        $this->fechaEdicion = new \DateTime("now");  
    }  

    public function __toString()
    {
        return $this->getDetalle();
    }


    /**
     * Add trabajoMatriculas
     *
     * @param \Colegio\EstudianteBundle\Entity\TrabajoMatricula $trabajoMatriculas
     * @return Trabajos
     */
    public function addTrabajoMatricula(\Colegio\EstudianteBundle\Entity\TrabajoMatricula $trabajoMatriculas)
    {
        $this->trabajoMatriculas[] = $trabajoMatriculas;
    
        return $this;
    }

    /**
     * Remove trabajoMatriculas
     *
     * @param \Colegio\EstudianteBundle\Entity\TrabajoMatricula $trabajoMatriculas
     */
    public function removeTrabajoMatricula(\Colegio\EstudianteBundle\Entity\TrabajoMatricula $trabajoMatriculas)
    {
        $this->trabajoMatriculas->removeElement($trabajoMatriculas);
    }

    /**
     * Get trabajoMatriculas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTrabajoMatriculas()
    {
        return $this->trabajoMatriculas;
    }
}