<?php

namespace Colegio\DocenteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NotaTrabajo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\DocenteBundle\Entity\NotaTrabajoRepository")
 */
class NotaTrabajo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\DocenteBundle\Entity\Trabajos", inversedBy="notasTrabajos")
     */
    private $trabajo;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\BoletinBundle\Entity\InscripcionAsignatura")
     */
    private $inscripcionAsignatura;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaCalificacion", type="datetime")
     */
    private $fechaCalificacion;

    /**
     * @var string
     *
     * @ORM\Column(name="calificacion", type="string", length=255)
     */
    private $calificacion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set trabajo
     *
     * @param \Colegio\DocenteBundle\Entity\Trabajos $trabajo
     * @return NotaTrabajo
     */
    public function setTrabajo(\Colegio\DocenteBundle\Entity\Trabajos $trabajo)
    {
        $this->trabajo = $trabajo;
    
        return $this;
    }

    /**
     * Get trabajo
     *
     * @return Colegio\DocenteBundle\Entity\Trabajos
     */
    public function getTrabajo()
    {
        return $this->trabajo;
    }

    /**
     * Set inscripcionAsignatura
     *
     * @param \Colegio\BoletinBundle\Entity\InscripcionAsignatura $inscripcionAsignatura
     * @return NotaTrabajo
     */
    public function setInscripcionAsignatura(\Colegio\BoletinBundle\Entity\InscripcionAsignatura $inscripcionAsignatura)
    {
        $this->inscripcionAsignatura = $inscripcionAsignatura;
    
        return $this;
    }

    /**
     * Get inscripcionAsignatura
     *
     * @return \Colegio\BoletinBundle\Entity\InscripcionAsignatura 
     */
    public function getInscripcionAsignatura()
    {
        return $this->inscripcionAsignatura;
    }

    /**
     * Set fechaCalificacion
     *
     * @param \DateTime $fechaCalificacion
     * @return NotaTrabajo
     */
    public function setFechaCalificacion($fechaCalificacion)
    {
        $this->fechaCalificacion = $fechaCalificacion;
    
        return $this;
    }

    /**
     * Get fechaCalificacion
     *
     * @return \DateTime 
     */
    public function getFechaCalificacion()
    {
        return $this->fechaCalificacion;
    }

    /**
     * Set calificacion
     *
     * @param string $calificacion
     * @return NotaTrabajo
     */
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;
    
        return $this;
    }

    /**
     * Get calificacion
     *
     * @return string 
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return NotaTrabajo
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    public function __toString()
    {
        return $this->getTrabajo();
    }
}