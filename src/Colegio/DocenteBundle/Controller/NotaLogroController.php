<?php

namespace Colegio\DocenteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\EstudianteBundle\Entity\NotaLogro;
use Colegio\DocenteBundle\Form\NotaLogroType;

/**
 * NotaLogro controller.
 *
 */
class NotaLogroController extends Controller
{

    /**
     * Lists all NotaLogro entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ColegioEstudianteBundle:NotaLogro')->findAll();

        return $this->render('ColegioDocenteBundle:NotaLogro:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new NotaLogro entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new NotaLogro();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('notadocentelogro_show', array('id' => $entity->getId())));
        }

        return $this->render('ColegioDocenteBundle:NotaLogro:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a NotaLogro entity.
    *
    * @param NotaLogro $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(NotaLogro $entity)
    {
        $form = $this->createForm(new NotaLogroType(), $entity, array(
            'action' => $this->generateUrl('notadocentelogro_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new NotaLogro entity.
     *
     */
    public function newAction()
    {
        $entity = new NotaLogro();
        $form   = $this->createCreateForm($entity);

        return $this->render('ColegioDocenteBundle:NotaLogro:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a NotaLogro entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioEstudianteBundle:NotaLogro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NotaLogro entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioDocenteBundle:NotaLogro:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing NotaLogro entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioEstudianteBundle:NotaLogro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NotaLogro entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioDocenteBundle:NotaLogro:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a NotaLogro entity.
    *
    * @param NotaLogro $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(NotaLogro $entity)
    {
        $form = $this->createForm(new NotaLogroType(), $entity, array(
            'action' => $this->generateUrl('notadocentelogro_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing NotaLogro entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioEstudianteBundle:NotaLogro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NotaLogro entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('notadocentelogro_edit', array('id' => $id)));
        }

        return $this->render('ColegioDocenteBundle:NotaLogro:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a NotaLogro entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioEstudianteBundle:NotaLogro')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find NotaLogro entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('notadocentelogro'));
    }

    /**
     * Creates a form to delete a NotaLogro entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('notadocentelogro_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
