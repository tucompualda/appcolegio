<?php

namespace Colegio\DocenteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Colegio\BoletinBundle\Entity\Boletin;
use Colegio\BoletinBundle\Entity\GrupoBoletin;
use Colegio\BoletinBundle\Entity\Logro;
use Colegio\EstudianteBundle\Entity\Nota;
use Colegio\EstudianteBundle\Entity\NotaLogro;
use Colegio\DocenteBundle\Entity\Observacion;

class WebServiceController extends Controller
{
 	public function boletinAction($asignatura,$periodo,$grupoAsignatura,$descripcion)
 	{

 		 $em = $this->getDoctrine()->getManager();
 		 $usuarioActivo = $this->obtenerUsuario();
 		 $colegio = $this->obtenerColegio($usuarioActivo);
 		 
 		 $asignaturaReal = $em->getRepository('ColegioGrupoBundle:Asignatura')->find($asignatura);
 		 $periodoReal = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodo);
 		 $grupoAsignaturaReal = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->find($grupoAsignatura);

 		 $hoy = new \DateTime();


 		 $boletin = new Boletin();
 		 $boletin->setAsignatura($asignaturaReal);
		 $boletin->setDescripcion($descripcion);
		 $boletin->setFechaInicio($hoy);
		 $boletin->setFechaFin($hoy);
		 $boletin->setActivo(true);
		 $boletin->setColegio($colegio);
		 $boletin->setPeriodo($periodoReal);

		 $em->persist($boletin);
		 $em->flush();

		 $grupoBoletin = new GrupoBoletin();
		 $grupoBoletin->setBoletin($boletin);
		 $grupoBoletin->setGrupoAsignatura($grupoAsignaturaReal);
		 $grupoBoletin->setPeriodo($periodoReal);
		 $grupoBoletin->setColegio($colegio);

		 $em->persist($grupoBoletin);
		 $em->flush();

 		$this->get('session')->getFlashBag()->add(
            'notice',
            'Ahora puedes crear Logros en tu Boletin...');

 		 $respuesta = "Ok Creado";
 		 $response = new Response($respuesta);
       	 return $response;

 	}

 	public function logrosAction($boletin,$categoria,$descripcion)
 	{
 		 $em = $this->getDoctrine()->getManager();
 		 $boletinReal = $em->getRepository('ColegioBoletinBundle:Boletin')->find($boletin);
 		 $categoriaReal = $em->getRepository('ColegioBoletinBundle:CategoriaLogro')->find($categoria);

 		 $logros = new Logro();
 		 $logros->setIdBoletin($boletinReal);
 		 $logros->setCategoria($categoriaReal);
 		 $logros->setDescripcion($descripcion);
 		 
 		 $em->persist($logros);
 		 $em->flush();

 		 $respuesta = $logros->getId();
 		 $response = new Response($respuesta);
       	 return $response;
 	}


 	public function actualizarLogrosAction($logro,$descripcion,$categoria)
 	{

		$em = $this->getDoctrine()->getManager();

		 $usuarioActivo = $this->obtenerUsuario();
 		 $colegio = $this->obtenerColegio($usuarioActivo);

 		 $categoriaReal = $em->getRepository('ColegioBoletinBundle:CategoriaLogro')->find($categoria);


 		 $logroActualizar = $em->getRepository('ColegioBoletinBundle:Logro')->find($logro);
 		 $logroActualizar->setDescripcion($descripcion);
 		 $logroActualizar->setCategoria($categoriaReal);

 		 $em->flush();

 		 $this->get('session')->getFlashBag()->add(
            'notice',
            'Has actualizado el logro ...');

 		 $respuesta = "Actualizado";
 		 $response = new Response($respuesta);
       	 return $response;
 	}

 	public function actualizarNotaAction($grupoAsignatura,$matricula)
 	{

 		$em = $this->getDoctrine()->getManager();

		 $usuarioActivo = $this->obtenerUsuario();
 		 $colegio = $this->obtenerColegio($usuarioActivo);

 		 $periodo = $this->obtenerPeriodo($colegio);	

 		 $matriculaFin = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->find($matricula);

 		 $grupoBoletin = $em->getRepository('ColegioBoletinBundle:GrupoBoletin')->findOneBy(array(
 		 	'grupoAsignatura'=>$grupoAsignatura,
 		 	'periodo'=>$periodo,
 		 	'colegio'=>$colegio
 		 ));

 		 $notaFinalExistente = $em->getRepository('ColegioEstudianteBundle:Nota')->findOneBy(array(
 		 	'grupoBoletin'=>$grupoBoletin,
 		 	'matricula'=>$matricula
 		 ));

 		 $notasTrabajos = $em->getRepository('ColegioDocenteBundle:NotaTrabajo')->notasEstudiantePeriodo($matricula,$periodo,$grupoAsignatura);

 		 $porcentajestipoTrabajo = $em->getRepository('ColegioDocenteBundle:PorcentajeAsignaturaTrabajo')->findBy(array(
 		 	'periodo'         => $periodo,
 		 	'grupoAsignatura' => $grupoAsignatura
 		 )); 

		 
 		 
 		 $totalPorcentajes = 0;
 		 $notaN = 0;
			$notaNueva=0;
			$notaInicial=0;
			$nuevoPorcentaje=0;
			$promedio = 0;

 		 foreach ($porcentajestipoTrabajo as $key) {
 		 	# code...
 		 	$notaParcial = 0;
 		    $contador    = 0;
 		 	$totalPorcentajes = $totalPorcentajes + 1;
 		 	
 		 	$notasTrabajosPromedio = $em->getRepository('ColegioDocenteBundle:NotaTrabajo')->notasEstudiantePeriodoPromedio($matricula,$periodo,$grupoAsignatura,$key->getId());

 		 	foreach ($notasTrabajosPromedio as $notica) {
 		 		$notaPromedio = $notica['promedio'];
 		 		$nuevoPorcentaje = $key->getNuevoPorcentaje();	
 		 		$ponderado = $notaPromedio * ($nuevoPorcentaje/100);
 		 	}
 				

		 		$notaInicial = $notaInicial+$ponderado;	
 		 		

 		 }



 		 if(!$notaFinalExistente){
 		 	if($notaInicial>5){

 		 	}else{
			$hoy = new \DateTime('now');

 		 	$notaInsersion = new Nota();

 		 	$notaInsersion->setCalificacion($notaInicial);
 		 	
 		 	$notaInsersion->setDescripcion("");
 		 	$notaInsersion->setFecha($hoy);
 		 	$notaInsersion->setGrupoBoletin($grupoBoletin);
 		 	$notaInsersion->setMatricula($matriculaFin);
 		 	$notaInsersion->setFallas(0);

 		 	$em->persist($notaInsersion);
 		 	$em->flush();
 		 	}
 		 }else{
 		 	$notaFinalExistente->setCalificacion($notaInicial);
 		 	$em->flush();
 		 }

 		// $jsonp = new JsonResponse($contador);
        // return $jsonp;
 		 if($notaInicial>5){
			
 		 	$response = new Response("ERROR");
 		 }else{

		 	$notica = number_format($notaInicial,1,",",".");
 		 	$response = new Response($notica);
 		}

       	return $response;
 	}

 	public function calificarLogrosAction($matricula,$logro,$calificacion)
 	{
		$em = $this->getDoctrine()->getManager();

 		$logroReal = $em->getRepository('ColegioBoletinBundle:Logro')->find($logro);
 		$matriculaReal = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->find($matricula);

 		$notaLogro = $em->getRepository('ColegioEstudianteBundle:notaLogro')->findOneBy(array(
 			'matricula'=>$matricula,
 			'logro'=>$logro
 		));


 		if($notaLogro==null){
 			//insertamos
 						    $notalogro = new NotaLogro();
                            $notalogro->setCalificacion($calificacion);
                            $notalogro->setDescripcion('--');
                            $notalogro->setMatricula($matriculaReal);
                            $notalogro->setLogro($logroReal);

                            $em->persist($notalogro);
                            $em->flush();
 		}else{
 			//actualizamos
 			$notaLogro->setCalificacion($calificacion);
 			$em->flush();
 		}

 		$response = new Response("ok");
 		return $response;
 	}

 	public function fortalezaAction($matricula,$logro,$fortaleza)
 	{
		$em = $this->getDoctrine()->getManager();

 		$logroReal = $em->getRepository('ColegioBoletinBundle:Logro')->find($logro);
 		$matriculaReal = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->find($matricula);

 		$notaLogro = $em->getRepository('ColegioEstudianteBundle:notaLogro')->findOneBy(array(
 			'matricula'=>$matricula,
 			'logro'=>$logro
 		));


 		if($notaLogro==null){
 			//insertamos
			$notalogro = new NotaLogro();
            $notalogro->setFortaleza($fortaleza);
            $notalogro->setCalificacion(0);
            $notalogro->setDescripcion('--');
            $notalogro->setMatricula($matriculaReal);
            $notalogro->setLogro($logroReal);

            $em->persist($notalogro);
            $em->flush();
 		}else{
 			//actualizamos
 			$notaLogro->setFortaleza($fortaleza);
 			$em->flush();
 		}

 		$response = new Response("ok");
 		return $response;
 	}

    public function debilidadAction($matricula,$logro,$debilidad)
 	{
		$em = $this->getDoctrine()->getManager();

 		$logroReal = $em->getRepository('ColegioBoletinBundle:Logro')->find($logro);
 		$matriculaReal = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->find($matricula);

 		$notaLogro = $em->getRepository('ColegioEstudianteBundle:notaLogro')->findOneBy(array(
 			'matricula'=>$matricula,
 			'logro'=>$logro
 		));


 		if($notaLogro==null){
 			//insertamos
 						    $notalogro = new NotaLogro();
                            $notalogro->setDebilidad($debilidad);
                            $notalogro->setDescripcion('--');
                            $notalogro->setMatricula($matriculaReal);
                            $notalogro->setLogro($logroReal);
				            $notalogro->setCalificacion(0);

                            $em->persist($notalogro);
                            $em->flush();
 		}else{
 			//actualizamos
 			$notaLogro->setDebilidad($debilidad);
 			$em->flush();
 		}

 		$response = new Response("ok");
 		return $response;
 	}

    public function fallasAction($idNota,$matricula,$grupoBoletin,$fallas)
    {
        $em = $this->getDoctrine()->getManager();
        if($idNota == null){
        $notaFinal = $em->getRepository('ColegioEstudianteBundle:Nota')->findBy(array(
            'matricula'=>$matricula,
            'grupoBoletin'=>$grupoBoletin,
            'fallas'=>$fallas
        ));
        }
        else
        {
            $notaFinal = $em->getRepository('ColegioEstudianteBundle:Nota')->find($idNota);
        }

        $notaFinal->setFallas($fallas);
        $em->flush();
        
        $response = new Response("Fallas actualizada");
        return $response;
    }

    public function notaObservacionAction($observacionID,$calificacion)
    {
        $observaciones = new Observacion();
        $em = $this->getDoctrine()->getManager();
        $observaciones = $em->getRepository('ColegioDocenteBundle:Observacion')->find($observacionID);
        $observaciones->setCalificacion($calificacion);
        $em->flush();
        $response = new Response("Calificacion ok");
        return $response;

    }


 	public function obtenerPeriodo($idColegio)
 	{
		$em = $this->getDoctrine()->getManager();
		$hoy = new \DateTime('now');
        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
        
        if($periodo==null){
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Solicita por favor que habiliten un periodo escolar... con tu administrador');
            return $this->redirect($this->generateUrl('colegio_docente_homepage'));
        }
        $idperiodo = $periodo->getId();
 		return $idperiodo;
 	}

 	public function obtenerColegio($usuarioActivo)
 	{
        $idColegio = $usuarioActivo->getIdColegio();
        return $idColegio;
 	}

 	public function obtenerUsuario(){
 		$usuarioActivo = $this->get('security.context')->getToken()->getUser();
		return $usuarioActivo; 		
 	}
}