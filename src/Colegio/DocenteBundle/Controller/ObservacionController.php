<?php

namespace Colegio\DocenteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\DocenteBundle\Entity\Observacion;
use Colegio\DocenteBundle\Form\ObservacionType;

/**
 * Observacion controller.
 *
 */
class ObservacionController extends Controller
{

    /**
     * Lists all Observacion entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ColegioDocenteBundle:Observacion')->findAll();

        return $this->render('ColegioDocenteBundle:Observacion:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Observacion entity.
     *
     */
    public function createAction(Request $request,$matricula)
    {

        
        $em               =  $this->getDoctrine()->getManager();
        $usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
        $idDocente        =  $usuarioActivo->getId();
        $idColegio        =  $usuarioActivo->getIdColegio();
        $estudiante  = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->find($matricula);

        $hoy = new \DateTime('now');
        $periodos = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);

        $docente = $em->getRepository('ColegioDocenteBundle:Docente')->findOneById($idDocente);

        $entity = new Observacion();
        $entity->setCreador($docente); 
        $entity->setFechaCreacion($hoy);       
        $entity->setFechaModificacion($hoy);
        $entity->setEstudiante($estudiante);
        $entity->setPeriodo($periodos);

        $form = $this->createCreateForm($entity,$matricula);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!, PUEDES CERRAR ESTA VENTANA');
            return $this->redirect($this->generateUrl('observacion_show', array('id' => $entity->getId())));
        }

        return $this->render('ColegioDocenteBundle:Observacion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Observacion entity.
    *
    * @param Observacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Observacion $entity,$matricula)
    {
        $form = $this->createForm(new ObservacionType(), $entity, array(
            'action' => $this->generateUrl('observacion_create',array('matricula'=>$matricula)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Observacion entity.
     *
     */
    public function newAction($matricula)
    {
        
        $em               =  $this->getDoctrine()->getManager();
        $usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
        $idDocente        =  $usuarioActivo->getId();
        $colegio          =  $usuarioActivo->getIdColegio();
        
        $prueba = $em->getRepository('ColegioDocenteBundle:Observacion')->findObservaciones($colegio,$matricula);

        if($prueba == null){
            
        }else{
            return $this->redirect($this->generateUrl('observacion_show', array('id' => $prueba->getId())));
               
        }
        
        $entity = new Observacion();
        $entity->setEstado(true);


        $form   = $this->createCreateForm($entity,$matricula);

        return $this->render('ColegioDocenteBundle:Observacion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'matricula'=> $matricula,
           ));
    }

    /**
     * Finds and displays a Observacion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioDocenteBundle:Observacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Observacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioDocenteBundle:Observacion:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Observacion entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioDocenteBundle:Observacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Observacion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioDocenteBundle:Observacion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Observacion entity.
    *
    * @param Observacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Observacion $entity)
    {
        $form = $this->createForm(new ObservacionType(), $entity, array(
            'action' => $this->generateUrl('observacion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Observacion entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioDocenteBundle:Observacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Observacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!, puedes cerrar esta ventana');
            return $this->redirect($this->generateUrl('observacion_edit', array('id' => $id)));
        }

        return $this->render('ColegioDocenteBundle:Observacion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Observacion entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioDocenteBundle:Observacion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Observacion entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');
        }

        return $this->redirect($this->generateUrl('observacion'));
    }

    /**
     * Creates a form to delete a Observacion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('observacion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }

    public function crearAction($matricula)
    {
        $em               =  $this->getDoctrine()->getManager();
       	$usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
    	$idDocente        =  $usuarioActivo->getId();
        $idColegio        =  $usuarioActivo->getIdColegio();
        $estudiante  = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->find($matricula);

        $hoy = new \DateTime('now');
        $periodos = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);

        $docente = $em->getRepository('ColegioDocenteBundle:Docente')->find($idDocente);

        $entity = new Observacion();
        $entity->setCreador($docente); 
        $entity->setFechaCreacion($hoy);       
        $entity->setFechaModificacion($hoy);
        $entity->setEstudiante($estudiante);
        $entity->setPeriodo($periodos);
 

       $form = $this->createForm(new ObservacionType(), $entity, array(
            'action' => $this->generateUrl('observacion_crear',array('matricula'=>$matricula)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));
        return $this->render('ColegioDocenteBundle:Observacion:crear.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'matricula'=> $matricula,
        ));


    }
}
