<?php

namespace Colegio\DocenteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Colegio\EstudianteBundle\Entity\Estudiante;
use Colegio\EstudianteBundle\Form\PerfilType;
use Colegio\AdminBundle\Entity\Mensajes;



class ChatController extends Controller
{
    
    public function chatAction()
    {
        $em               =  $this->getDoctrine()->getManager();
        $usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
        $idDocente     =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();

        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
         if($periodo==null){
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Solicita por favor que habiliten un periodo escolar... con tu administrador');
            return $this->redirect($this->generateUrl('colegio_estudiante_homepage'));
        }
        $periodoEscolar = $periodo->getPeriodoEscolar()->getId();
        $idperiodo = $periodo->getId();

        $administradores = $em->getRepository('UsuariosUsuariosBundle:Usuarios')->findByIdColegio($idColegio);

        $estudiantes = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findEstudianteDocente($periodoEscolar,$idDocente,$idColegio);

        $docentes = $em->getRepository('ColegioDocenteBundle:Docente')->findBy(array(
            'idColegio'=>$idColegio,
            'isActive'=>true
        ));

        return $this->render('ColegioDocenteBundle:Default:chat.html.twig',array(
            'administradores'=>$administradores,
            'estudiantes'=>$estudiantes,
            'docentes'=>$docentes
        ));
    }
}