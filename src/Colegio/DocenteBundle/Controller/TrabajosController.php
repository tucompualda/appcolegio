<?php

namespace Colegio\DocenteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\DocenteBundle\Entity\Trabajos;
use Colegio\DocenteBundle\Form\TrabajosType;
use Colegio\DocenteBundle\Form\TrabajosLogrosType;

/**
 * Trabajos controller.
 *
 */
class TrabajosController extends Controller
{

    /**
     * Lists all Trabajos entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idDocente = $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();
        
        $periodoactual = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
        $fechaInicial = $periodoactual->getFechaInicio();
        $fechaFinal = $periodoactual->getFechaFin();
        
        $entities = $em->getRepository('ColegioDocenteBundle:Trabajos')->findTrabajos($idDocente,$fechaInicial,$fechaFinal);

        return $this->render('ColegioDocenteBundle:Trabajos:index.html.twig', array(
            'entities' => $entities,
            'periodo'  => $periodoactual->getId()
        ));
    }
    /**
     * Creates a new Trabajos entity.
     *
     */
    public function createAction(Request $request,$grupoAsignatura,$periodo)
    {
        
        $em               =  $this->getDoctrine()->getManager();
       	$usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
    	$idDocente        =  $usuarioActivo->getId();
        
        $grupo = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->find($grupoAsignatura);
        
        
        $entity = new Trabajos();
        $hoy = new \DateTime('now');
        $entity->setFecha($hoy);
        $form = $this->createCreateForm($entity,$grupoAsignatura, $periodo);
        $form->handleRequest($request);
        $idColegio = $usuarioActivo->getIdColegio();
        
        $periodoactual = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
        $idPeriodo = $periodoactual->getId();
        
        $periodoFinal = $em->getRepository('ColegioBoletinBundle:Periodo')->find($idPeriodo);
        $entity->setPeriodo($periodoFinal);
        
        $entity->setGrupoAsignatura($grupo);
       
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
             $this->get('session')->getFlashBag()->add(
            'notice',
            'Has creado correctamente tu trabajo!, tienes una hora para editar a partir de que Publiques la tarea');
            return $this->redirect($this->generateUrl('trabajos_new', array('grupoAsignatura' => $grupoAsignatura, 'periodo'=>$periodo)));
        }

        return $this->render('ColegioDocenteBundle:Trabajos:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Trabajos entity.
    *
    * @param Trabajos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Trabajos $entity,$grupoAsignatura,$periodo)
    {

        $em = $this->getDoctrine()->getManager();

        $hoy = new \DateTime('tomorrow + 3 days');
    	$usuarioActivo = $this->get('security.context')->getToken()->getUser();
    	$idDocente = $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();
        
        $periodoactual = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
        $idPeriodo = $periodoactual->getId();

        $entity->setFechaEntrega($hoy);
        //// REVISAR URGENTE TrabajosLogrosType  !!!
        $grupoAsig = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->find($grupoAsignatura);
        $asignatura = $grupoAsig->getIdAsignatura();

        if($periodoactual->getTipoCalificacion()->getDescripcion()=="FINAL"){
            
            $form = $this->createForm(new TrabajosType($idDocente,$idColegio,$grupoAsignatura,$periodo,$asignatura), $entity, array(
                'action' => $this->generateUrl('trabajos_create',array('grupoAsignatura'=>$grupoAsignatura, 'periodo'=>$periodo)),
                'method' => 'POST',
            ));

        }else if($periodoactual->getTipoCalificacion()->getDescripcion()=="LOGROS"){

            $form = $this->createForm(new TrabajosLogrosType($idDocente,$idColegio,$grupoAsignatura,$periodo,$asignatura), $entity, array(
                'action' => $this->generateUrl('trabajos_create',array('grupoAsignatura'=>$grupoAsignatura, 'periodo'=>$periodo)),
                'method' => 'POST',
            ));
        }
            



        $form->add('submit', 'submit', array('label' => '  Guardar  '));

        return $form;
    }



    /**
     * Displays a form to create a new Trabajos entity.
     *
     */
    public function newAction($grupoAsignatura,$periodo)
    {
        $entity = new Trabajos();
        $form   = $this->createCreateForm($entity,$grupoAsignatura,$periodo);
        $em  =  $this->getDoctrine()->getManager();
        $trabajos = $em->getRepository('ColegioDocenteBundle:Trabajos')
                ->findBy(
                    array('grupoAsignatura'=>$grupoAsignatura,'periodo' => $periodo),
                    array('id'=>'DESC')
                  );
        $grupo = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->find($grupoAsignatura);
         
        
        ////////////////////// actualiza los porcentajes /////////////////////////////////
         $periodoActual = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodo);
          
          $usuarioActivo = $this->get('security.context')->getToken()->getUser();
          $idDocente = $usuarioActivo->getId();
          $colegio = $usuarioActivo->getIdColegio();
          
          $porcentajesGrupos = $em->getRepository('ColegioDocenteBundle:PorcentajeAsignaturaTrabajo')->findBy(array(
              'grupoAsignatura' => $grupo,
              'periodo'         => $periodoActual
          ));
          
         
          $porcentajesTrabajo = $em->getRepository('ColegioAdminBundle:PorcentajeTrabajo')->findPorcentajes($periodoActual);
           
            foreach ($porcentajesTrabajo as $nuevos){

                $tipoTrabajo = $nuevos->getId();
                $tipoTrabajoEspecifico = $em->getRepository('ColegioAdminBundle:PorcentajeTrabajo')->find($tipoTrabajo);
                $porcentajesGruposConsulta = $em->getRepository('ColegioDocenteBundle:PorcentajeAsignaturaTrabajo')->findBy(array(
                    'grupoAsignatura' => $grupo,
                    'periodo' => $periodoActual,
                    'porcentajeTrabajo' => $tipoTrabajo
                  ));    
                
                if($porcentajesGruposConsulta == null){
                    $insersionPorcentajes = new \Colegio\DocenteBundle\Entity\PorcentajeAsignaturaTrabajo();
                    $insersionPorcentajes->setEstado(true);
                    $insersionPorcentajes->setGrupoAsignatura($grupo);
                    $insersionPorcentajes->setNuevoPorcentaje($nuevos->getPorcentaje());
                    $insersionPorcentajes->setPeriodo($periodoActual);
                    $insersionPorcentajes->setPorcentajeTrabajo($tipoTrabajoEspecifico);
                   
                    $em->persist($insersionPorcentajes);
                    $em->flush();
                    
                }
        
            }
        
        
        //////////////////////// fin actualizacion ///////////////////////////////////////
        
        
        ///////////////////// tipos de tareas ////////////////////////////////////////////
        
        $tipos  = $em->getRepository('ColegioDocenteBundle:PorcentajeAsignaturaTrabajo')->findBy(array(
           'grupoAsignatura'=>$grupo,
           'periodo'=> $periodo 
        ));

        $totales = $em->getRepository('ColegioDocenteBundle:Trabajos')->findTrabajosTipo($grupoAsignatura,$periodo);

        ///////////////////////// fin tipo tareas ////////////////////////////////////////    
     
        $logros = $em->createQuery('SELECT l, b, g, t FROM ColegioBoletinBundle:Logro l 
                JOIN l.idBoletin b JOIN b.gruposBoletin g LEFT JOIN l.trabajos t
                WHERE b.colegio = :colegio AND b.periodo = :periodo AND g.grupoAsignatura = :asignatura
                GROUP By l.id, t.id
            ');
        $logros->setParameter('colegio', $colegio);
        $logros->setParameter('periodo', $periodoActual);
        $logros->setParameter('asignatura', $grupo);
        $logrosEntrega = $logros->getResult();
        
        return $this->render('ColegioDocenteBundle:Trabajos:new.html.twig', array(
            'periodoActual'=>$periodoActual,
            'periodo'=>$periodo,
            'grupo'=>$grupo,
            'trabajos'=>$trabajos,
            'entity' => $entity,
            'tiposTrabajo'=>$tipos,
            'totales'=>$totales,
            'form'   => $form->createView(),
            'logros' => $logrosEntrega
        ));
    }

    /**
     * Finds and displays a Trabajos entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idDocente = $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();

        $estudiantes = $em->getRepository('ColegioEstudianteBundle:TrabajoMatricula')->findBy(
            array('trabajo'=>$id, 'estado'=>true),
            array('fecha'=>'ASC')
        );
        //$entity = $em->getRepository('ColegioDocenteBundle:Trabajos')->find($id);
        $entityQuery = $em->createQuery('
            SELECT t,ga,d FROM ColegioDocenteBundle:Trabajos t
            INNER JOIN t.grupoAsignatura ga
            INNER JOIN ga.idDocente d
            WHERE t.id = :id AND d.id = :idDocente
        ');
        $entityQuery->setParameter('id',$id);
        $entityQuery->setParameter('idDocente',$idDocente);
        $entityQuery->setMaxResults(1);
        $entity = $entityQuery->getOneOrNullResult();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Trabajos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioDocenteBundle:Trabajos:show.html.twig', array(
            'entity'      => $entity,
            'estudiantes' => $estudiantes,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Trabajos entity.
     *
     */
    public function editAction($id,$grupoAsignatura,$periodo)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioDocenteBundle:Trabajos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Trabajos entity.');
        }

       

        $editForm = $this->createEditForm($entity,$grupoAsignatura,$periodo);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioDocenteBundle:Trabajos:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Trabajos entity.
    *
    * @param Trabajos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Trabajos $entity,$grupoAsignatura,$periodo)
    {
    	$usuarioActivo = $this->get('security.context')->getToken()->getUser();
    	$idDocente = $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();
        $form = $this->createForm(new TrabajosType($idDocente,$idColegio,$grupoAsignatura,$periodo), $entity, array(
            'action' => $this->generateUrl('trabajos_update', array('id' => $entity->getId(),'grupoAsignatura'=>$grupoAsignatura,'periodo'=>$periodo)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Trabajos entity.
     *
     */
    public function updateAction(Request $request, $id, $grupoAsignatura, $periodo)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioDocenteBundle:Trabajos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Trabajos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity,$grupoAsignatura,$periodo);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('trabajos_new', array('grupoAsignatura' => $grupoAsignatura, 'periodo'=>$periodo)));
        }

        return $this->render('ColegioDocenteBundle:Trabajos:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Trabajos entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioDocenteBundle:Trabajos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Trabajos entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('trabajos'));
    }

    /**
     * Creates a form to delete a Trabajos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('trabajos_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
