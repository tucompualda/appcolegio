<?php

namespace Colegio\DocenteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\EstudianteBundle\Entity\Nota;
use Colegio\EstudianteBundle\Entity\NotaLogro;
use Colegio\DocenteBundle\Form\NotaType;
use Colegio\DocenteBundle\Form\NotaLogroType;

/**
 * Nota controller.
 *
 */
class NotaController extends Controller
{

    /**
     * Lists all Nota entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ColegioEstudianteBundle:Nota')->findAll();

        return $this->render('ColegioDocenteBundle:Nota:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Nota entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Nota();
        $hoy = new \DateTime('now');
        $entity->setFecha($hoy);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('notadocente_show', array('id' => $entity->getId())));
        }

        return $this->render('ColegioDocenteBundle:Nota:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Nota entity.
    *
    * @param Nota $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Nota $entity,$matricula,$asignatura)
    {
		
        $form = $this->createForm(new NotaType($matricula,$asignatura), $entity, array(
            'action' => $this->generateUrl('notadocente_create',$matricula,$asignatura),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Nota entity.
     *
     */
    public function newAction()
    {
        $entity = new Nota();
        $form   = $this->createCreateForm($entity);

        return $this->render('ColegioDocenteBundle:Nota:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Nota entity.
     *
     */
    public function showAction($id,$matricula,$asignatura,$grupo)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioEstudianteBundle:Nota')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Nota entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioDocenteBundle:Nota:show.html.twig', array(
            'entity'      => $entity,
            'matricula'   => $matricula,
            'asignatura'  => $asignatura,
            'grupo'       => $grupo,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Nota entity.
     *
     */
    public function editAction($id,$matricula,$asignatura,$grupo)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioEstudianteBundle:Nota')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Nota entity.');
        }

        $editForm = $this->createEditForm($entity,$matricula,$asignatura,$grupo);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioDocenteBundle:Nota:edit.html.twig', array(
            'entity'      => $entity,
            'matricula'	  => $matricula,
            'asignatura'  => $asignatura,
            'grupo'	      => $grupo,			
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Nota entity.
    *
    * @param Nota $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Nota $entity,$matricula,$asignatura,$grupo)
    {
		
        $em = $this->getDoctrine()->getManager();

		//Obtenemos el periodo actual, debemos mejorarlo, ya que hay un servicio q lo hace y se lo pone a las plantillas
		$usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
		
		$hoy = new \DateTime('now');
		$periodoactual = $em->createQuery(
			'SELECT p FROM ColegioBoletinBundle:Periodo p WHERE p.fechaInicio < :actual  AND p.fechaFin >= :actual AND p.colegio = :colegio'
		)->setParameters(array(
			'actual' => $hoy,
			'colegio'=> $idColegio,
		))->setMaxResults(1);
		
		$periodo = $periodoactual->getResult();
        $form = $this->createForm(new NotaType($matricula,$asignatura,$periodo), $entity, array(
            'action' => $this->generateUrl('notadocente_update', array('id' => $entity->getId(),'matricula'=>$matricula,'asignatura'=>$asignatura,'grupo'=>$grupo)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Nota entity.
     *
     */
    public function updateAction(Request $request, $id, $matricula,$asignatura,$grupo)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioEstudianteBundle:Nota')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Nota entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity,$matricula,$asignatura,$grupo);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
			$this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho lo has actualizado correctamente! Puedes cerrar esta ventana');
            return $this->redirect($this->generateUrl('notadocente_edit', array('id' => $id, 'matricula'=>$matricula,'asignatura'=>$asignatura,'grupo'=>$grupo)));
        }

        return $this->render('ColegioDocenteBundle:Nota:edit.html.twig', array(
            'entity'      => $entity,
            'matricula'   => $matricula,
            'asignatura'  => $asignatura,
            'grupo'		  => $grupo,		
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Nota entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioEstudianteBundle:Nota')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Nota entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('notadocente'));
    }

    /**
     * Creates a form to delete a Nota entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('notadocente_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
    
    public function registroAction(Request $request,$matricula,$asignatura,$grupo, $nota, $logro)
    {
		
		$em = $this->getDoctrine()->getManager();
		//Obtenemos el periodo actual, debemos mejorarlo, ya que hay un servicio q lo hace y se lo pone a las plantillas
		$usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
		
		
		$hoy = new \DateTime('now');
		$periodoactual = $em->createQuery(
			'SELECT p,t FROM ColegioBoletinBundle:Periodo p 
             JOIN p.tipoCalificacion t 
             WHERE p.fechaInicio < :actual  AND p.fechaFin >= :actual AND p.colegio = :colegio'
		)->setParameters(array(
			'actual' => $hoy,
			'colegio'=> $idColegio,
		))->setMaxResults(1);
		
		$periodo = $periodoactual->getResult();
		$idperiodo = $periodo[0]->getId();
        $parametro = $periodo[0]->getTipoCalificacion();
        $idpara = $periodo[0]->getTipoCalificacion()->getId();

		$gruposBoletines = $em->getRepository('ColegioBoletinBundle:GrupoBoletin')->findBy(array(
				'grupoAsignatura' => $asignatura,
				'periodo'         => $periodo,
		));

        $periodoEnviado = $em->getRepository('ColegioBoletinBundle:Periodo')->find($idperiodo);
		/**$gruposBoletinesQuery = $em->createQuery('SELECT g FROM ColegioBoletinBundle:GrupoBoletin g JOIN g.boletin b LEFT JOIN b.logros l LEFT JOIN l.notalogro n WHERE g.grupoAsignatura = :asignatura AND g.periodo = :periodo AND n.matricula = :matricula')
		->setParameters(array(
			'asignatura'=>$asignatura,
			'periodo'=> $periodo,
			'matricula'=> $matricula
		));
		$gruposBoletines = $gruposBoletinesQuery->getResult();*/
		
		
		$grupoBoletin = $em->getRepository('ColegioBoletinBundle:GrupoBoletin')->findBy(array(
				'grupoAsignatura'=>$asignatura,
		));
		
        if($grupoBoletin==null){
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Error!, solicita a tu admin un Boletin, o creálo directamente en tu menú');
            return $this->redirect($this->generateUrl('colegio_docente_homepage'));
        }

    	$mygroup = $grupoBoletin;
		$idgrupoBoletin = $grupoBoletin[0]->getId();

        $verificacionQuery = $em->createQuery('SELECT n FROM ColegioEstudianteBundle:Nota n JOIN n.grupoBoletin g WHERE g.id = :grupo AND n.matricula = :matricula');
        $verificacionQuery->setParameter('grupo',$gruposBoletines);
        $verificacionQuery->setParameter('matricula',$matricula);
      //  $verificacionQuery->setParameter('periodo',$idperiodo);

        $verificacion = $verificacionQuery->getResult();

		$verificacion2 = $em->getRepository('ColegioEstudianteBundle:Nota')->findBy(array(
				'grupoBoletin'=>$mygroup,
				'matricula'=>$matricula,
		));
		
        $entity = new Nota();
        $hoy = new \DateTime('now');
        $entity->setFallas(0);
        $entity->setFecha($hoy);
        $entity->setDescripcion("...");
        
        $grupoestudiante = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->find($matricula);
        $entity->setMatricula($grupoestudiante);
       
       ////////Insersion de NotasLogros//////////
       
       
       //Consulta de Notas del estudiante ///////
       
       //// fin consulta ////////////////////////
		
		$datos = $request->request->all();
	
        $form = $this->createForm(new NotaType($matricula,$asignatura,$periodo), $entity);
        $form->add('submit','submit',array('label'=>'Crear'));

                if($idpara == 1){

                        $form->handleRequest($request);

                        if ($form->isValid()) {
                            $em = $this->getDoctrine()->getManager();
                            $em->persist($entity);
                            $em->flush();

                            return $this->redirect($this->generateUrl('notadocente_show', array('id' => $entity->getId(),'matricula'=>$matricula,'asignatura'=>$asignatura,'grupo'=>$grupo)));
                        }
                        
                       
                }else{    
                          
                            if($request->getMethod() == 'POST'){
                                        $notica = $this->get('request')->request->get('notas');
                                        $logrito = $this->get('request')->request->get('logros');
                                        
                                        $logritos = $em->getRepository('ColegioBoletinBundle:Logro')->find($logrito);
                                       
                                            $notalogro = new NotaLogro();
                                            $notalogro->setCalificacion($notica);
                                            $notalogro->setDescripcion('--');
                                            $notalogro->setMatricula($grupoestudiante);
                                            $notalogro->setLogro($logritos);

                                            $em->persist($notalogro);
                                            $em->flush();
                   // return $this->redirect($this->generateUrl('notadocente_registro'),array('matricula'=>$matricula,'asignatura'=>$asignatura,'grupo'=>grupo));
                               //     path('',{ 'matricula': entity.id, 'asignatura': asignatura.id, 'grupo': grupo.id }) }}
                                }
                               
                  
                       ///////fin de insersión//////////
                } 

        return $this->render('ColegioDocenteBundle:Nota:new.html.twig', array(
            'gruposBoletines' => $gruposBoletines,
            'entity'        => $entity,
            'grupo'         => $grupo,
            'asignatura'    => $asignatura,
            'verificacion'  => $verificacion,
            'matricula'     => $matricula,
            'form'          => $form->createView(),
            'parametro'     => $parametro,
            'GrupoEstudiante'=>$grupoestudiante,
            'periodoactual'=>$periodoEnviado
            
        ));
    }
}
