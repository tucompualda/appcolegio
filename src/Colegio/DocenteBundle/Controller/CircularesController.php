<?php

namespace Colegio\DocenteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\AdminBundle\Entity\Circulares;
use Colegio\DocenteBundle\Form\CircularesType;

/**
 * Circulares controller.
 *
 */
class CircularesController extends Controller
{

    /**
     * Lists all Circulares entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
 
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $periodoAnual = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->getPeriodo($idColegio);
        $entities = $em->getRepository('ColegioAdminBundle:Circulares')->findBy(array(
            'colegio'=>$idColegio,
            'docente'=>$usuarioActivo,
            'periodoEscolar'=> $periodoAnual->getId()
            ),array(
            'fechaVisita'=>'DESC'
            ));

        return $this->render('ColegioDocenteBundle:Circulares:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Circulares entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $periodoAnual = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->getPeriodo($idColegio);
        //$entidad->setPeriodoEscolar($periodoAnual);
        $entity = new Circulares();
        $form = $this->createCreateForm($entity,$idColegio);
        $form->handleRequest($request);

        $hoy = new \DateTime('now');
        $entity->setFecha($hoy);
        $entity->setFechaVisita($hoy);
        $entity->setColegio($idColegio);
        $entity->setDocente($usuarioActivo);
        $entity->setPeriodoEscolar($periodoAnual);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!, tienes una hora para editar luego de checkear en "Publicado"');
            return $this->redirect($this->generateUrl('circulares_showDocente', array('id' => $entity->getId())));
        }

        return $this->render('ColegioAdminBundle:Circulares:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Circulares entity.
     *
     * @param Circulares $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Circulares $entity,$idColegio)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        

        $form = $this->createForm(new CircularesType($idColegio,$usuarioActivo), $entity, array(
            'action' => $this->generateUrl('circulares_createDocente'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Circulares entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();

        
        $periodoAnual = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->getPeriodo($idColegio);
       
        $entity = new Circulares();
        $form   = $this->createCreateForm($entity,$idColegio);

        return $this->render('ColegioDocenteBundle:Circulares:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Circulares entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $estudiantes = $em->getRepository('ColegioAdminBundle:MatriculasCirculares')->findBy(array(
                'circular'=>$id
            ),array(
                'fechaVisita'=>'ASC'
            ));
        $entity = $em->getRepository('ColegioAdminBundle:Circulares')->findOneBy(array(
            'id'=>$id,
            'docente'=>$usuarioActivo
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Circulares entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioDocenteBundle:Circulares:show.html.twig', array(
            'entity'      => $entity,
            'estudiantes' => $estudiantes,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Circulares entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:Circulares')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Circulares entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioDocenteBundle:Circulares:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Circulares entity.
    *
    * @param Circulares $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Circulares $entity)
    {
        $em = $this->getDoctrine()->getManager();

      $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        

        $hoy = new \DateTime('now');$usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        
        $periodoAnual = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->getPeriodo($idColegio);
        
        $entity->setFechaVisita($hoy);

        $form = $this->createForm(new CircularesType($idColegio,$usuarioActivo), $entity, array(
            'action' => $this->generateUrl('circulares_updateDocente', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Circulares entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:Circulares')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Circulares entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);


        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!, tienes una hora para editar luego de checkear en "Publicado"');
            return $this->redirect($this->generateUrl('circulares_editDocente', array('id' => $id)));
        }

        return $this->render('ColegioDocenteBundle:Circulares:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Circulares entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioAdminBundle:Circulares')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Circulares entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');
        }

        return $this->redirect($this->generateUrl('circulares'));
    }

    /**
     * Creates a form to delete a Circulares entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('circulares_deleteDocente', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
