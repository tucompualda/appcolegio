<?php

namespace Colegio\DocenteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Colegio\DocenteBundle\Entity\Estudiante;
use Colegio\DocenteBundle\Form\perfilType;

use Colegio\BoletinBundle\Entity\Boletin;
use Colegio\BoletinBundle\Entity\GrupoBoletin;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ColegioDocenteBundle:Default:index.html.twig');
    }
    
    public function gruposAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$usuarioActivo = $this->get('security.context')->getToken()->getUser();
    	$idDocente = $usuarioActivo->getId();
        $colegio = $usuarioActivo->getIdColegio();
        
        $hoy = new \DateTime('now');
        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($colegio);
        
         if($periodo==null){
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Solicita por favor que habiliten un periodo escolar... con tu administrador');
            return $this->redirect($this->generateUrl('colegio_docente_homepage'));
        }
        $idperiodo = $periodo->getId();
        
        $periodoAnual = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->getPeriodo($colegio)->getId();
        $asignatura = $em->getRepository('ColegioDocenteBundle:Docente')->findGrupos($idDocente);
    	 
    	

////////////////////////////////////////////////// tipos trabajos //////////////////////////////
         $tipos  = $em->getRepository('ColegioDocenteBundle:PorcentajeAsignaturaTrabajo')->findByPeriodo($periodo);

        $totales = $em->getRepository('ColegioDocenteBundle:Trabajos')->findTrabajosTipoTotal($periodo);
//////////////////////// fin tipos ///////////////////////////////////////////////////////////




    	return $this->render('ColegioDocenteBundle:Default:grupos.html.twig', array(
    			'asignaturas'=> $asignatura,
                'idperiodo'  => $idperiodo,
                'tipos'      => $tipos,
                'totales'    => $totales
    	));
    }

    public function matriculasAction($id,$asignatura,$formato)
    {
        // id = Grupo
        // asignatura = grupo asignatura
    	$em               =  $this->getDoctrine()->getManager();
       	$usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
    	$idDocente        =  $usuarioActivo->getId();
    	$nota = null;
    	$logro = null;

        $idColegio = $usuarioActivo->getIdColegio();

        $hoy = new \DateTime('now');
        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
        
        if($periodo == null){
            $this->get('session')->getFlashBag()->add(
                'notice', 'Solicita por favor que habiliten un periodo escolar... con tu administrador');
            return $this->redirect($this->generateUrl('colegio_docente_homepage'));
        }
        $idperiodo = $periodo->getId();
      	//  Obtener la Materia del grupo asignatura, para enviarla por array a las calificaciones (registroAction)
    	$materia =  $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->findOneById($asignatura);
    	    	
    	$entities2 = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findByIdGrupo($id);
    	$entitiesQuery = $em->createQuery('
            SELECT g,e 
            FROM ColegioEstudianteBundle:GrupoEstudiante g 
            JOIN g.idEstudiante e 
            WHERE g.idGrupo = :id AND g.isActive = 1 
            GROUP BY g.id ORDER BY e.primerApellido ASC');
        //$entitiesQuery->setParameter('id',$id);
        
  ///////////////////// tipos de tareas ////////////////////////////////////////////
        
        $tipos  = $em->getRepository( 'ColegioDocenteBundle:PorcentajeAsignaturaTrabajo' )->findBy(array(
            'grupoAsignatura'=>$materia,
            'periodo'=>$idperiodo
        ));

        $totales = $em->getRepository( 'ColegioDocenteBundle:Trabajos' )->findTrabajosTipo($materia,$periodo);

        ///////////////////////// fin tipo tareas ////////////////////////////////////////    
        
        //$entitiesQuery = $em->createQuery('SELECT n,m,e FROM ColegioEstudianteBundle:Nota n LEFT JOIN n.matricula m JOIN m.idEstudiante e WHERE m.idGrupo = :id');
        $entitiesQuery->setParameter( 'id',$id );

        ///////////////////// Precarga de notas ////////////////////////////
         
        $notasfin = $em->getRepository( 'ColegioDocenteBundle:NotaTrabajo' )->cargarNotas( $id, $asignatura );
        
        
        /////////////////// Fin precarga de notas //////////////////////////
        
        $gruposBoletines = $em->getRepository('ColegioBoletinBundle:GrupoBoletin')->findBy(array(
        'grupoAsignatura' => $asignatura,
        'periodo'         => $periodo,
        ));
         
        if( $gruposBoletines == null ){
            if($periodo->getCrearBoletinDefecto()){
                $materiaNueva = $materia->getIdAsignatura();
                
                $boletinNuevo = new Boletin();
                $boletinNuevo->setAsignatura($materiaNueva);
                $boletinNuevo->setDescripcion('Boletín generado dinámicamente desde la planilla docente');
                $boletinNuevo->setFechaInicio($periodo->getFechaInicio());
                $boletinNuevo->setFechaFin($periodo->getFechaFin());
                $boletinNuevo->setActivo(true);
                $boletinNuevo->setColegio($idColegio);
                $boletinNuevo->setPeriodo($periodo);

                $em->persist($boletinNuevo);

                $grupoBoletinNuevo = new GrupoBoletin();
                $grupoBoletinNuevo->setBoletin($boletinNuevo);
                $grupoBoletinNuevo->setGrupoAsignatura($materia);
                $grupoBoletinNuevo->setPeriodo($periodo);
                $grupoBoletinNuevo->setColegio($idColegio);

                $em->persist($grupoBoletinNuevo);

                $em->flush();


                $this->get( 'session' )->getFlashBag()->add( 'notice', 
                    'ATENCIÓN: El sistema ha generado un Boletín sin logros por defecto para que la planilla pueda calcular la nota final, no olvides crear tus logros al inicio del período para un mejor desempeño.');
                 //return $this->redirect($this->generateUrl('colegio_docente_homepage'));
                $notas = null;
            }else{
                 $this->get( 'session' )->getFlashBag()->add( 'notice', 
                    'ATENCIÓN: El sistema no ha podido crear un boletín por defecto, solicita a tu Administrador/Rector que habilite la opción para este período.');
                 $notas = null;
            }
        }else{                
            $notasQuery = $em->createQuery( '
                SELECT n,g,a,m 
                FROM ColegioEstudianteBundle:Nota n 
                JOIN n.grupoBoletin g 
                JOIN g.grupoAsignatura a 
                JOIN n.matricula m
                WHERE a.idGrupo = :id AND g.periodo = :periodo AND n.grupoBoletin = :grupo 
                GROUP BY n.matricula' );
            $notasQuery->setParameter('id',$id);
            $notasQuery->setParameter('periodo',$idperiodo);
            $notasQuery->setParameter('grupo',$gruposBoletines);
            $notas = $notasQuery->getResult();
        }
      

        $entities = $entitiesQuery->getResult();

    	$grupos = $em->getRepository( 'ColegioGrupoBundle:Grupo' )->find($id);
	
        $trabajos = $em->getRepository( 'ColegioDocenteBundle:Trabajos' )->findTrabajoGrupo($asignatura,$idperiodo);
   
        if($formato==null){
            $respuesta = $this->render('ColegioDocenteBundle:Default:matriculas.html.twig', array(
            'formato' => "",
            'notasfinales'=> $notasfin,
            'trabajos'    => $trabajos,
            'entities'    => $entities,
            'grupo'       => $grupos,
            'asignatura'  => $materia,
            'nota'        => $nota,
            'logro'       => $logro,
            'notas'       => $notas,
            'idperiodo'   => $idperiodo,
            'tiposTrabajo'=>$tipos,
            'totales'=>$totales,
        	));
        }elseif ($formato=="pdf") {
             $html =  $this->renderView('ColegioDocenteBundle:Default:matriculas.html.twig', array(
            'formato' => $formato,
            'notasfinales'       => $notasfin,
            'trabajos'    => $trabajos,
            'entities'    => $entities,
            'grupo'       => $grupos,
            'asignatura'  => $materia,
            'nota'        => $nota,
            'logro'       => $logro,
            'notas'       => $notas,
            'idperiodo'   => $idperiodo,
            'tiposTrabajo'=>$tipos,
            'totales'=>$totales,
            ));

            $respuesta = new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html,array(
                        'lowquality' => true,
                        'encoding' => 'utf-8',
                        'page-size' => 'Letter',
                        'images' => true,
                        'image-dpi' => 100,
                        'margin-top' => 15,
                        'orientation'=> 'portrait',
                        'enable-internal-links'=> false
                )),
                200,
                array(
                    'Content-Type'          => 'application/pdf',
                    'Content-Disposition'   => 'attachment; filename="planilla.pdf"',
                    
                )
            );
        }

        return $respuesta;
    }
    
    public function notasAction($id,$asignatura)
    {
		
    }

    public function mensajesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
    	$idDocente        =  $usuarioActivo->getId();
        
        $mensajes = $em->getRepository('ColegioAdminBundle:Mensajes')->findMensajes($idDocente);
        
        return $this->render('ColegioDocenteBundle:Default:mensajes.html.twig',array(
            'mensajes' => $mensajes,
        ));
    }
    
    public function enviadosAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
    	$idDocente        =  $usuarioActivo->getId();
        
        $mensajes = $em->getRepository('ColegioAdminBundle:Mensajes')->findMensajesEnviadosDocente($idDocente);
        
        return $this->render('ColegioDocenteBundle:Default:mensajesEnviados.html.twig',array(
            'mensajes' => $mensajes,
        ));
    }
    
    
    public function migrupoAction($grupo)
    {
        $em               =  $this->getDoctrine()->getManager();
       	$usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
    	$idDocente        =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();
        $periodoAnual = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->getPeriodo($idColegio);
        
        //$grupos = $em->getRepository('ColegioGrupoBundle:Grupo')->findByIdDocenteResponsable($idDocente);
        $grupos = $em->getRepository('ColegioGrupoBundle:Grupo')->findBy(array(
            'idDocenteResponsable'=>$idDocente,
            'periodoEscolar'=>$periodoAnual
        ));
          
        $estudianteQuery = $em->createQuery('SELECT m,e,o FROM ColegioEstudianteBundle:GrupoEstudiante m '
                . ' JOIN m.idEstudiante e LEFT JOIN m.observaciones o '
                .'  WHERE  m.idGrupo = :grupo AND m.isActive = 1 ORDER BY e.primerApellido ASC');
        $estudianteQuery->setParameter('grupo',$grupo);
        $estudiante = $estudianteQuery->getResult();

        return $this->render('ColegioDocenteBundle:Default:migrupo.html.twig',array(
                'entities'  =>$grupos,
                'estudiante'=>$estudiante
        ));
    }

    
    public function entradaAction()
    {
        $peticion = $this->getRequest();
        $sesion = $peticion->getSession();
        $error = $peticion->attributes->get(
        SecurityContext::AUTHENTICATION_ERROR,
        $sesion->get(SecurityContext::AUTHENTICATION_ERROR)
        );
        return $this->render('UsuariosUsuariosBundle:Default:login3.html.twig', array(
        'last_username' => $sesion->get(SecurityContext::LAST_USERNAME),
        'error'   => $error
        ));
    }  

    public function perfilAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idUsuario = $usuarioActivo->getId();        

        $datos = $em->getRepository('ColegioDocenteBundle:Docente')->findOneById($idUsuario);

        $entity = $em->getRepository('ColegioDocenteBundle:Docente')->find($idUsuario);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Estudiante entity.');
        }

        
        
        $form = $this->createForm(new PerfilType(), $datos, array(
            //'action' => $this->generateUrl('estudiante_perfil_update', array('id' => $datos->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));
        
        $passwordOriginal = $form->getData()->getPassword();
        $form->handleRequest($request);
        if ($form->isValid()) {
                
            if (null == $datos->getPassword()) {
                $datos->setPassword($passwordOriginal);
                }
                else{
                    $encoder = $this->get('security.encoder_factory')
                                    ->getEncoder($datos);
                    $passwordCodificado = $encoder->encodePassword(
                        $datos->getPassword(),
                        $datos->getSalt()
                    );
                    $datos->setPassword($passwordCodificado);
                    }
            
            
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho! has actualizado correctamente tu Información');
            return $this->redirect($this->generateUrl('colegio_docente_perfil'));
        }

        return  $this->render('ColegioDocenteBundle:Default:perfil.html.twig',array(
                    'datos'  => $datos,
                    'form'   => $form->createView(),
            ));
    }
    
     /**
     * 
     * Lista los porcentajes por grupo actuales
     * 
     *
     */
    public function listarPorcentajesAction($grupoAsignatura, $periodo)
    {
          $em = $this->getDoctrine()->getManager();
          $grupo = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->find($grupoAsignatura);
          $periodoActual = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodo);
          
          $usuarioActivo = $this->get('security.context')->getToken()->getUser();
          $idDocente = $usuarioActivo->getId();
          $colegio = $usuarioActivo->getIdColegio();
          
          $porcentajesGrupos = $em->getRepository('ColegioDocenteBundle:PorcentajeAsignaturaTrabajo')->findBy(array(
              'grupoAsignatura'=>$grupo,
              'periodo' => $periodoActual
          ));
          
          //Colegio\BoletinBundle\Entity\CategoriaLogro
          $categoriaLogros = $em->getRepository('ColegioBoletinBundle:CategoriaLogro')->findByColegio($colegio);
         
          $porcentajesTrabajo = $em->getRepository('ColegioAdminBundle:PorcentajeTrabajo')->findPorcentajes($periodoActual);
           
            foreach ($porcentajesTrabajo as $nuevos){

                $tipoTrabajo = $nuevos->getId();
                $tipoTrabajoEspecifico = $em->getRepository('ColegioAdminBundle:PorcentajeTrabajo')->find($tipoTrabajo);
                $porcentajesGruposConsulta = $em->getRepository('ColegioDocenteBundle:PorcentajeAsignaturaTrabajo')->findBy(array(
                    'grupoAsignatura' => $grupo,
                    'periodo' => $periodoActual,
                    'porcentajeTrabajo' => $tipoTrabajo
                  ));    
                
                if($porcentajesGruposConsulta == null){
                    $insersionPorcentajes = new \Colegio\DocenteBundle\Entity\PorcentajeAsignaturaTrabajo();
                    $insersionPorcentajes->setEstado(true);
                    $insersionPorcentajes->setGrupoAsignatura($grupo);
                    $insersionPorcentajes->setNuevoPorcentaje($nuevos->getPorcentaje());
                    $insersionPorcentajes->setPeriodo($periodoActual);
                    $insersionPorcentajes->setPorcentajeTrabajo($tipoTrabajoEspecifico);
                   
                    $em->persist($insersionPorcentajes);
                    $em->flush();
                    
                }
                    
                    
             }
          
          
        $grupoBoletin = $em->getRepository('ColegioBoletinBundle:GrupoBoletin')->findBy(array(
            'grupoAsignatura'=>$grupo,
            'periodo'=>$periodoActual
        ));
               
        
        if($grupoBoletin==null){
                $boletin = "";
                $logros = "";
        }else{

                
                foreach ($grupoBoletin as $key) {
                    # code...
                    $boletin = $key->getBoletin()->getId();
                    $logros  = $em->getRepository('ColegioBoletinBundle:Logro')->findByIdBoletin($boletin);
         
                }
         }

          return $this->render('ColegioDocenteBundle:Default:porcentajes.html.twig',array(
              'grupoAsignatura'  => $grupo,
              'periodo'          => $periodoActual,
              'porcentajesGrupos'=> $porcentajesGrupos,
              'porcentajes'      => $porcentajesTrabajo,
              'grupoBoletin'     => $grupoBoletin,
              'categorias'       => $categoriaLogros,
              'boletin'          => $boletin,
              'logros'           => $logros
          ));
    }
    

    public function circularesDocenteAction()
    {
        return $this->render('ColegioDocenteBundle:Default:circulares.html.twig');
    }

    public function soporteAction()
    {
        return $this->render('ColegioDocenteBundle:Default:soporte.html.twig');
    }

    public function planillasAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idDocente = $usuarioActivo->getId();
        $colegio = $usuarioActivo->getIdColegio();

        $asignatura = $em->getRepository('ColegioDocenteBundle:Docente')->findGrupos($idDocente);
        
        $periodoAnual = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->getPeriodo($colegio)->getId();

        $periodos = $em->getRepository('ColegioBoletinBundle:Periodo')->findBy(array(
            'periodoEscolar'=>$periodoAnual,
            'colegio'=>$colegio
        ));

        
        return $this->render('ColegioDocenteBundle:Default:planillas.html.twig',array(
            'asignaturas'=>$asignatura,
            'listadoPeriodos'=>$periodos
        ));
    }

    public function planillaDescargaAction($grupoAsignatura,$periodo,$formato)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idDocente = $usuarioActivo->getId();
        $colegio = $usuarioActivo->getIdColegio();

        $GrupoAsignatura = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->findOneById($grupoAsignatura);
        $asignatura = $GrupoAsignatura->getIdAsignatura();
        $trabajos = $em->getRepository( 'ColegioDocenteBundle:Trabajos' )->findTrabajoGrupo($grupoAsignatura,$periodo);
        $grupo = $GrupoAsignatura->getIdGrupo();

        $matriculasQuery = $em->createQuery('
           SELECT 
                g,e,n,gb 
           FROM 
                ColegioEstudianteBundle:GrupoEstudiante g 
                JOIN g.idEstudiante e 
                LEFT JOIN g.notas n
                LEFT JOIN n.grupoBoletin gb
           WHERE 
                g.idGrupo = :id AND g.isActive = 1
                AND gb.periodo = :periodo
                AND gb.grupoAsignatura = :grupoasignatura
           GROUP BY g.id ORDER BY e.primerApellido ASC
        ');
        $matriculasQuery->setParameter('id',$grupo);
        $matriculasQuery->setParameter('periodo',$periodo);
        $matriculasQuery->setParameter('grupoasignatura',$grupoAsignatura);
        $matriculas = $matriculasQuery->getResult();

        $notasfin = $em->getRepository('ColegioDocenteBundle:NotaTrabajo')->cargarNotas($grupo,$grupoAsignatura);
        $periodoActual = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodo);

        $colegioQuery = $em->createQuery('
                SELECT c,d 
                FROM ColegioAdminBundle:Colegio c 
                JOIN c.detalle d 
                WHERE c.id = :idColegio');
        $colegioQuery->SetParameter('idColegio',$colegio);
        $colegioQuery->setMaxResults(1);
        $colegioDatos = $colegioQuery->getResult();
        
        foreach($colegioDatos as $cole)
        {
            $logo = $cole->getDetalle()->getWebPath();
            $nombreColegio = $cole->getNombre();
        }



        return $this->render('ColegioDocenteBundle:Default:planillasConsolidado.html.twig', array(
            'trabajos'=>$trabajos,
            'matriculas'=>$matriculas,
            'notas' => $notasfin,
            'grupoAsignatura' => $GrupoAsignatura,
            'periodoActual'=>$periodoActual,
            'logo'=>$logo,
            'datosColegio'=>$nombreColegio
        ));
    }
}
