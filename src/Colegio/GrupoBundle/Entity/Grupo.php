<?php

namespace Colegio\GrupoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Grupo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\GrupoBundle\Entity\GrupoRepository")
 */
class Grupo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\GrupoBundle\Entity\Nivel")
     */
    private $idNivel;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\DocenteBundle\Entity\Docente")
     */
    private $idDocenteResponsable;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Sede")
     */
    private $idSede;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    /**
     * @ORM\OneToMany(targetEntity="Colegio\GrupoBundle\Entity\GrupoAsignatura", mappedBy="idGrupo", cascade={"persist"})
     * @Assert\Valid()    
     */
    private $gruposAsignaturas;
    
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Colegio")
     */
    private $colegio;
    
    
     /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaInicio", type="datetime")
     */
    private $fechaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaFin", type="datetime")
     */
    private $fechaFin;
    
      /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\PeriodoEscolar")
     */
    private $periodoEscolar;
    
     /**
     * @var string
     *
     * @ORM\Column(name="tipoCalificacion", type="string", length=255)
     */
    private $tipoCalificacion="Cuantitativo";

    /**
    * @ORM\OneToMany(targetEntity="Colegio\EstudianteBundle\Entity\GrupoEstudiante", mappedBy="idGrupo", cascade={"persist"})
    * @Assert\Valid() 
    */
    private $grupoEstudiantes;    
 
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->gruposAsignaturas = new ArrayCollection();
    }

     /**
     * Set grupoEstudiantes
     *
     * @return \Colegio\EstudianteBundle\Entity\GrupoEstudiantes
     */
    public function setGrupoEstudiantes(ArrayCollection $grupoEstudiantes)
    {
        $this->grupoEstudiantes = $grupoEstudiantes;
    }

    /**
     * Get grupoEstudiantes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGrupoEstudiantes()
    {
        return $this->grupoEstudiantes;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idNivel
     *
     * @param string $idNivel
     * @return Grupo
     */
    public function setIdNivel(\Colegio\GrupoBundle\Entity\Nivel $idNivel)
    {
        $this->idNivel = $idNivel;
    
        return $this;
    }

    /**
     * Get idNivel
     *
     * @return string 
     */
    public function getIdNivel()
    {
        return $this->idNivel;
    }

    /**
     * Set idDocenteResponsable
     *
     * @param string $idDocenteResponsable
     * @return Grupo
     */
    public function setIdDocenteResponsable(\Colegio\DocenteBundle\Entity\Docente $idDocenteResponsable)
    {
        $this->idDocenteResponsable = $idDocenteResponsable;
    
        return $this;
    }

    /**
     * Get idDocenteResponsable
     *
     * @return string 
     */
    public function getIdDocenteResponsable()
    {
        return $this->idDocenteResponsable;
    }

    /**
     * Set idSede
     *
     * @param string $idSede
     * @return Grupo
     */
    public function setIdSede(\Colegio\AdminBundle\Entity\Sede $idSede)
    {
        $this->idSede = $idSede;
    
        return $this;
    }

    /**
     * Get idSede
     *
     * @return string 
     */
    public function getIdSede()
    {
        return $this->idSede;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Grupo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Grupo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    
        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }
    
    
    /**
     * Set gruposAsignaturas
     *
     * @return \Colegio\GrupoBundle\Entity\GrupoAsignatura
     */
    public function setGruposAsignaturas(ArrayCollection $gruposAsignaturas)
    {
        $this->gruposAsignaturas = $gruposAsignaturas;
        foreach ($gruposAsignaturas as $grupoAsignatura) {
            $grupoAsignatura->setIdGrupo($this);
        }
    }

    /**
     * Get gruposAsignaturas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGruposAsignaturas()
    {
        return $this->gruposAsignaturas;
    }

     /**
     * Set colegio
     *
     * @param string $colegio
     * @return Grupo
     */
    public function setColegio(\Colegio\AdminBundle\Entity\Colegio $colegio)
    {
        $this->colegio = $colegio;
    
        return $this;
    }

    /**
     * Get colegio
     *
     * @return string 
     */
    public function getColegio()
    {
        return $this->colegio;
    }
    
     /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return Boletin
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    
        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return Boletin
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    
        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    
    public function __toString()
    {
        return $this->getId()."-".$this->getNombre();
    }

    /**
     * Add gruposAsignaturas
     *
     * @param \Colegio\GrupoBundle\Entity\GrupoAsignatura $gruposAsignaturas
     * @return Grupo
     */
    public function addGruposAsignatura(\Colegio\GrupoBundle\Entity\GrupoAsignatura $gruposAsignaturas)
    {
        $this->gruposAsignaturas[] = $gruposAsignaturas;
    
        return $this;
    }

    /**
     * Remove gruposAsignaturas
     *
     * @param \Colegio\GrupoBundle\Entity\GrupoAsignatura $gruposAsignaturas
     */
    public function removeGruposAsignatura(\Colegio\GrupoBundle\Entity\GrupoAsignatura $gruposAsignaturas)
    {
        $this->gruposAsignaturas->removeElement($gruposAsignaturas);
    }

    /**
     * Set periodoEscolar
     *
     * @param \Colegio\AdminBundle\Entity\PeriodoEscolar $periodoEscolar
     * @return Grupo
     */
    public function setPeriodoEscolar(\Colegio\AdminBundle\Entity\PeriodoEscolar $periodoEscolar = null)
    {
        $this->periodoEscolar = $periodoEscolar;
    
        return $this;
    }

    /**
     * Get periodoEscolar
     *
     * @return \Colegio\AdminBundle\Entity\PeriodoEscolar 
     */
    public function getPeriodoEscolar()
    {
        return $this->periodoEscolar;
    }

    /**
     * Set tipoCalificacion
     *
     * @param string $tipoCalificacion
     * @return Grupo
     */
    public function setTipoCalificacion($tipoCalificacion)
    {
        $this->tipoCalificacion = $tipoCalificacion;
    
        return $this;
    }

    /**
     * Get tipoCalificacion
     *
     * @return string 
     */
    public function getTipoCalificacion()
    {
        return $this->tipoCalificacion;
    }

    /**
     * Add grupoEstudiantes
     *
     * @param \Colegio\EstudianteBundle\Entity\GrupoEstudiante $grupoEstudiantes
     * @return Grupo
     */
    public function addGrupoEstudiante(\Colegio\EstudianteBundle\Entity\GrupoEstudiante $grupoEstudiantes)
    {
        $this->grupoEstudiantes[] = $grupoEstudiantes;
    
        return $this;
    }

    /**
     * Remove grupoEstudiantes
     *
     * @param \Colegio\EstudianteBundle\Entity\GrupoEstudiante $grupoEstudiantes
     */
    public function removeGrupoEstudiante(\Colegio\EstudianteBundle\Entity\GrupoEstudiante $grupoEstudiantes)
    {
        $this->grupoEstudiantes->removeElement($grupoEstudiantes);
    }
}