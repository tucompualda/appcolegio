<?php

namespace Colegio\GrupoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * GrupoAsignatura
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\GrupoBundle\Entity\GrupoAsignaturaRepository")
 */
class GrupoAsignatura
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Colegio\GrupoBundle\Entity\Grupo", inversedBy="gruposAsignaturas", cascade={"persist"})
     * @ORM\JoinColumn(name="idGrupo_id", referencedColumnName="id")
     */
    private $idGrupo;

    /**
     * @ORM\ManyToOne(targetEntity="Colegio\GrupoBundle\Entity\Asignatura", inversedBy="asignaturasGrupos", cascade={"persist"})
     * @ORM\JoinColumn(name="idAsignatura_id", referencedColumnName="id")
     */
    private $idAsignatura;

    /**
     * @ORM\ManyToOne(targetEntity="Colegio\DocenteBundle\Entity\Docente", inversedBy="gruposAsignaturas", cascade={"persist"})
     * @ORM\JoinColumn(name="idDocente_id", referencedColumnName="id")
     */
    private $idDocente;

 
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Colegio")
     */
    private $colegio;
    
  /**
     * @ORM\Column(name="intensidadhoraria", type="float")
     */
    private $intensidadHoraria;
    
    /**
     * @ORM\OneToMany(targetEntity="Colegio\DocenteBundle\Entity\Trabajos", mappedBy="grupoAsignatura", cascade={"persist"})
     * @Assert\Valid()    
     */
    private $trabajos;

    /**
     * @ORM\OneToMany(targetEntity="Colegio\BoletinBundle\Entity\GrupoBoletin", mappedBy="grupoAsignatura", cascade={"persist"})
     * @Assert\Valid()    
     */
    private $gruposBoletines;

    /**
     * @ORM\OneToMany(targetEntity="Colegio\BoletinBundle\Entity\InscripcionAsignatura", mappedBy="grupoAsignatura", cascade={"persist"})
     * @Assert\Valid()    
     */
    private $inscripcionAsignatura;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isActive", type="boolean",nullable=true)
     */
    private $isActive;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->trabajos = new ArrayCollection();
        $this->inscripcionAsignatura = new ArrayCollection();
        $this->gruposBoletines = new ArrayCollection();

    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idGrupo
     *
     * @param \Colegio\GrupoBundle\Entity\Grupo $idGrupo
     * @return GrupoAsignatura
     */
    public function setIdGrupo(\Colegio\GrupoBundle\Entity\Grupo $idGrupo)
    {
        $this->idGrupo = $idGrupo;
    
        return $this;
    }

    /**
     * Get idGrupo
     *
     * @return \Colegio\GrupoBundle\Entity\Grupo
     */
    public function getIdGrupo()
    {
        return $this->idGrupo;
    }

    /**
     * Set idAsignatura
     *
     * @param \Colegio\GrupoBundle\Entity\Asignatura $idAsignatura
     * @return GrupoAsignatura
     */
    public function setIdAsignatura(\Colegio\GrupoBundle\Entity\Asignatura $idAsignatura)
    {
        $this->idAsignatura = $idAsignatura;
    
        return $this;
    }

    /**
     * Get idAsignatura
     *
     * @return \Colegio\GrupoBundle\Entity\Asignatura
     */
    public function getIdAsignatura()
    {
        return $this->idAsignatura;
    }

    /**
     * Set idDocente
     *
     * @param \Colegio\DocenteBundle\Entity\Docente $idDocente
     * @return GrupoAsignatura
     */
    public function setIdDocente(\Colegio\DocenteBundle\Entity\Docente $idDocente)
    {
        $this->idDocente = $idDocente;
    
        return $this;
    }

    /**
     * Get idDocente
     *
     * @return \Colegio\DocenteBundle\Entity\Docente 
     */
    public function getIdDocente()
    {
        return $this->idDocente;
    }
    
      /**
     * Set colegio
     *
     * @param string $colegio
     * @return GrupoAsignatura
     */
    public function setColegio(\Colegio\AdminBundle\Entity\Colegio $colegio)
    {
        $this->colegio = $colegio;
    
        return $this;
    }

    /**
     * Set trabajos
     *
     * @return \Colegio\DocenteBundle\Entity\Trabajos
     */
    public function setTrabajos(ArrayCollection $trabajos)
    {
        $this->trabajos = $trabajos;
    }

    /**
     * Get trabajos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrabajos()
    {
        return $this->trabajos;
    }

   /**
     * Set inscripcionAsignatura
     *
     * @return \Colegio\BoletinBundle\Entity\InscripcionAsignatura
     */
    public function setInscripcionAsignatura(ArrayCollection $inscripcionAsignatura)
    {
        $this->inscripcionAsignatura = $inscripcionAsignatura;
    }

    /**
     * Get inscripcionAsignatura
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscripcionAsignatura()
    {
        return $this->inscripcionAsignatura;
    }

    /**
     * Get colegio
     *
     * @return string 
     */
    public function getColegio()
    {
        return $this->colegio;
    }
    
    public function __toString() 
    {
        return $this->getIdGrupo()." - ".$this->getIdAsignatura();
    }

    /**
     * Set intensidadHoraria
     *
     * @param float $intensidadHoraria
     * @return GrupoAsignatura
     */
    public function setIntensidadHoraria($intensidadHoraria)
    {
        $this->intensidadHoraria = $intensidadHoraria;
    
        return $this;
    }

    /**
     * Get intensidadHoraria
     *
     * @return float 
     */
    public function getIntensidadHoraria()
    {
        return $this->intensidadHoraria;
    }

    /**
     * Add trabajos
     *
     * @param \Colegio\DocenteBundle\Entity\Trabajos $trabajos
     * @return GrupoAsignatura
     */
    public function addTrabajo(\Colegio\DocenteBundle\Entity\Trabajos $trabajos)
    {
        $this->trabajos[] = $trabajos;
    
        return $this;
    }

    /**
     * Remove trabajos
     *
     * @param \Colegio\DocenteBundle\Entity\Trabajos $trabajos
     */
    public function removeTrabajo(\Colegio\DocenteBundle\Entity\Trabajos $trabajos)
    {
        $this->trabajos->removeElement($trabajos);
    }

    /**
     * Add gruposBoletines
     *
     * @param \Colegio\BoletinBundle\Entity\GrupoBoletin $gruposBoletines
     * @return GrupoAsignatura
     */
    public function addGruposBoletine(\Colegio\BoletinBundle\Entity\GrupoBoletin $gruposBoletines)
    {
        $this->gruposBoletines[] = $gruposBoletines;
    
        return $this;
    }

    /**
     * Remove gruposBoletines
     *
     * @param \Colegio\BoletinBundle\Entity\GrupoBoletin $gruposBoletines
     */
    public function removeGruposBoletine(\Colegio\BoletinBundle\Entity\GrupoBoletin $gruposBoletines)
    {
        $this->gruposBoletines->removeElement($gruposBoletines);
    }

    /**
     * Get gruposBoletines
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGruposBoletines()
    {
        return $this->gruposBoletines;
    }

    /**
     * Add inscripcionAsignatura
     *
     * @param \Colegio\BoletinBundle\Entity\InscripcionAsignatura $inscripcionAsignatura
     * @return GrupoAsignatura
     */
    public function addInscripcionAsignatura(\Colegio\BoletinBundle\Entity\InscripcionAsignatura $inscripcionAsignatura)
    {
        $this->inscripcionAsignatura[] = $inscripcionAsignatura;
    
        return $this;
    }

    /**
     * Remove inscripcionAsignatura
     *
     * @param \Colegio\BoletinBundle\Entity\InscripcionAsignatura $inscripcionAsignatura
     */
    public function removeInscripcionAsignatura(\Colegio\BoletinBundle\Entity\InscripcionAsignatura $inscripcionAsignatura)
    {
        $this->inscripcionAsignatura->removeElement($inscripcionAsignatura);
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return GrupoAsignatura
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}