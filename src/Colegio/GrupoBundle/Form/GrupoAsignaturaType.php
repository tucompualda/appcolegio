<?php

namespace Colegio\GrupoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class GrupoAsignaturaType extends AbstractType
{
	public function __construct($colegio)
	{
            $this->colegio = $colegio;
            $this->hoy = new \DateTime('now');  
	}
     
     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $self = $this;
                     
        $builder
 
            ->add('idGrupo','entity',array(
                'class'=>'ColegioGrupoBundle:Grupo',
            	'query_builder'=> function(EntityRepository $er) use($self){
            			return $er->createQueryBuilder('g')
            			->where('g.colegio = :colegio AND g.fechaInicio <= :hoy AND g.fechaFin >= :hoy')
            			->setParameter('colegio', $self->colegio)
                                ->setParameter('hoy',$self->hoy);
            },	
            	'label' => 'Selecciona un grupo activo',
            	'empty_value' => 'Seleccione',
            	'required' => true,
            ))
            ->add('idAsignatura','entity',array(
                'class'=>'ColegioGrupoBundle:Asignatura',
            	'query_builder'=> function(EntityRepository $er) use($self){
            			return $er->createQueryBuilder('g')
            			->where('g.idColegio = :colegio')
            			->setParameter('colegio', $self->colegio);
            },	
            	'label' => 'Selecciona una Asignatura',
            	'empty_value' => 'Seleccione',
            	'required' => true,
            ))
            ->add('idDocente','entity',array(
                'class'=>'ColegioDocenteBundle:Docente',
            	'query_builder'=> function(EntityRepository $er) use($self){
            			return $er->createQueryBuilder('g')
            			->where('g.idColegio = :colegio and g.isActive = true')
            			->setParameter('colegio', $self->colegio);
            },	
            	'label' => 'Selecciona un Docente',
            	'empty_value' => 'Seleccione',
            	'required' => true,
            ))
            ->add('intensidadHoraria')
            ->add('isActive', null,array(
                'label'=>'Estado',
            ))            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\GrupoBundle\Entity\GrupoAsignatura'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_grupobundle_grupoasignatura';
    }
}
