<?php

namespace Colegio\GrupoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Colegio\GrupoBundle\Entity\Grupo;
use Colegio\GrupoBundle\Form\GrupoType;
use Colegio\AdminBundle\Entity\Sede;

/**
 * Grupo controller.
 *
 */
class GrupoController extends Controller {

    /**
     * Lists all Grupo entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();

        $entities = $em->getRepository('ColegioAdminBundle:Sede')->findColegio($idColegio);

        return $this->render('ColegioGrupoBundle:Grupo:index.html.twig', array(
                    'entities' => $entities,
                    'grupos' => null
        ));
    }

    /**
     * Creates a new Grupo entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Grupo();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $periodoAnual = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->getPeriodo($idColegio);

        //$grupos = new \Colegio\GrupoBundle\Entity\GrupoAsignatura();
        //$entity->getGruposAsignaturas()->add($grupos);
        $entity->setColegio($idColegio);
        $entity->setPeriodoEscolar($periodoAnual);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                    'notice', 'Bien hecho!');

            return $this->redirect($this->generateUrl('grupo_showbysede', array('id' => $entity->getIdSede())));
        }

        return $this->render('ColegioGrupoBundle:Grupo:new.html.twig', array(
                    'entities' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Grupo entity.
     *
     * @param Grupo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Grupo $entity)
    {
        $hoy = new \DateTime('W');
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $entity->setFechaInicio($hoy);
        
        $entity->setFechaFin($hoy);
        
        $form = $this->createForm(new GrupoType($idColegio), $entity, array(
            'action' => $this->generateUrl('grupo_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Grupo entity.
     *
     */
    public function newAction()
    {
        $entity = new Grupo();
        $form = $this->createCreateForm($entity);
        $fechahoy = new \DateTime('now');

        $entity->setFechaInicio($fechahoy);
        return $this->render('ColegioGrupoBundle:Grupo:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Grupo entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();


        $materias = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->findGrupos($id);
        $entity = $em->getRepository('ColegioGrupoBundle:Grupo')->findGrupos($id, $idColegio);

        $categoriaLogros = $em->getRepository('ColegioBoletinBundle:CategoriaLogro')->findByColegio($idColegio);
         
        //$periodos = $em->getRepository()
        $hoy = new \DateTime('now');
        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
        
        if($periodo==null){
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Por favor crea un Periodo académico');
            return $this->redirect($this->generateUrl('periodo'));
        }
        $idperiodo = $periodo->getId();
        
        $periodoAnual = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->getPeriodo($idColegio)->getId();

        $periodos = $em->getRepository('ColegioBoletinBundle:Periodo')->findBy(array(
            'periodoEscolar'=>$periodoAnual,
            'colegio'=>$idColegio
        ));


        if (!$entity)
        {
            throw $this->createNotFoundException('Unable to find Grupo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioGrupoBundle:Grupo:show.html.twig', array(
                    'categorias'=>$categoriaLogros,
                    'periodosTotal'=>$periodos,
                    'materias' => $materias,
                    'entity' => $entity,
                    'id' => $id,
                    'idPeriodo'=> $idperiodo,
                    'delete_form' => $deleteForm->createView(),));
    }

    public function groupBySedeAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $hoy = new \DateTime('now');
        
        if(!$id){
             
            $consulta = $em->createQuery('SELECT s FROM ColegioAdminBundle:Sede s 
                JOIN s.idDetalleColegio p
                WHERE p.idColegio = :colegio ORDER BY s.id ASC');
            $consulta->setParameter('colegio',$idColegio);
            $consulta->setMaxResults(1);
            $entities = $consulta->getOneOrNullResult();

            $id = $entities->getId();

            $sede = $em->getRepository('ColegioAdminBundle:Sede')->find($id);

             $gruposQuery = $em->createQuery('SELECT g,d,s,n FROM ColegioGrupoBundle:Grupo g 
                JOIN g.idDocenteResponsable d
                JOIN g.idSede s JOIN g.idNivel n
                WHERE g.colegio = :colegio 
                AND s.id = :id AND g.fechaInicio <= :hoy 
                AND g.fechaFin >= :hoy ORDER BY n.orden ASC
            ');
            $gruposQuery->setParameter('colegio', $idColegio);
            $gruposQuery->setParameter('id', $id);
            $gruposQuery->setParameter('hoy',$hoy);

           $entities = $em->getRepository('ColegioAdminBundle:Sede')->findColegio($idColegio);


        }else{
           
           $entities = $em->getRepository('ColegioAdminBundle:Sede')->findColegio($idColegio);
            $gruposQuery = $em->createQuery('SELECT g,d,s,n 
                FROM ColegioGrupoBundle:Grupo g JOIN g.idDocenteResponsable d
                JOIN g.idSede s JOIN g.idNivel n
                WHERE 
                g.colegio = :colegio 
                AND s.id = :id 
                AND g.fechaInicio <= :hoy 
                AND g.fechaFin >= :hoy ORDER BY n.orden ASC
            ');
            $gruposQuery->setParameter('colegio', $idColegio);
            $gruposQuery->setParameter('id', $id);
            $gruposQuery->setParameter('hoy',$hoy);

            $sede = $em->getRepository('ColegioAdminBundle:Sede')->find($id);

        }

           

            $grupos = $gruposQuery->getResult(); 

        return $this->render('ColegioGrupoBundle:Grupo:index.html.twig', array(
                    'idSede'=>$id,
                    'sede' => $sede,
                    'entities' => $entities,
                    'grupos' => $grupos));
    }

    /**
     * Displays a form to edit an existing Grupo entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();

        $entity = $em->getRepository('ColegioGrupoBundle:Grupo')->findOneBy(array(
            'id' => $id,
            'colegio' => $idColegio,
        ));

        if (!$entity)
        {
            throw $this->createNotFoundException('Unable to find Grupo entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioGrupoBundle:Grupo:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Grupo entity.
     *
     * @param Grupo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Grupo $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();


        $form = $this->createForm(new GrupoType($idColegio), $entity, array(
            'action' => $this->generateUrl('grupo_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }

    /**
     * Edits an existing Grupo entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioGrupoBundle:Grupo')->find($id);

        if (!$entity)
        {
            throw $this->createNotFoundException('Unable to find Grupo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                    'notice', 'Bien hecho!');
            return $this->redirect($this->generateUrl('grupo_edit', array('id' => $id)));
        }

        return $this->render('ColegioGrupoBundle:Grupo:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Grupo entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioGrupoBundle:Grupo')->find($id);

            if (!$entity)
            {
                throw $this->createNotFoundException('Unable to find Grupo entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                    'notice', 'Bien hecho!');
        }

        return $this->redirect($this->generateUrl('grupo'));
    }

    /**
     * Creates a form to delete a Grupo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('grupo_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Borrar'))
                        ->getForm()
        ;
    }

}
