<?php

namespace Colegio\GrupoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\GrupoBundle\Entity\GrupoAsignatura;
use Colegio\GrupoBundle\Form\GrupoAsignaturaType;

/**
 * GrupoAsignatura controller.
 *
 */
class GrupoAsignaturaController extends Controller
{

    /**
     * Lists all GrupoAsignatura entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $entities = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->findByColegio($idColegio);

        return $this->render('ColegioGrupoBundle:Default:grupos.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new GrupoAsignatura entity.
     *
     */
    public function createAction(Request $request,$grupo)
    {
        $entity = new GrupoAsignatura();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $entity->setColegio($idColegio);
        $form = $this->createCreateForm($entity,$grupo);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');
            return $this->redirect($this->generateUrl('grupoasignatura_show', array('id' => $entity->getId(), 'grupo' => $grupo)));
        }

        return $this->render('ColegioGrupoBundle:GrupoAsignatura:new.html.twig', array(
            'entity' => $entity,
            'grupo'  => $grupo,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a GrupoAsignatura entity.
    *
    * @param GrupoAsignatura $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(GrupoAsignatura $entity,$grupo)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $grupoEspecifico = $em->getRepository('ColegioGrupoBundle:Grupo')->findOneById($grupo);
        $entity->setIdGrupo($grupoEspecifico);
        $form = $this->createForm(new GrupoAsignaturaType($idColegio), $entity, array(
            'action' => $this->generateUrl('grupoasignatura_create',array('grupo'=>$grupo)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new GrupoAsignatura entity.
     *
     */
    public function newAction($grupo)
    {
        $entity = new GrupoAsignatura();
        $form   = $this->createCreateForm($entity,$grupo);

        return $this->render('ColegioGrupoBundle:GrupoAsignatura:new.html.twig', array(
            'entity' => $entity,
            'grupo'  => $grupo,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a GrupoAsignatura entity.
     *
     */
    public function showAction($id,$grupo)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();

        $entity = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->findOneBy(array(
            'id'=> $id,
            'colegio'=>$idColegio
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GrupoAsignatura entity.');
        }

        $deleteForm = $this->createDeleteForm($id,$grupo);

        return $this->render('ColegioGrupoBundle:GrupoAsignatura:show.html.twig', array(
            'entity'      => $entity,
            'grupo'       => $grupo,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing GrupoAsignatura entity.
     *
     */
    public function editAction($id,$grupo)
    {
        $em = $this->getDoctrine()->getManager();

        $gruposBoletin = $em->getRepository('ColegioBoletinBundle:GrupoBoletin')->findByGrupoAsignatura($id);

        $entity = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GrupoAsignatura entity.');
        }

        $editForm = $this->createEditForm($entity,$grupo);
        $deleteForm = $this->createDeleteForm($id,$grupo);

        return $this->render('ColegioGrupoBundle:GrupoAsignatura:edit.html.twig', array(
            'entity'        => $entity,
            'gruposBoletin' => $gruposBoletin, 
            'grupo'         => $grupo, 
            'edit_form'     => $editForm->createView(),
            'delete_form'   => $deleteForm->createView(),
            
        ));
    }

    /**
    * Creates a form to edit a GrupoAsignatura entity.
    *
    * @param GrupoAsignatura $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(GrupoAsignatura $entity,$grupo)
    {
    	$usuarioActivo = $this->get('security.context')->getToken()->getUser();
    	$idColegio = $usuarioActivo->getIdColegio();
        $form = $this->createForm(new GrupoAsignaturaType($idColegio), $entity, array(
            'action' => $this->generateUrl('grupoasignatura_update', array('id' => $entity->getId(),'grupo'=>$grupo)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing GrupoAsignatura entity.
     *
     */
    public function updateAction(Request $request, $id,$grupo)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GrupoAsignatura entity.');
        }

        $deleteForm = $this->createDeleteForm($id,$grupo);
        $editForm = $this->createEditForm($entity,$grupo);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
             $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');
            return $this->redirect($this->generateUrl('grupoasignatura_edit', array('id' => $id,'grupo'=>$grupo)));
        }

        return $this->render('ColegioGrupoBundle:GrupoAsignatura:edit.html.twig', array(
            'entity'      => $entity,
            'grupo'       => $grupo,  
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a GrupoAsignatura entity.
     *
     */
    public function deleteAction(Request $request, $id, $grupo)
    {
        $form = $this->createDeleteForm($id,$grupo);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find GrupoAsignatura entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Borraste una asignatura de un Grupo!');
        }

        return $this->redirect($this->generateUrl('grupo_show',array('id'=>$grupo)));
    }

    /**
     * Creates a form to delete a GrupoAsignatura entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id,$grupo)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('grupoasignatura_delete', array('id' => $id, 'grupo' => $grupo)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
