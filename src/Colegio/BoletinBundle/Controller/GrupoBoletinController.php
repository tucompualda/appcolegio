<?php

namespace Colegio\BoletinBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\BoletinBundle\Entity\GrupoBoletin;
use Colegio\BoletinBundle\Form\GrupoBoletinType;

/**
 * GrupoBoletin controller.
 *
 */
class GrupoBoletinController extends Controller
{

    /**
     * Lists all GrupoBoletin entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        //$entities = $em->getRepository('ColegioBoletinBundle:GrupoBoletin')->findByColegio($idColegio);
        $entidad = $em->createQuery('SELECT g,b,a FROM ColegioBoletinBundle:GrupoBoletin g JOIN g.boletin b JOIN g.grupoAsignatura a WHERE  g.colegio = :idColegio');  
        $entidad->setParameter('idColegio',$idColegio);      
        $entities = $entidad->getResult();

        return $this->render('ColegioBoletinBundle:GrupoBoletin:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new GrupoBoletin entity.
     *
     */
    public function createAction(Request $request)
    {
    	$entity = new GrupoBoletin();
    	//$em = $this->getDoctrine()->getManager();
    	$usuarioActivo = $this->get('security.context')->getToken()->getUser();
    	$idColegio = $usuarioActivo->getIdColegio();
    	$entity->setColegio($idColegio);
    	$form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('grupoboletin_show', array('id' => $entity->getId())));
        }

        return $this->render('ColegioBoletinBundle:GrupoBoletin:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a GrupoBoletin entity.
    *
    * @param GrupoBoletin $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(GrupoBoletin $entity)
    {
    	$em = $this->getDoctrine()->getManager();
    	$usuarioActivo = $this->get('security.context')->getToken()->getUser();
    	$idColegio = $usuarioActivo->getIdColegio();
        $form = $this->createForm(new GrupoBoletinType($idColegio), $entity, array(
            'action' => $this->generateUrl('grupoboletin_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new GrupoBoletin entity.
     *
     */
    public function newAction()
    {
        $entity = new GrupoBoletin();
        $form   = $this->createCreateForm($entity);

        return $this->render('ColegioBoletinBundle:GrupoBoletin:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a GrupoBoletin entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioBoletinBundle:GrupoBoletin')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GrupoBoletin entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioBoletinBundle:GrupoBoletin:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing GrupoBoletin entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioBoletinBundle:GrupoBoletin')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GrupoBoletin entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioBoletinBundle:GrupoBoletin:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a GrupoBoletin entity.
    *
    * @param GrupoBoletin $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(GrupoBoletin $entity)
    {
    	$em = $this->getDoctrine()->getManager();
    	$usuarioActivo = $this->get('security.context')->getToken()->getUser();
    	$idColegio = $usuarioActivo->getIdColegio();
        $form = $this->createForm(new GrupoBoletinType($idColegio), $entity, array(
            'action' => $this->generateUrl('grupoboletin_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing GrupoBoletin entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioBoletinBundle:GrupoBoletin')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GrupoBoletin entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('grupoboletin_edit', array('id' => $id)));
        }

        return $this->render('ColegioBoletinBundle:GrupoBoletin:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a GrupoBoletin entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioBoletinBundle:GrupoBoletin')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find GrupoBoletin entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('grupoboletin'));
    }

    /**
     * Creates a form to delete a GrupoBoletin entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('grupoboletin_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
