<?php

namespace Colegio\BoletinBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\BoletinBundle\Entity\InscripcionAsignatura;
use Colegio\BoletinBundle\Form\InscripcionAsignaturaType;

/**
 * InscripcionAsignatura controller.
 *
 */
class InscripcionAsignaturaController extends Controller
{

    /**
     * Lists all InscripcionAsignatura entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ColegioBoletinBundle:InscripcionAsignatura')->findAll();

        return $this->render('ColegioBoletinBundle:InscripcionAsignatura:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new InscripcionAsignatura entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new InscripcionAsignatura();
        $hoy = new \DateTime('now');
        $entity->setFechaInscripcion($hoy);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('inscripcionasignatura_show', array('id' => $entity->getId())));
        }

        return $this->render('ColegioBoletinBundle:InscripcionAsignatura:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a InscripcionAsignatura entity.
    *
    * @param InscripcionAsignatura $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(InscripcionAsignatura $entity)
    {
        $form = $this->createForm(new InscripcionAsignaturaType(), $entity, array(
            'action' => $this->generateUrl('inscripcionasignatura_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new InscripcionAsignatura entity.
     *
     */
    public function newAction()
    {
        $entity = new InscripcionAsignatura();
        $form   = $this->createCreateForm($entity);

        return $this->render('ColegioBoletinBundle:InscripcionAsignatura:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a InscripcionAsignatura entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioBoletinBundle:InscripcionAsignatura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find InscripcionAsignatura entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioBoletinBundle:InscripcionAsignatura:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing InscripcionAsignatura entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioBoletinBundle:InscripcionAsignatura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find InscripcionAsignatura entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioBoletinBundle:InscripcionAsignatura:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a InscripcionAsignatura entity.
    *
    * @param InscripcionAsignatura $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(InscripcionAsignatura $entity)
    {
        $form = $this->createForm(new InscripcionAsignaturaType(), $entity, array(
            'action' => $this->generateUrl('inscripcionasignatura_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing InscripcionAsignatura entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioBoletinBundle:InscripcionAsignatura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find InscripcionAsignatura entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('inscripcionasignatura_edit', array('id' => $id)));
        }

        return $this->render('ColegioBoletinBundle:InscripcionAsignatura:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a InscripcionAsignatura entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioBoletinBundle:InscripcionAsignatura')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find InscripcionAsignatura entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('inscripcionasignatura'));
    }

    /**
     * Creates a form to delete a InscripcionAsignatura entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('inscripcionasignatura_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
