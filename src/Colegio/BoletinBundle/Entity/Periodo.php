<?php

namespace Colegio\BoletinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Periodo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\BoletinBundle\Entity\PeriodoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Periodo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="periodo", type="string", length=255)
     */
    private $periodo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechacreacion", type="datetime")
     */
    private $fechaCreacion;   
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaedicion", type="datetime")
     */
    private $fechaEdicion;     
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaInicio", type="datetime")
     */
    private $fechaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaFin", type="datetime")
     */
    private $fechaFin;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Colegio")
     */
    private $colegio;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\PeriodoEscolar")
     */
    private $periodoEscolar;

    /**
     * @var string
     * 
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\TipoCalificacion")
     *  
     */
    private $tipoCalificacion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="porcentajeestandar", type="boolean", nullable=true)
     */
    private $porcentajeEstandar;    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean", nullable=true)
     */
    private $establecer;

     /**
     * @var boolean
     *
     * @ORM\Column(name="fortaleza", type="boolean", nullable=true)
     */
    private $fortaleza;

     /**
     * @var boolean
     *
     * @ORM\Column(name="debilidad", type="boolean", nullable=true)
     */
    private $debilidad;


    /**
     * @var boolean
     *
     * @ORM\Column(name="boletinDocente", type="boolean", nullable=true)
     */
    private $boletinDocente = false;
  
      /**
     * @var boolean
     *
     * @ORM\Column(name="crearBoletinDefecto", type="boolean", nullable=true)
     */
    private $crearBoletinDefecto;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cerrarPeriodo", type="boolean", nullable=true)
     */
    private $cerrarPeriodo;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set periodo
     *
     * @param string $periodo
     * @return Periodo
     */
    public function setPeriodo($periodo)
    {
        $this->periodo = $periodo;
    
        return $this;
    }

    /**
     * Get periodo
     *
     * @return string 
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return Periodo
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    
        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return Periodo
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    
        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }
    
     /**
     * Set colegio
     *
     * @param string $colegio
     * @return Periodo
     */
    public function setColegio(\Colegio\AdminBundle\Entity\Colegio $colegio)
    {
        $this->colegio = $colegio;
    
        return $this;
    }

    /**
     * Get colegio
     *
     * @return string 
     */
    public function getColegio()
    {
        return $this->colegio;
    }
    
    /**
     * Set periodoEscolar
     *
     * @param string $periodoEscolar
     * @return Periodo
     */
    public function setPeriodoEscolar(\Colegio\AdminBundle\Entity\PeriodoEscolar $periodoEscolar)
    {
        $this->periodoEscolar = $periodoEscolar;
    
        return $this;
    }

    /**
     * Get tipoCalificacion
     *
     * @return string 
     */
    public function getTipoCalificacion()
    {
        return $this->tipoCalificacion;
    }

 /**
     * Set tipoCalificacion
     *
     * @param string $tipoCalificacion
     * @return Periodo
     */
    public function setTipoCalificacion(\Colegio\AdminBundle\Entity\TipoCalificacion $tipoCalificacion)
    {
        $this->tipoCalificacion = $tipoCalificacion;
    
        return $this;
    }

    /**
     * Get periodoEscolar
     *
     * @return string 
     */
    public function getPeriodoEscolar()
    {
        return $this->periodoEscolar;
    }

    public function __toString()
    {
        return $this->getPeriodo()." (".$this->getPeriodoEscolar().")";
    }

    /**
     * Set porcentajeEstandar
     *
     * @param boolean $porcentajeEstandar
     * @return Periodo
     */
    public function setPorcentajeEstandar($porcentajeEstandar)
    {
        $this->porcentajeEstandar = $porcentajeEstandar;
    
        return $this;
    }

    /**
     * Get porcentajeEstandar
     *
     * @return boolean 
     */
    public function getPorcentajeEstandar()
    {
        return $this->porcentajeEstandar;
    }

    /**
     * Set establecer
     *
     * @param boolean $establecer
     * @return Periodo
     */
    public function setEstablecer($establecer)
    {
        $this->establecer = $establecer;
    
        return $this;
    }

    /**
     * Get establecer
     *
     * @return boolean 
     */
    public function getEstablecer()
    {
        return $this->establecer;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return Periodo
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;
    
        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaEdicion
     *
     * @param \DateTime $fechaEdicion
     * @return Periodo
     */
    public function setFechaEdicion($fechaEdicion)
    {
        $this->fechaEdicion = $fechaEdicion;
    
        return $this;
    }

    /**
     * Get fechaEdicion
     *
     * @return \DateTime 
     */
    public function getFechaEdicion()
    {
        return $this->fechaEdicion;
    }
    
    
    /**
     * @ORM\PrePersist
     */
    public function setCreatedValue()
    {
        $this->fechaCreacion = new \DateTime();
        $this->fechaEdicion = new \DateTime();
    }
    
    /** 
     * @ORM\PreUpdate 
     */  
    public function setUpdatedAt()  
    {  
        $this->fechaEdicion = new \DateTime();  
    }  
    

    /**
     * Set boletinDocente
     *
     * @param boolean $boletinDocente
     * @return Periodo
     */
    public function setBoletinDocente($boletinDocente)
    {
        $this->boletinDocente = $boletinDocente;
    
        return $this;
    }

    /**
     * Get boletinDocente
     *
     * @return boolean 
     */
    public function getBoletinDocente()
    {
        return $this->boletinDocente;
    }

    /**
     * Set fortaleza
     *
     * @param boolean $fortaleza
     * @return Periodo
     */
    public function setFortaleza($fortaleza)
    {
        $this->fortaleza = $fortaleza;
    
        return $this;
    }

    /**
     * Get fortaleza
     *
     * @return boolean 
     */
    public function getFortaleza()
    {
        return $this->fortaleza;
    }

    /**
     * Set debilidad
     *
     * @param boolean $debilidad
     * @return Periodo
     */
    public function setDebilidad($debilidad)
    {
        $this->debilidad = $debilidad;
    
        return $this;
    }

    /**
     * Get debilidad
     *
     * @return boolean 
     */
    public function getDebilidad()
    {
        return $this->debilidad;
    }

    /**
     * Set cerrarPeriodo
     *
     * @param boolean $cerrarPeriodo
     * @return Periodo
     */
    public function setCerrarPeriodo($cerrarPeriodo)
    {
        $this->cerrarPeriodo = $cerrarPeriodo;
    
        return $this;
    }

    /**
     * Get cerrarPeriodo
     *
     * @return boolean 
     */
    public function getCerrarPeriodo()
    {
        return $this->cerrarPeriodo;
    }

    /**
     * Set crearBoletinDefecto
     *
     * @param boolean $crearBoletinDefecto
     * @return Periodo
     */
    public function setCrearBoletinDefecto($crearBoletinDefecto)
    {
        $this->crearBoletinDefecto = $crearBoletinDefecto;
    
        return $this;
    }

    /**
     * Get crearBoletinDefecto
     *
     * @return boolean 
     */
    public function getCrearBoletinDefecto()
    {
        return $this->crearBoletinDefecto;
    }
}