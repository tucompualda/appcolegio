<?php

namespace Colegio\BoletinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Logro
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Logro
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Colegio\BoletinBundle\Entity\Boletin", inversedBy="logros", cascade={"persist"})
     * @ORM\JoinColumn(name="idBoletin_id", referencedColumnName="id")
     */
    private $idBoletin;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\BoletinBundle\Entity\CategoriaLogro")
     */
    private $categoria;    
    
    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;


   /**
     * @ORM\OneToMany(targetEntity="Colegio\EstudianteBundle\Entity\NotaLogro", mappedBy="logro", cascade={"persist"})
     * @Assert\Valid()
     */
    private $notalogro;

    /**
     * @ORM\ManyToMany(targetEntity="Colegio\DocenteBundle\Entity\Trabajos", mappedBy="logros")
     */
    private $trabajos;

   /**
	*public function __construct()
    {
        $this->notalogro = new ArrayCollection();
    }
   */
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idBoletin
     *
     * @param \Colegio\BoletinBundle\Entity\Boletin $idBoletin
     * @return Logro
     */
    public function setIdBoletin(\Colegio\BoletinBundle\Entity\Boletin $idBoletin=null)
    {
        $this->idBoletin = $idBoletin;
    
        return $this;
    }

    /**
     * Get idBoletin
     *
     * @return \Colegio\BoletinBundle\Entity\Boletin
     */
    public function getIdBoletin()
    {
        return $this->idBoletin;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Logro
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
     
    /**
     * Set categoria
     *
     * @param \Colegio\BoletinBundle\Entity\CategoriaLogro $categoria
     * @return Logro
     */
    public function setCategoria(\Colegio\BoletinBundle\Entity\CategoriaLogro $categoria=null)
    {
        $this->categoria = $categoria;
    
        return $this;
    }

    /**
     * Get categoria
     *
     * @return \Colegio\BoletinBundle\Entity\CategoriaLogro
     */
    public function getCategoria()
    {
        return $this->categoria;
    }


    /**
     * Set notalogro
     *
     * @return \Colegio\EstudianteBundle\Entity\NotaLogro
     */
    public function setNotalogro($notalogro)
    {
        
    }

    /**
     * Get notalogro
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotalogro()
    {
        return $this->notalogro;
    }

    
    public function __toString()
    {
        return $this->getDescripcion();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notalogro = new \Doctrine\Common\Collections\ArrayCollection();
        $this->trabajos = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add notalogro
     *
     * @param \Colegio\EstudianteBundle\Entity\NotaLogro $notalogro
     * @return Logro
     */
    public function addNotalogro(\Colegio\EstudianteBundle\Entity\NotaLogro $notalogro)
    {
        $this->notalogro[] = $notalogro;
    
        return $this;
    }

    /**
     * Remove notalogro
     *
     * @param \Colegio\EstudianteBundle\Entity\NotaLogro $notalogro
     */
    public function removeNotalogro(\Colegio\EstudianteBundle\Entity\NotaLogro $notalogro)
    {
        $this->notalogro->removeElement($notalogro);
    }

    /**
     * Add trabajos
     *
     * @param \Colegio\DocenteBundle\Entity\Trabajos $trabajos
     * @return Logro
     */
    public function addTrabajo(\Colegio\DocenteBundle\Entity\Trabajos $trabajos)
    {
        $this->trabajos[] = $trabajos;
    
        return $this;
    }

    /**
     * Remove trabajos
     *
     * @param \Colegio\DocenteBundle\Entity\Trabajos $trabajos
     */
    public function removeTrabajo(\Colegio\DocenteBundle\Entity\Trabajos $trabajos)
    {
        $this->trabajos->removeElement($trabajos);
    }

    /**
     * Get trabajos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTrabajos()
    {
        return $this->trabajos;
    }
}