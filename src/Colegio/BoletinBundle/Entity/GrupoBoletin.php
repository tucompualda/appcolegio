<?php

namespace Colegio\BoletinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GrupoBoletin
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\BoletinBundle\Entity\GrupoBoletinRepository")
 */
class GrupoBoletin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\BoletinBundle\Entity\Boletin", cascade={"persist","remove"}, inversedBy="gruposBoletin")
     * @ORM\JoinColumn(name="boletin_id", referencedColumnName="id")
     */
    private $boletin;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Colegio\GrupoBundle\Entity\GrupoAsignatura", inversedBy="gruposBoletines",cascade={"persist"})
     * @ORM\JoinColumn(name="grupoasignatura_id", referencedColumnName="id")
     */
    private $grupoAsignatura;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Colegio\BoletinBundle\Entity\Periodo")
     * @ORM\JoinColumn(name="periodo_id", referencedColumnName="id")
     */
    private $periodo;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Colegio")
     */
    private $colegio;

    /**
     * @ORM\OneToMany(targetEntity="Colegio\EstudianteBundle\Entity\Nota", mappedBy="grupoBoletin", cascade={"persist"})
     */
    private $notas;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set boletin
     *
     * @param \Colegio\BoletinBundle\Entity\Boletin $boletin
     * @return GrupoBoletin
     */
    public function setBoletin(\Colegio\BoletinBundle\Entity\Boletin $boletin)
    {
        $this->boletin = $boletin;
    
        return $this;
    }

    /**
     * Get boletin
     *
     * @return \Colegio\BoletinBundle\Entity\Boletin 
     */
    public function getBoletin()
    {
        return $this->boletin;
    }

    /**
     * Set grupoAsignatura
     *
     * @param \Colegio\GrupoBundle\Entity\GrupoAsignatura $grupoAsignatura
     * @return GrupoBoletin
     */
    public function setGrupoAsignatura(\Colegio\GrupoBundle\Entity\GrupoAsignatura $grupoAsignatura)
    {
        $this->grupoAsignatura = $grupoAsignatura;
    
        return $this;
    }

    /**
     * Get grupoAsignatura
     *
     * @return \Colegio\GrupoBundle\Entity\GrupoAsignatura
     */
    public function getGrupoAsignatura()
    {
        return $this->grupoAsignatura;
    }

    /**
     * Set periodo
     *
     * @param \Colegio\BoletinBundle\Entity\Periodo $periodo
     * @return GrupoBoletin
     */
    public function setPeriodo(\Colegio\BoletinBundle\Entity\Periodo $periodo)
    {
        $this->periodo = $periodo;
    
        return $this;
    }

    /**
     * Get periodo
     *
     * @return \Colegio\BoletinBundle\Entity\Periodo
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }
    /**
     * Set colegio
     *
     * @param string $colegio
     * @return GrupoBoletin
     */
    public function setColegio(\Colegio\AdminBundle\Entity\Colegio $colegio)
    {
    	$this->colegio = $colegio;
    
    	return $this;
    }
    
    /**
     * Get colegio
     *
     * @return string
     */
    public function getColegio()
    {
    	return $this->colegio;
    }
    
    public function __toString()
    {
    	return $this->getGrupoAsignatura() ." ". $this->getPeriodo();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notas = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add notas
     *
     * @param \Colegio\EstudianteBundle\Entity\Nota $notas
     * @return GrupoBoletin
     */
    public function addNota(\Colegio\EstudianteBundle\Entity\Nota $notas)
    {
        $this->notas[] = $notas;
    
        return $this;
    }

    /**
     * Remove notas
     *
     * @param \Colegio\EstudianteBundle\Entity\Nota $notas
     */
    public function removeNota(\Colegio\EstudianteBundle\Entity\Nota $notas)
    {
        $this->notas->removeElement($notas);
    }

    /**
     * Get notas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotas()
    {
        return $this->notas;
    }
}