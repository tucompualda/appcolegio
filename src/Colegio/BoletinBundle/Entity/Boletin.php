<?php

namespace Colegio\BoletinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Boletin
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\BoletinBundle\Entity\BoletinRepository")
 */
class Boletin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @var string
    *
    * @ORM\ManyToOne(targetEntity="Colegio\GrupoBundle\Entity\Asignatura", inversedBy="asignaturasboletin", cascade={"persist","remove"})
    * @ORM\JoinColumn(name="asignatura_id", referencedColumnName="id")
    */
    private $asignatura;
  
    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaInicio", type="datetime")
     */
    private $fechaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaFin", type="datetime")
     */
    private $fechaFin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean", nullable=true)
     */
    private $activo;

    /**
     * @ORM\OneToMany(targetEntity="Colegio\BoletinBundle\Entity\Logro", mappedBy="idBoletin", cascade={"persist","remove"})
     * @Assert\Valid()    
     */
    private $logros;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Colegio")
     */
    private $colegio;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\BoletinBundle\Entity\Periodo")
     */
    private $periodo;
    
    /**
     * @ORM\OneToMany(targetEntity="Colegio\BoletinBundle\Entity\GrupoBoletin", mappedBy="boletin", cascade={"persist","remove"})
     * 
     */
    private $gruposBoletin;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set asignatura
     *
     * @param \Colegio\GrupoBundle\Entity\Asignatura $asignatura
     * @return Sede
     */
    public function setAsignatura(\Colegio\GrupoBundle\Entity\Asignatura $asignatura = null)
    {
        $this->asignatura = $asignatura;
    
        return $this;
    }

    /**
     * Get asignatura
     *
     * @return \Colegio\GrupoBundle\Entity\Asignatura
     */
    public function getAsignatura()
    {
        return $this->asignatura;
    }
    
  /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Boletin
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

  
  
    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return Boletin
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    
        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return Boletin
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    
        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Boletin
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    
        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set logros
     *
     * @return \Colegio\BoletinBundle\Entity\Logro
     */
    public function setLogros($logros)
    {
        $this->logros = $logros;
        foreach ($logros as $logro) {
            $logro->setIdBoletin($this);
        }
    }

    /**
     * Get logros
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLogros()
    {
        return $this->logros;
    }

   // public function removeTag(Tag $tag)
   // {
   //     $this->tags->removeElement($tag);
   // }
    
    /**
     * Set colegio
     *
     * @param string $colegio
     * @return Boletin
     */
    public function setColegio(\Colegio\AdminBundle\Entity\Colegio $colegio)
    {
        $this->colegio = $colegio;
    
        return $this;
    }

    /**
     * Get colegio
     *
     * @return string 
     */
    public function getColegio()
    {
        return $this->colegio;
    }
    
    
    /**
     * Set periodo
     *
     * @param string $periodo
     * @return Boletin
     */
    public function setPeriodo(\Colegio\BoletinBundle\Entity\Periodo $periodo)
    {
        $this->periodo = $periodo;
    
        return $this;
    }

    /**
     * Get periodo
     *
     * @return string 
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }
    
    
    public function __toString() 
    {
        return $this->getAsignatura(). " " . $this->getPeriodo(). " ".$this->getDescripcion();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->logros = new \Doctrine\Common\Collections\ArrayCollection();
        $this->gruposBoletin = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add logros
     *
     * @param \Colegio\BoletinBundle\Entity\Logro $logros
     * @return Boletin
     */
    public function addLogro(\Colegio\BoletinBundle\Entity\Logro $logros)
    {
        $this->logros[] = $logros;
    
        return $this;
    }

    /**
     * Remove logros
     *
     * @param \Colegio\BoletinBundle\Entity\Logro $logros
     */
    public function removeLogro(\Colegio\BoletinBundle\Entity\Logro $logros)
    {
        $this->logros->removeElement($logros);
    }

    /**
     * Add gruposBoletin
     *
     * @param \Colegio\BoletinBundle\Entity\GrupoBoletin $gruposBoletin
     * @return Boletin
     */
    public function addGruposBoletin(\Colegio\BoletinBundle\Entity\GrupoBoletin $gruposBoletin)
    {
        $this->gruposBoletin[] = $gruposBoletin;
    
        return $this;
    }

    /**
     * Remove gruposBoletin
     *
     * @param \Colegio\BoletinBundle\Entity\GrupoBoletin $gruposBoletin
     */
    public function removeGruposBoletin(\Colegio\BoletinBundle\Entity\GrupoBoletin $gruposBoletin)
    {
        $this->gruposBoletin->removeElement($gruposBoletin);
    }

    /**
     * Get gruposBoletin
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGruposBoletin()
    {
        return $this->gruposBoletin;
    }
}