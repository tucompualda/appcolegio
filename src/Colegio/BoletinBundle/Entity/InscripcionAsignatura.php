<?php

namespace Colegio\BoletinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * InscripcionAsignatura
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Colegio\BoletinBundle\Entity\InscripcionAsignaturaRepository")
 */
class InscripcionAsignatura
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\EstudianteBundle\Entity\GrupoEstudiante")
     */
    private $matricula;
    
    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Colegio\GrupoBundle\Entity\GrupoAsignatura", inversedBy="inscripcionAsignatura", cascade={"persist"}))
     * @ORM\JoinColumn(name="grupoasignatura_id", referencedColumnName="id")
     */
    private $grupoAsignatura;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaInscripcion", type="datetime")
     */
    private $fechaInscripcion;

    /**
     * @ORM\Column(name="acumulado", type="float", nullable=true)
     */
    private $acumulado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaAcumulado", type="datetime", nullable=true)
     */
    private $fechaAcumulado;

    /**
     * @var string
     * @ORM\Column(name="descripcionAcumulado", type="text", nullable=true)
     */
    private $descripcionAcumulado;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set matricula
     *
     * @param \Colegio\EstudianteBundle\Entity\GrupoEstudiante $matricula
     * @return InscripcionAsignatura
     */
    public function setMatricula(\Colegio\EstudianteBundle\Entity\GrupoEstudiante $matricula)
    {
        $this->matricula = $matricula;
    
        return $this;
    }

    /**
     * Get matricula
     *
     * @return \Colegio\EstudianteBundle\Entity\GrupoEstudiante 
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * Set grupoAsignatura
     *
     * @param \Colegio\GrupoBundle\Entity\GrupoAsignatura $grupoAsignatura
     * @return GrupoBoletin
     */
    public function setGrupoAsignatura(\Colegio\GrupoBundle\Entity\GrupoAsignatura $grupoAsignatura)
    {
        $this->grupoAsignatura = $grupoAsignatura;
    
        return $this;
    }

    /**
     * Get grupoAsignatura
     *
     * @return \Colegio\GrupoBundle\Entity\GrupoAsignatura
     */
    public function getGrupoAsignatura()
    {
        return $this->grupoAsignatura;
    }

    /**
     * Set fechaInscripcion
     *
     * @param \DateTime $fechaInscripcion
     * @return InscripcionAsignatura
     */
    public function setFechaInscripcion($fechaInscripcion)
    {
        $this->fechaInscripcion = $fechaInscripcion;
    
        return $this;
    }

    /**
     * Get fechaInscripcion
     *
     * @return \DateTime 
     */
    public function getFechaInscripcion()
    {
        return $this->fechaInscripcion;
    }

    /**
     * Set acumulado
     *
     * @param float $acumulado
     * @return InscripcionAsignatura
     */
    public function setAcumulado($acumulado)
    {
        $this->acumulado = $acumulado;
    
        return $this;
    }

    /**
     * Get acumulado
     *
     * @return float 
     */
    public function getAcumulado()
    {
        return $this->acumulado;
    }

    /**
     * Set fechaAcumulado
     *
     * @param \DateTime $fechaAcumulado
     * @return InscripcionAsignatura
     */
    public function setFechaAcumulado($fechaAcumulado)
    {
        $this->fechaAcumulado = $fechaAcumulado;
    
        return $this;
    }

    /**
     * Get fechaAcumulado
     *
     * @return \DateTime 
     */
    public function getFechaAcumulado()
    {
        return $this->fechaAcumulado;
    }

      /**
     * @ORM\PrePersist
     */
    public function setCreatedValue()
    {
        $this->setFechaAcumulado(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdateValue()
    {
        $this->setFechaAcumulado(new \DateTime());
    }

    public function __ToString()
    {
        return $this->getMatricula()." -".$this->getGrupoAsignatura();
    }

    /**
     * Set descripcionAcumulado
     *
     * @param string $descripcionAcumulado
     * @return InscripcionAsignatura
     */
    public function setDescripcionAcumulado($descripcionAcumulado)
    {
        $this->descripcionAcumulado = $descripcionAcumulado;
    
        return $this;
    }

    /**
     * Get descripcionAcumulado
     *
     * @return string 
     */
    public function getDescripcionAcumulado()
    {
        return $this->descripcionAcumulado;
    }
}