<?php

namespace Colegio\BoletinBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class LogroType extends AbstractType
{
    public function __construct($idColegio)
    {
        //recibe el idcolegio del usuario activo, no la sede como aparenta
        $this->idColegio = $idColegio;
    } 
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $self = $this;
        $builder
            ->add('categoria','entity',array(
                'class'=>'ColegioBoletinBundle:CategoriaLogro',
                'query_builder'=>function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('c')
                                ->where('c.colegio = :idColegio')
                                ->setParameter('idColegio', $self->idColegio);
                },
                'attr'=>array('empty_value'=>'seel'),
            ))    
            ->add('descripcion','textarea')
            ->add('idBoletin','entity',array(
                'class'=>'ColegioBoletinBundle:Boletin',
                'query_builder'=>function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('b')
                                ->where('b.colegio = :idColegio')
                                ->setParameter('idColegio', $self->idColegio);
                }
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\BoletinBundle\Entity\Logro'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_boletinbundle_logro';
    }
}
