<?php

namespace Colegio\BoletinBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class PeriodoType extends AbstractType
{
    public function __construct($idColegio) 
    {
        $this->idColegio = $idColegio;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $self = $this;
        $builder
//            ->add('periodoEscolar')
            ->add('tipoCalificacion',null,array(
                   'empty_value'=>false 
                ))
            ->add('crearBoletinDefecto')
            ->add('colegio','entity',array(
                'class'=>'ColegioAdminBundle:Colegio',
                'query_builder'=>function(EntityRepository $er) use($self){
                    return $er->CreateQueryBuilder('u')
                            ->where('u.id = :idColegio')
                            ->setParameter('idColegio',$self->idColegio);
                },
                 'label'      => 'Mi colegio',
                 'required'   =>true))    
            ->add('periodo')
            ->add('fechaInicio','date',array(
                'input'=>'datetime',
                'widget'=>'choice',
            ))
            ->add('fechaFin')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\BoletinBundle\Entity\Periodo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_boletinbundle_periodo';
    }
}
