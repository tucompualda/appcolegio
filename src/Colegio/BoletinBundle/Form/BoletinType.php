<?php

namespace Colegio\BoletinBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class BoletinType extends AbstractType
{
    public function __construct($sede)
    {
        //recibe el idcolegio del usuario activo, no la sede como aparenta
        $this->sede = $sede;
    } 
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $self = $this; 
        $builder
            ->add('periodo','entity',array(
                'class'=>'ColegioBoletinBundle:Periodo',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('p')
                                ->where('p.colegio = :sede')
                                ->setParameter('sede', $self->sede);
                } 
            )) 
            ->add('descripcion','textarea',array(
				'required'=>true,
            ))   
            ->add('asignatura','entity',array(
                'class'=>'ColegioGrupoBundle:Asignatura',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('a')
                                ->where('a.idColegio = :sede')
                                ->setParameter('sede', $self->sede);
                },
                 'label'=>'Materia',
                 'empty_value'=>'Escoge la materia',
                 'required'=> true,
            ))    
            
            ->add('fechaInicio')
            ->add('fechaFin')
            ->add('activo')
            //->add('idGrupoAsignatura',null,array(
            //    'label'=>'Grupos Asignatura'
            //))
            //->add('logros', 'collection',array(
              //  'type'         => new LogroType(),
                //'allow_add'    => true,
                //'by_reference' => false,
                //'allow_delete' => true,
                //))    
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\BoletinBundle\Entity\Boletin',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_boletinbundle_boletin';
    }
}
