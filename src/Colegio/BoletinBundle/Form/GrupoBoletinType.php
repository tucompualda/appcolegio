<?php

namespace Colegio\BoletinBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class GrupoBoletinType extends AbstractType
{
	/*
	 * Los datos del constructor son $sede = $idColegio y $colegio = $detalleColegio estos se necesitan
	* para filtrar correctamente los docentes y las sedes respectivamente
	*/
	public function __construct($colegio)
	{
		$this->colegio = $colegio;
	}
	
	/**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$self = $this;
    	$builder
            ->add('boletin','entity',array(
                'class' => 'ColegioBoletinBundle:Boletin',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('u')
                                ->where('u.colegio = :colegio')
                                ->setParameter('colegio', $self->colegio);
                },
                 'label'=>'Boletin',
                 'empty_value'=>'Escoge un boletín',
                 'required'=> true,
            ))
            ->add('grupoAsignatura','entity',array(
                'class' => 'ColegioGrupoBundle:GrupoAsignatura',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('g')
                                ->where('g.colegio = :colegio')
                                ->setParameter('colegio', $self->colegio);
                },
                 'label'=>'Escoge un grupo Asignatura',
                 'empty_value'=>'...',
                 'required'=> true,
            ))
            ->add('periodo','entity',array(
                'class' => 'ColegioBoletinBundle:Periodo',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('p')
                                ->where('p.colegio = :colegio')
                                ->setParameter('colegio', $self->colegio);
                },
                 'label'=>'Periodo',
                 'empty_value'=>'Escoge un periodo',
                 'required'=> true,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\BoletinBundle\Entity\GrupoBoletin'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_boletinbundle_grupoboletin';
    }
}
