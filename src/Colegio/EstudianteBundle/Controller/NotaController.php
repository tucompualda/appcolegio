<?php

namespace Colegio\EstudianteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\EstudianteBundle\Entity\Nota;
use Colegio\AdminBundle\Form\NotaType;
use Colegio\EstudianteBundle\Form\NoticasType;


/**
 * Nota controller.
 *
 */
class NotaController extends Controller
{

    /**
     * Lists all Nota entities.
     *
     */
    public function indexAction( Request $request,$grupo,$sede,$parametros )
    {

        $em = $this->getDoctrine()->getManager();
		$usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $colegio = $usuarioActivo->getIdColegio();
        
        $datos = $request->request->all();
        $sedes = $sede;
        $periodo = $parametros;
        foreach($datos as $otros)
        {
        $sedes   = $otros['sede'];
        $periodo = $otros['parametros'];
        } 
        
        $parametrosQuery = $em->createQuery('SELECT p,e FROM ColegioAdminBundle:Parametros p JOIN p.periodoEscolar e WHERE p.id = :id');
        $parametrosQuery->setParameter('id',$periodo);
        $parametros = $parametrosQuery->getResult();

        $periodoEscolar="";
        foreach ( $parametros as $key ) 
        {
        $periodoEscolar = $key->getPeriodoEscolar();
        }
        

        $fechaInicio = null;
        $fechaFin    = null;

        if($parametros == null)
        {

        }
        else
        {   
        $fechaInicio = $parametros[0]->getPeriodoEscolar()->getFechaInicio();
        $fechaFin = $parametros[0]->getPeriodoEscolar()->getFechaFin();
        }

        $gruposQuery = $em->createQuery('SELECT g,n FROM ColegioGrupoBundle:Grupo g JOIN g.idNivel n
            WHERE g.idSede = :sede AND g.colegio = :colegio
             AND g.fechaInicio >= :fechaInicio AND g.fechaFin <= :fechaFin OR g.periodoEscolar = :periodoEscolar
             ORDER BY n.orden ASC
            ');
        $gruposQuery->setParameter('sede',$sedes);
        $gruposQuery->setParameter('colegio',$colegio);
        $gruposQuery->setParameter('fechaInicio',$fechaInicio);
        $gruposQuery->setParameter('fechaFin',$fechaFin);
        $gruposQuery->setParameter('periodoEscolar',$periodoEscolar);

        $grupos = $gruposQuery->getResult();

        $estudiantesQuery = $em->createQuery('SELECT g,e FROM ColegioEstudianteBundle:GrupoEstudiante g JOIN g.idEstudiante e
                        WHERE g.idGrupo = :grupo ORDER BY e.primerApellido ASC
            ');
        $estudiantesQuery->setParameter('grupo',$grupo);
        $estudiantes = $estudiantesQuery->getResult();


        $defecto = null;
        $form = $this->createForm(new NoticasType($colegio),$defecto,array(
            //'action' => $this->generateUrl('nota'),
            'method' => 'POST',
         )); 
        $form->add('submit', 'submit', array('label' => 'Buscar Grupos'));
        $datogrupos = $em->getRepository('ColegioGrupoBundle:Grupo')->findOneById($grupo);
        
        return $this->render('ColegioEstudianteBundle:Nota:index.html.twig',array(
                'form'          => $form->createView(),
                'grupos'        => $grupos,
                'sedes'         => $sedes,
                'periodoEscolar'=> $periodoEscolar,
                'periodo'       => $periodo,
                'fechaInicio'   => $fechaInicio,
                'fechaFin'      => $fechaFin,
                'estudiantes'   => $estudiantes,
                'grupo'         => $grupo,
                'datogrupos'    => $datogrupos,
            ));
    }

    public function estudianteAction(Request $request,$matricula,$parametros,$grupo, $calificacioncilla)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $colegio = $usuarioActivo->getIdColegio();

        ///datos del estudiante ////
        $datos = $em->createQuery('SELECT m,e FROM ColegioEstudianteBundle:GrupoEstudiante m JOIN m.idEstudiante e 
            WHERE m.id = :id AND m.colegio = :colegio');
        $datos->setParameter('id',$matricula);
        $datos->setParameter('colegio',$colegio);
        $estudiante = $datos->getSingleResult();

        // datos del grupo
        $grupoQuery = $em->createQuery('SELECT g FROM ColegioGrupoBundle:Grupo g WHERE
                g.id = :id AND g.colegio = :colegio
            ');
        $grupoQuery->setParameter('id',$grupo);
        $grupoQuery->setParameter('colegio',$colegio);
        $datosGrupo = $grupoQuery->getSingleResult();

        // datos del año o parametro
        $parametroQuery = $em->createQuery('SELECT p,e FROM ColegioAdminBundle:Parametros p JOIN p.periodoEscolar e 
                    WHERE p.id = :id AND p.colegio = :colegio
            ');
        $parametroQuery->setParameter('id',$parametros);
        $parametroQuery->setParameter('colegio',$colegio);
        $datoParametro = $parametroQuery->getSingleResult();

        $extra = $parametroQuery->getResult();

        $periodoEscolar = $extra[0]->getPeriodoEscolar()->getId();

        // datos de grupos de Materias que debería ver basado en el id del grupo en la Entidad GrupoAsignatura
        $asignaturasQuery = $em->createQuery('SELECT g,a,d,p FROM ColegioGrupoBundle:GrupoAsignatura g 
            JOIN g.idAsignatura a JOIN g.idDocente d JOIN g.idGrupo p WHERE g.idGrupo = :grupo AND p.colegio = :colegio
            ORDER BY a.nombre ASC
        ');
        $asignaturasQuery->setParameter('grupo',$grupo);
        $asignaturasQuery->setParameter('colegio',$colegio);
        $asignaturas = $asignaturasQuery->getResult();



        // datos de grupos de Materias que debería ver basado en el id del grupo en la Entidad GrupoAsignatura
        $boletinesQuery = $em->createQuery('SELECT b,g,a,p,t FROM ColegioBoletinBundle:GrupoBoletin b
                    JOIN b.grupoAsignatura g JOIN g.idAsignatura a JOIN b.periodo p JOIN p.tipoCalificacion t
                    WHERE g.idGrupo = :grupo AND g.colegio = :colegio
            ORDER BY b.periodo DESC
        ');
        $boletinesQuery->setParameter('grupo',$grupo);
        $boletinesQuery->setParameter('colegio',$colegio);
        $boletines = $boletinesQuery->getResult();

        // Periodos académicos (sub)
        $periodo = $em->createQuery('SELECT p,c,e FROM ColegioBoletinBundle:Periodo p 
            JOIN p.periodoEscolar e JOIN p.tipoCalificacion c
            WHERE p.periodoEscolar = :periodoEscolar AND p.colegio = :colegio ORDER BY p.fechaInicio ASC');
        $periodo->setParameter('periodoEscolar',$periodoEscolar);
        $periodo->setParameter('colegio',$colegio);
        $datosPeriodos = $periodo->getResult();

        // Encontramos las notas Totales ///
        $notasQuery = $em->createQuery('SELECT n,g,a,p FROM ColegioEstudianteBundle:Nota n 
                JOIN n.grupoBoletin g JOIN g.grupoAsignatura a JOIN g.periodo p
                WHERE n.matricula = :matricula  
            ');
        $notasQuery->setParameter('matricula',$matricula);
        $notas = $notasQuery->getResult();

        // Encontramos las Notas por Logros si las hay agrupandolas por boletin
        $notalogrosQuery = $em->createQuery('SELECT n,m,l,b,p,a  
                    FROM ColegioEstudianteBundle:NotaLogro n 
                    JOIN n.matricula m JOIN n.logro l JOIN l.idBoletin b JOIN b.periodo p JOIN b.asignatura a
                    WHERE n.matricula = :estudiante GROUP BY l.idBoletin
            ');
        $notalogrosQuery->setParameter('estudiante',$estudiante);
        $notalogros = $notalogrosQuery->getResult();
        // Encontramos las notas sin agrupar
        $notalogrostotalQuery = $em->createQuery('SELECT n,m,l,b,p,a,c  
                    FROM ColegioEstudianteBundle:NotaLogro n 
                    JOIN n.matricula m JOIN n.logro l JOIN l.idBoletin b JOIN b.periodo p JOIN b.asignatura a JOIN l.categoria c
                    WHERE n.matricula = :estudiante
            ');
        $notalogrostotalQuery->setParameter('estudiante',$estudiante);
        $notalogrostotal = $notalogrostotalQuery->getResult();
        ///// calculamos los promedios acumulados
        $promediologrosQuery = $em->createQuery('SELECT n,l,b,a,AVG(n.calificacion) as notatotal FROM ColegioEstudianteBundle:NotaLogro n 
                 JOIN n.logro l JOIN l.idBoletin b JOIN b.asignatura a
                WHERE n.matricula = :matricula GROUP BY a.id
            ');
        $promediologrosQuery->setParameter('matricula',$matricula);
        $promedioslogros = $promediologrosQuery->getResult();






        /// Promedio basado en Notas FINAL
        $promedioQuery = $em->createQuery('SELECT n,g,a,AVG(n.calificacion) as notatotal FROM ColegioEstudianteBundle:Nota n 
                 JOIN n.grupoBoletin g JOIN g.grupoAsignatura a
                WHERE n.matricula = :matricula GROUP BY g.grupoAsignatura
            ');
        $promedioQuery->setParameter('matricula',$matricula);
        $promedios = $promedioQuery->getResult();

         if($request->getMethod() == 'POST'){
            $calificacion = $this->get('request')->request->get('calificacion');
            $fallas = $this->get('request')->request->get('fallas');
            $id = $this->get('request')->request->get('id');

            $notica = $this->get('request')->request->get('noticaid');
            $confirmacion = $this->get('request')->request->get('eliminar');

            $calificacionlogro = $this->get('request')->request->get('calificacionlogro');
            $idNotalogro = $this->get('request')->request->get('idlogro');

                  if($confirmacion == null){

                  }else{
                    $notaBorrar = $em->getRepository('ColegioEstudianteBundle:Nota')->find($notica);
                    $em->remove($notaBorrar);
                    $em->flush();
                     $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Eliminaste una nota!');
                     return $this->redirect($this->generateUrl('estudiante_admin', array('matricula' => $matricula,'parametros' => $parametros,'grupo' => $grupo)));
                  }  

                  if($calificacion == null ){

                  }else{  
                        $actualizar = $em->getRepository('ColegioEstudianteBundle:Nota')->find($id);
                        $actualizar->setCalificacion($calificacion);
                        $actualizar->setFallas($fallas);
                        $em->flush();              
                        $this->get('session')->getFlashBag()->add(
                        'notice',
                        'Bien hecho has actualizado una calificacion!'); 
                        return $this->redirect($this->generateUrl('estudiante_admin', array('matricula' => $matricula,'parametros' => $parametros,'grupo' => $grupo)));              
                }

                if($calificacionlogro == null){

                }else{
                        $actualizar = $em->getRepository('ColegioEstudianteBundle:NotaLogro')->find($idNotalogro);
                        $actualizar->setCalificacion($calificacionlogro);
                        $em->flush();              
                        $this->get('session')->getFlashBag()->add(
                        'notice',
                        'Bien hecho has actualizado una calificacion!'); 
                        return $this->redirect($this->generateUrl('estudiante_admin', array('matricula' => $matricula,'parametros' => $parametros,'grupo' => $grupo)));              
               
                }



            }
            
            $calificacioncilla = $this->get('request')->request->get('calificacioncilla');
            $fallascilla = $this->get('request')->request->get('fallascilla');
            $boletincillo = $this->get('request')->request->get('boletincillo');

        if($request->getMethod()=='POST'){
            if($boletincillo == null){

            }else{    
            $boletincilloQuery = $em->getRepository('ColegioBoletinBundle:GrupoBoletin')->find($boletincillo);

            $hoy = new \DateTime('now');
            $insersion = new Nota();
            $insersion->setCalificacion($calificacioncilla); 
            $insersion->setFallas($fallascilla); 
            $insersion->setDescripcion('...');
            $insersion->setFecha($hoy);
            $insersion->setGrupoBoletin($boletincilloQuery);
            $insersion->setMatricula($estudiante);

              $em = $this->getDoctrine()->getManager();
              $em->persist($insersion);
              $em->flush();
              $this->get('session')->getFlashBag()->add(
                 'notice',
                 'Nota Correctamente insertada!');
              return $this->redirect($this->generateUrl('estudiante_admin', array('matricula' => $matricula,'parametros' => $parametros,'grupo' => $grupo)));
              }
         } 

            $noticalogro = $this->get('request')->request->get('noticalogroid');
            $confirmacionnoticalogro = $this->get('request')->request->get('eliminarnotalogro');

                  if($confirmacionnoticalogro == null){

                  }else{
                    $notaBorrarlogro = $em->getRepository('ColegioEstudianteBundle:NotaLogro')->find($noticalogro);
                    $em->remove($notaBorrarlogro);
                    $em->flush();
                     $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Eliminaste una nota!');
                     return $this->redirect($this->generateUrl('estudiante_admin', array('matricula' => $matricula,'parametros' => $parametros,'grupo' => $grupo)));
                  }     

        
       
        return $this->render('ColegioEstudianteBundle:Nota:estudiante.html.twig',array(
                    'entities'        => $estudiante,
                    'grupos'          => $datosGrupo,
                    'parametro'       => $datoParametro,
                    'asignaturas'     => $asignaturas,
                    'datosPeriodos'   => $datosPeriodos,
                    'notas'           => $notas,
                    'promedios'       => $promedios,
                    'boletines'       => $boletines,
                    'notalogros'      => $notalogros,
                    'notalogrostotal' => $notalogrostotal,
                    'promedioslogros' => $promedioslogros,
                    
            ));
    }

    /**
     * Creates a new Nota entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Nota();
        $hoy = new \DateTime('now');
        $entity->setFecha($hoy);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
			$this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');
            return $this->redirect($this->generateUrl('nota_show', array('id' => $entity->getId())));
        }

        return $this->render('ColegioEstudianteBundle:Nota:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Nota entity.
    *
    * @param Nota $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Nota $entity)
    {
		$usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $form = $this->createForm(new NotaType($idColegio), $entity, array(
            'action' => $this->generateUrl('nota_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Nota entity.
     *
     */
    public function newAction()
    {
        $entity = new Nota();
        $entity->setDescripcion("...");
        $hoy = new \DateTime('now');
        $entity->setFecha($hoy);
        $form   = $this->createCreateForm($entity);

        return $this->render('ColegioEstudianteBundle:Nota:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Nota entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioEstudianteBundle:Nota')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Nota entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioEstudianteBundle:Nota:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Nota entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioEstudianteBundle:Nota')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Nota entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioEstudianteBundle:Nota:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Nota entity.
    *
    * @param Nota $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Nota $entity)
    {
		$usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $form = $this->createForm(new NotaType($idColegio), $entity, array(
            'action' => $this->generateUrl('nota_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Nota entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioEstudianteBundle:Nota')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Nota entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
			$this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');
            return $this->redirect($this->generateUrl('nota_edit', array('id' => $id)));
        }

        return $this->render('ColegioEstudianteBundle:Nota:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Nota entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioEstudianteBundle:Nota')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Nota entity.');
            }

            $em->remove($entity);
            $em->flush();
             $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');
        }

        return $this->redirect($this->generateUrl('nota'));
    }

    /**
     * Creates a form to delete a Nota entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('nota_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
