<?php

namespace Colegio\EstudianteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

use Symfony\Component\HttpFoundation\Request;
use Colegio\EstudianteBundle\Entity\Estudiante;
use Colegio\EstudianteBundle\Form\PerfilType;
use Colegio\AdminBundle\Entity\MatriculasCirculares;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em               =  $this->getDoctrine()->getManager();
        $usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
        $idEstudiante     =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();
        
        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
         if($periodo==null){
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Solicita por favor que habiliten un periodo escolar... con tu administrador');
            return $this->redirect($this->generateUrl('ColegioEstudianteBundle_soporte'));
        }else{

        }
        $periodoEscolar = $periodo->getPeriodoEscolar()->getId();
        $idperiodo = $periodo->getId();
        
        $matricula = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findOneBy(array(
            'periodoEscolar'=>$periodoEscolar,
            'idEstudiante'=>$idEstudiante
        ));

        $grupo = $matricula->getIdGrupo();

        $entities = $em->getRepository('ColegioAdminBundle:Circulares')->findCircularesInicial($grupo);
        return $this->render('ColegioEstudianteBundle:Default:index.html.twig',array(
            'entities'=>$entities
        ));
    }
    public function entradaAction()
    {
        $peticion = $this->getRequest();
        $sesion = $peticion->getSession();
        $error = $peticion->attributes->get(
        SecurityContext::AUTHENTICATION_ERROR,
        $sesion->get(SecurityContext::AUTHENTICATION_ERROR));
        return $this->render('UsuariosUsuariosBundle:Default:login2.html.twig', array(
        'last_username' => $sesion->get(SecurityContext::LAST_USERNAME),
        'error'   => $error
        ));
    }
    
    public function notasAction($periodoEscolar)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idUsuario = $usuarioActivo->getId();
        $colegio = $usuarioActivo->getIdColegio();

        $pension = $usuarioActivo->getPension();

        if($periodoEscolar==null){
            $hoy = new \DateTime('now');
            $parametros = $em->getRepository('ColegioAdminBundle:Parametros')->findParametro($hoy,$colegio);
        }


        if(!$parametros){
            return $this->redirect($this->generateUrl('colegio_estudiante_homepage'));
        }

        //// periodo escolar /////
        $periodoEscolarConsultado = $parametros->getPeriodoEscolar()->getId();
        //Periodos escolares 
        $periodos = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodosColegio($colegio,$parametros);

        //Grupos que debería ver el estudiante
        $grupos = $em->getRepository('ColegioEstudianteBundle:Nota')->findNotasEstudiante($idUsuario);
        
        /* consultamos los grupos asignatura que posee el estudiante
        Basándonos en la matricula actual de la Tabla GrupoEstudiante
        */
        $grupoEstudiante = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findMatriculasEstudiantes($periodoEscolarConsultado,$idUsuario);
        //$grupoEstudiante = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findMatriculasEstudiante($colegio,$idUsuario);

        //calificaciones Total
        $entities = $em->getRepository('ColegioEstudianteBundle:Nota')->findNotasTotal($idUsuario);

        // Calificaciones por Logro
        $calificacionesLogros = $em->getRepository('ColegioEstudianteBundle:NotaLogro')->findNotaLogroEstudiante($idUsuario);

        $calificacionesLogrosBoletin = $em->getRepository('ColegioEstudianteBundle:NotaLogro')->findNotaLogrosBoletin($idUsuario);

        //Promedios
        $promedios = $em->getRepository('ColegioEstudianteBundle:Nota')->findPromedioAppEstudiante($idUsuario);
        $promedioslogros = $em->getRepository('ColegioEstudianteBundle:NotaLogro')->findPromedioLogrosEstudiante($idUsuario);


        ////////////// Observaciones ///////////////////
        $observacionesQuery = $em->createQuery('SELECT o,pt FROM ColegioDocenteBundle:Observacion o 
            JOIN o.periodo pt 
            WHERE o.estudiante = :estudiante AND pt.periodoEscolar = :periodoEscolar
        ');
        
        $observacionesQuery->setParameter('estudiante',$grupoEstudiante);
        $observacionesQuery->setParameter('periodoEscolar',$periodoEscolarConsultado);
        $observaciones = $observacionesQuery->getResult();

        //////////// escala del colegio //////////////////
        $normaId = $parametros->getNorma();
        
        if(!$normaId)
        {
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Hay un error, pide al administrador que habilite la norma para el año lectivo!');
            return $this->redirect($this->generateUrl('colegio_estudiante_homepage'));
        }
        else
        {
            $norma = $em->getRepository('ColegioAdminBundle:Norma')->find($normaId);
            $items = $em->getRepository('ColegioAdminBundle:Item')->findItemValor($norma,$parametros);
        }

        return $this->render('ColegioEstudianteBundle:Default:notas.html.twig', array(
            'pension'               => $pension,
            'entities'              => $entities,
            'grupos'                => $grupos,
            'periodosConsulta'      => $periodos,
            'promedios'             => $promedios,
            'calificacionesLogros'  => $calificacionesLogros,
            'grupoEstudiante'       => $grupoEstudiante,
            'calificacionesLogrosBoletin'  => $calificacionesLogrosBoletin,
            'promedioslogros'       => $promedioslogros,
            'observaciones'         => $observaciones,
            'items'                 => $items
        ));
	}          
	
    public function tareasAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idEstudiante = $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();
        $hoy = new \DateTime('now');

        $parametros = $em->getRepository('ColegioAdminBundle:Parametros')->findParametro($hoy,$idColegio);

        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
         if($periodo==null){
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Solicita por favor que habiliten un periodo escolar... con tu administrador');
            return $this->redirect($this->generateUrl('colegio_estudiante_homepage'));
        }
        $idPeriodo = $periodo->getId();
        $periodoEscolar = $periodo->getPeriodoEscolar()->getId();
        $idperiodo = $periodo->getId();
        
        $matricula = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findOneBy(array(
            'periodoEscolar'=>$periodoEscolar,
            'idEstudiante'=>$idEstudiante
        ));
        $idMatricula = $matricula->getId();


        $trabajos = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findMisTrabajos($matricula);

		return $this->render('ColegioEstudianteBundle:Default:tareastotal.html.twig',array(
                'trabajos' => $trabajos,
                'matricula'=> $idMatricula,
                'periodo'  => $idperiodo
            ));
	}

    public function tareasNuevasAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idEstudiante = $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();
        $hoy = new \DateTime('now');

        $parametros = $em->getRepository('ColegioAdminBundle:Parametros')->findParametro($hoy,$idColegio);

        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
         if($periodo==null){
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Solicita por favor que habiliten un periodo escolar... con tu administrador');
            return $this->redirect($this->generateUrl('colegio_estudiante_homepage'));
        }
        $idPeriodo = $periodo->getId();
        $periodoEscolar = $periodo->getPeriodoEscolar()->getId();
        $idperiodo = $periodo->getId();
        
        $matricula = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findOneBy(array(
            'periodoEscolar'=>$periodoEscolar,
            'idEstudiante'=>$idEstudiante
        ));
        $idMatricula = $matricula->getId();


        $trabajos = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findMisTrabajosSinCalificar($matricula,$periodo);

        return $this->render('ColegioEstudianteBundle:Default:tareastotalnuevas.html.twig',array(
                'trabajos' => $trabajos,
                'matricula'=> $idMatricula,
                'periodo'  => $idperiodo
            ));
    }
    
	
	public function circularesAction($circular)
    {
        $em               =  $this->getDoctrine()->getManager();
        $usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
        $idEstudiante     =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();
        
        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
         if($periodo==null){
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Solicita por favor que habiliten un periodo escolar... con tu administrador');
            return $this->redirect($this->generateUrl('colegio_estudiante_homepage'));
        }
        $periodoEscolar = $periodo->getPeriodoEscolar()->getId();
        $idperiodo = $periodo->getId();
        
        $matricula = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findOneBy(array(
            'periodoEscolar'=>$periodoEscolar,
            'idEstudiante'=>$idEstudiante
        ));

        $grupo = $matricula->getIdGrupo();

        $entities = $em->getRepository('ColegioAdminBundle:Circulares')->findCircularesGrupo($grupo);

        $matriculaCircular = $em->getRepository('ColegioAdminBundle:MatriculasCirculares')->findOneBy(array(
            'matricula' => $matricula->getId(),
            'circular'  => $circular
        ));

        $this->validarCircular($circular, $matricula, $matricula->getId());

		return $this->render('ColegioEstudianteBundle:Default:circulares.html.twig',array(
            'entities'=>$entities,
            'circular'=>$circular
        ));
	}
	
    public function validarCircular($circular, $obMatricula,$matriId)
    {
        $em = $this->getDoctrine()->getManager();

        $matriculaCircularQuery = $em->createQuery('
            SELECT mc FROM  ColegioAdminBundle:MatriculasCirculares mc
            WHERE mc.matricula = :matricula AND mc.circular = :circular
        ');
        $matriculaCircularQuery->setParameter('matricula',$matriId);
        $matriculaCircularQuery->setParameter('circular',$circular);
        $matriculaCircularQuery->setMaxResults(1);
        $matriculaCircular = $matriculaCircularQuery->getOneOrNullResult();
       
         if($circular != null || $circular != ""){
            $circularConsulta = $em->getRepository('ColegioAdminBundle:Circulares')->find($circular);
           if($matriculaCircular == null){
                $matriculaCircularNueva = new MatriculasCirculares();
                $matriculaCircularNueva->setMatricula($obMatricula);
                $matriculaCircularNueva->setCircular($circularConsulta);
                $matriculaCircularNueva->setFecha(new \DateTime());
                $matriculaCircularNueva->setFechaVisita(new \DateTime());
                $matriculaCircularNueva->setEstado(false);
                $em->persist($matriculaCircularNueva);
                $em->flush();
            }else{
                $matriculaCircular->setFechaVisita(new \DateTime());
                $matriculaCircular->setEstado(true);
                $em->flush();
            }
        }
    }

	public function matriculasAction()
    {
		$em = $this->getDoctrine()->getManager();
		$usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idUsuario = $usuarioActivo->getId();

        $entities = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findBy(array(
               'idEstudiante'=>$idUsuario,
               'isActive'=>true 
        ),array(
                'id'=>'DESC'
        ));

        $inscripcionesAsignaturasQuery = $em->createQuery('
            SELECT i,m,e,ga,a FROM ColegioBoletinBundle:InscripcionAsignatura i
            INNER JOIN i.matricula m
            INNER JOIN m.idEstudiante e
            INNER JOIN i.grupoAsignatura ga
            INNER JOIN ga.idAsignatura a
            WHERE e.id = :estudiante
            ORDER BY a.nombre ASC
        ');
        $inscripcionesAsignaturasQuery->setParameter('estudiante',$idUsuario);
        $inscripcionesAsignatura = $inscripcionesAsignaturasQuery->getResult();

        return $this->render('ColegioEstudianteBundle:Default:matriculas.html.twig',array(
			'entities'=> $entities,
            'inscripciones' => $inscripcionesAsignatura
		));
	}

    public function perfilAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idUsuario = $usuarioActivo->getId();        

        $datos = $em->getRepository('ColegioEstudianteBundle:Estudiante')->findOneById($idUsuario);

        $entity = $em->getRepository('ColegioEstudianteBundle:Estudiante')->find($idUsuario);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Estudiante entity.');
        }

        
        
        $form = $this->createForm(new PerfilType(), $datos, array(
            //'action' => $this->generateUrl('estudiante_perfil_update', array('id' => $datos->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));
        
        $passwordOriginal = $form->getData()->getPassword();
        $form->handleRequest($request);
        if ($form->isValid()) {
                
            if (null == $datos->getPassword()) {
                $datos->setPassword($passwordOriginal);
                }
                else{
                    $encoder = $this->get('security.encoder_factory')
                                    ->getEncoder($datos);
                    $passwordCodificado = $encoder->encodePassword(
                        $datos->getPassword(),
                        $datos->getSalt()
                    );
                    $datos->setPassword($passwordCodificado);
                    }
            
            
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho! has actualizado correctamente tu Información');
            return $this->redirect($this->generateUrl('colegio_estudiante_perfil'));
        }

        return  $this->render('ColegioEstudianteBundle:Default:perfil.html.twig',array(
                    'datos'  => $datos,
                    'form'   => $form->createView(),
            ));
    }

    public function juegosAction()
    {
        return $this->render('ColegioEstudianteBundle:Default:juegos.html.twig');
    }
    
    public function asignaturasAction()
    {
        $em               =  $this->getDoctrine()->getManager();
       	$usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
    	$idEstudiante     =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();
        
        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
         if($periodo==null){
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Solicita por favor que habiliten un periodo escolar... con tu administrador');
            return $this->redirect($this->generateUrl('colegio_estudiante_homepage'));
        }
        $periodoEscolar = $periodo->getPeriodoEscolar()->getId();
        $idperiodo = $periodo->getId();
        
        $matricula = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findOneBy(array(
            'periodoEscolar'=>$periodoEscolar,
            'idEstudiante'=>$idEstudiante
        ));
        
        $asignaturas = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->findBy(array(
            'idGrupo'=>$matricula->getIdGrupo(),
            'isActive'=>true
        ));
        
         return $this->render('ColegioEstudianteBundle:Default:asignaturas.html.twig', array(
             'asignaturas'=>$asignaturas,
             'matricula'=>$matricula,
             'periodo'=>$periodo,
             'periodoEscolar'=>$periodoEscolar
         ));
    }
    
    public function trabajosAction($grupoAsignatura,$periodo,$trabajo)
    {
        $em               =  $this->getDoctrine()->getManager();
       	$usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
    	$idEstudiante     =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();
        
        if($trabajo==null)
        {
          $trabajo = "";
        }

        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
         if($periodo==null){
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Solicita por favor que habiliten un periodo escolar... con tu administrador');
            return $this->redirect($this->generateUrl('colegio_estudiante_homepage'));
        }
        $periodoEscolar = $periodo->getPeriodoEscolar()->getId();
        $idperiodo = $periodo->getId();
        
        $matricula = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findOneBy(array(
            'periodoEscolar'=>$periodoEscolar,
            'idEstudiante'=>$idEstudiante
        ));
        $idMatricula = $matricula->getId();


        $grupo = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->find($grupoAsignatura);

        $trabajos = $em->getRepository('ColegioDocenteBundle:Trabajos')->findTrabajosEstudiante($grupoAsignatura,$periodo);
        
        return $this->render('ColegioEstudianteBundle:Default:trabajos.html.twig',array(
            'trabajos'=>$trabajos,
            'idTrabajo'=>$trabajo,
            'grupo'=>$grupo,
            'matricula'=>$idMatricula
        ));        
    }
    
    public function soporteAction()
    {
        return $this->render('ColegioEstudianteBundle:Default:soporte.html.twig');
    }

}
