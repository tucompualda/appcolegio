<?php

namespace Colegio\EstudianteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\EstudianteBundle\Entity\Estudiante;
use Colegio\EstudianteBundle\Form\EstudianteType;

/**
 * Estudiante controller.
 *
 */
class EstudianteController extends Controller
{

    /**
     * Lists all Estudiante entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $sede = $em->getRepository('ColegioAdminBundle:Sede')->findColegio($idColegio);
        
        $entities = $em->getRepository('ColegioEstudianteBundle:Estudiante')->findBy(
            array(
            'idSede'=>$sede
            ),
            array('isActive'=>'DESC','primerApellido'=>'ASC')
        );


        $entitiesQuery = $em->createQuery('
            SELECT 
                e,ge
            FROM
                ColegioEstudianteBundle:Estudiante e
                LEFT JOIN e.gruposEstudiantes ge
            WHERE
                e.idSede = :sede AND ge.isActive = true
            ORDER BY
                e.isActive DESC, e.primerApellido ASC, ge.id DESC    
        ');
        $entitiesQuery->setParameter('sede',$sede);
        $entites = $entitiesQuery->getResult();

        return $this->render('ColegioEstudianteBundle:Estudiante:index.html.twig', array(
            'entities' => $entities,
            'notas' => null
        ));
    }
    /**
     * Creates a new Estudiante entity.
     *
     */
    public function createAction(Request $request)
    {
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $entity = new Estudiante();
		$entity->setIdColegio($idColegio);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
       

        if ($form->isValid()) {
            $encoder = $this->get('security.encoder_factory')
                            ->getEncoder($entity);
            $entity->setSalt(md5(time()));
            $passwordCodificado = $encoder->encodePassword(
                       $entity->getPassword(),
                       $entity->getSalt()
            );
            $entity->setPassword($passwordCodificado);
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');

            return $this->redirect($this->generateUrl('estudiante_show', array('id' => $entity->getId())));
        }

        return $this->render('ColegioEstudianteBundle:Estudiante:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Estudiante entity.
    *
    * @param Estudiante $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Estudiante $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $detalleColegio = $em->getRepository('ColegioAdminBundle:detalleColegio')->findByidColegio($idColegio);
        $entity->setPension(true);
        $entity->setIsActive(true);

        $form = $this->createForm(new EstudianteType($detalleColegio), $entity, array(
            'action' => $this->generateUrl('estudiante_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Estudiante entity.
     *
     */
    public function newAction()
    {
        $entity = new Estudiante();
        $form   = $this->createCreateForm($entity);

        return $this->render('ColegioEstudianteBundle:Estudiante:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Estudiante entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $entity = $em->getRepository('ColegioEstudianteBundle:Estudiante')->findOneBy(array(
            'id'=>$id,
            'idColegio'=> $idColegio
            ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Estudiante entity.');
        }

        $matriculas = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findBy(array(
               'idEstudiante'=>$id,
               'isActive'=>true 
            ),array(
                'id'=>'DESC'
            ));

        $notas = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findMisNotasFinales($id,"Normal");
        $notasJson = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findMisNotasFinales($id,"JSON");
        $deleteForm = $this->createDeleteForm($id);

        $inscripcionesAsignaturasQuery = $em->createQuery('
            SELECT i,m,e,ga,a FROM ColegioBoletinBundle:InscripcionAsignatura i
            INNER JOIN i.matricula m
            INNER JOIN m.idEstudiante e
            INNER JOIN i.grupoAsignatura ga
            INNER JOIN ga.idAsignatura a
            WHERE e.id = :estudiante AND ga.isActive = true
            ORDER BY a.nombre ASC
        ');
        $inscripcionesAsignaturasQuery->setParameter('estudiante',$id);
        $inscripcionesAsignatura = $inscripcionesAsignaturasQuery->getResult();

        return $this->render('ColegioEstudianteBundle:Estudiante:show.html.twig', array(
            'entity'      => $entity,
            'matriculas'  => $matriculas,
            'notas'       => $notas,
            'notasJson'   => $notasJson,
            'inscripciones' => $inscripcionesAsignatura,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Finds and displays a Nota by Estudiante entity.
     *
     */
    public function showNotasAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ColegioEstudianteBundle:Estudiante')->findAll();
        $notas = $em->getRepository('ColegioEstudianteBundle:Nota')->findBy(array(
            'idEstudiante' => $id));
        
        return $this->render('ColegioEstudianteBundle:Estudiante:index.html.twig', array(
            'entities' => $entities,
            'notas' => $notas));
    }
    
    /**
     * Displays a form to edit an existing Estudiante entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioEstudianteBundle:Estudiante')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Estudiante entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioEstudianteBundle:Estudiante:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Estudiante entity.
    *
    * @param Estudiante $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Estudiante $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $detalleColegio = $em->getRepository('ColegioAdminBundle:detalleColegio')->findByIdColegio($idColegio);
        $form = $this->createForm(new EstudianteType($detalleColegio), $entity, array(
            'action' => $this->generateUrl('estudiante_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Estudiante entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioEstudianteBundle:Estudiante')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Estudiante entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $passwordOriginal = $editForm->getData()->getPassword();
        $editForm->handleRequest($request);
        

        if ($editForm->isValid()) {
                
            if (null == $entity->getPassword()) {
                $entity->setPassword($passwordOriginal);
                }
                else{
                    $encoder = $this->get('security.encoder_factory')
                                    ->getEncoder($entity);
                    $passwordCodificado = $encoder->encodePassword(
                        $entity->getPassword(),
                        $entity->getSalt()
                    );
                    $entity->setPassword($passwordCodificado);
                    }
            
            
            $em->flush();
			$this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');
            return $this->redirect($this->generateUrl('estudiante_edit', array('id' => $id)));
        }

        return $this->render('ColegioEstudianteBundle:Estudiante:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Estudiante entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioEstudianteBundle:Estudiante')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Estudiante entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');
        }

        return $this->redirect($this->generateUrl('estudiante'));
    }

    /**
     * Creates a form to delete a Estudiante entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('estudiante_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
