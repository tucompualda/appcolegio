<?php

namespace Colegio\EstudianteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Colegio\EstudianteBundle\Entity\Estudiante;
use Colegio\EstudianteBundle\Form\PerfilType;
use Colegio\AdminBundle\Entity\Mensajes;



class ChatController extends Controller
{
    
    public function chatAction()
    {
        $em               =  $this->getDoctrine()->getManager();
        $usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
        $idEstudiante     =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();



         $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
         if($periodo==null){
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Solicita por favor que habiliten un periodo escolar... con tu administrador');
            return $this->redirect($this->generateUrl('colegio_estudiante_homepage'));
        }
        $periodoEscolar = $periodo->getPeriodoEscolar()->getId();
        $idperiodo = $periodo->getId();
        
        $matricula = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findOneBy(array(
            'periodoEscolar'=>$periodoEscolar,
            'idEstudiante'=>$idEstudiante
        ));

        $grupo = $matricula->getIdGrupo();


        $estudiantes = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findMisCompaneros($grupo);

        $docentes = $em->getRepository('ColegioDocenteBundle:Docente')->misDocentes($grupo);

        $administradores = $em->getRepository('UsuariosUsuariosBundle:Usuarios')->findByIdColegio($idColegio);


        return $this->render('ColegioEstudianteBundle:Default:chat.html.twig',array(
            'grupo'=>$grupo,
            'estudiantes'=>$estudiantes,
            'docentes'=>$docentes,
            'administradores'=>$administradores
        ));
    }



    public function mensajesDocentesAction($docente)
    {
        $em               =  $this->getDoctrine()->getManager();
        $usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
        $idEstudiante     =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();

        $mensajes = $em->getRepository('ColegioAdminBundle:Mensajes')->findMensajesDocentes($idEstudiante,$docente);
        
        foreach ($mensajes as $key) {
            $id = $key['id'];
            $mensajeActualizado = $em->getRepository('ColegioAdminBundle:Mensajes')->find($id);
            $mensajeActualizado->setVistoEstudiante(true);
            $em->flush();
        }

        if($mensajes== null){
            $mensajes = "No hay mensajes";
        }

       $jsonp = new JsonResponse($mensajes);
       return $jsonp;
    }

   public function consultaNuevosDocenteAction($docente)
   {
       $em               =  $this->getDoctrine()->getManager();
        $usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
        $idEstudiante     =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();

        $mensajes = $em->getRepository('ColegioAdminBundle:Mensajes')->findMensajesNuevosDocentes($idEstudiante,$docente);
        
        foreach ($mensajes as $key) {
            $id = $key['id'];
            $mensajeActualizado = $em->getRepository('ColegioAdminBundle:Mensajes')->find($id);
            $mensajeActualizado->setVistoDocente(true);
            $em->flush();
        }

        if($mensajes== null){
            $mensajes = "No hay mensajes";
        }



       $jsonp = new JsonResponse($mensajes);
       return $jsonp;
   }

   public function consultaTotalNuevosDocenteAction($docente)
   {
       $em               =  $this->getDoctrine()->getManager();
        $usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
        $idEstudiante     =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();

        $mensajes = $em->getRepository('ColegioAdminBundle:Mensajes')->findTotalMensajesNuevosDocentes($idEstudiante,$docente);
        
        foreach ($mensajes as $key) {
            $total = $key[0];
        }

        if($mensajes == null){
           
            $total = "0";
        }



       $respuesta = new Response($total);
       return $respuesta;
   }


 /**
 * llama los mensaje de un estudiante en especifico, deberia quitar el contador
 * y actualiza el estado de visto al docente
 */
  public function mensajesEstudiantesAction($estudiante)
    {
        $em               =  $this->getDoctrine()->getManager();
        $usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
        $docente     =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();

        $mensajes = $em->getRepository('ColegioAdminBundle:Mensajes')->findMensajesDocentes($estudiante,$docente);

        if($mensajes== null){
            $mensajes = "No hay mensajes";
        }else{
            foreach ($mensajes as $key) {
            $id = $key['id'];
            $mensajeActualizado = $em->getRepository('ColegioAdminBundle:Mensajes')->find($id);
            $mensajeActualizado->setVistoDocente(true);
            $em->flush();
        }
        }

       $jsonp = new JsonResponse($mensajes);
       return $jsonp;
    }

  public function consultaNuevosEstudianteAction($estudiante)
   {
       $em               =  $this->getDoctrine()->getManager();
        $usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
        $docente     =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();

        $mensajes = $em->getRepository('ColegioAdminBundle:Mensajes')->findMensajesNuevosEstudiantes($estudiante,$docente);
        
        foreach ($mensajes as $key) {
            $id = $key['id'];
            $mensajeActualizado = $em->getRepository('ColegioAdminBundle:Mensajes')->find($id);
            $mensajeActualizado->setVistoEstudiante(true);
            $em->flush();
        }

        if($mensajes== null){
            $mensajes = "No hay mensajes";
        }

        

       $jsonp = new JsonResponse($mensajes);
       return $jsonp;
   }

 public function consultaTotalNuevosEstudianteAction($estudiante)
   {
       $em               =  $this->getDoctrine()->getManager();
        $usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
        $docente     =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();

        $mensajes = $em->getRepository('ColegioAdminBundle:Mensajes')->findTotalMensajesNuevosEstudiantes($estudiante,$docente);
        
        foreach ($mensajes as $key) {
            $total = $key[0];
        }

        if($mensajes== null){
            $mensajes = "0";
            $total = "0";
        }

        

       $jsonp = new Response($total);
       return $jsonp;
   }




    public function mensajesparaDocenteAction($docente,$mensaje,$asunto)
    {

        $em               =  $this->getDoctrine()->getManager();
        $usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
        $idEstudiante     =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();

        $hoy = new \DateTime("now");

        $docenteReal = $em->getRepository('ColegioDocenteBundle:Docente')->find($docente);


        $enviado = new Mensajes();
        $enviado->setDocenteDestinatario($docenteReal);
        $enviado->setEstudiante($usuarioActivo);
        $enviado->setFechaCreacion($hoy);
        $enviado->setFechaModificacion($hoy);
        $enviado->setAsunto($asunto);
        $enviado->setMensaje($mensaje);
        $enviado->setEstado(true);
        $enviado->setBorrador(false);
        $enviado->setVistoDocente(false);

        $em->persist($enviado);
        $em->flush();



        $mensajes = "OK";
        $jsonp = new JsonResponse($mensajes);
       return $jsonp;
    }

      public function mensajesparaEstudianteAction($estudiante,$mensaje,$asunto)
    {

        $em               =  $this->getDoctrine()->getManager();
        $usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
        $idDocente     =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();

        $hoy = new \DateTime("now");

        $estudianteReal = $em->getRepository('ColegioEstudianteBundle:Estudiante')->find($estudiante);


        $enviado = new Mensajes();
        $enviado->setDocente($usuarioActivo);
        $enviado->setEstudianteDestinatario($estudianteReal);
        $enviado->setFechaCreacion($hoy);
        $enviado->setFechaModificacion($hoy);
        $enviado->setAsunto($asunto);
        $enviado->setMensaje($mensaje);
        $enviado->setEstado(true);
        $enviado->setBorrador(false);

        $em->persist($enviado);
        $em->flush();



        $mensajes = "OK";
        $jsonp = new JsonResponse($mensajes);
       return $jsonp;
    }
    
}
