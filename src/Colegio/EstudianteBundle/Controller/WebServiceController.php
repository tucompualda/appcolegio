<?php

namespace Colegio\EstudianteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Colegio\EstudianteBundle\Entity\TrabajoMatricula;
/**
 * Description of WebServiceController
 *
 * @author flia
 */
class WebServiceController extends Controller
{
	public function trabajosAction()
	{
		$em = $this->getDoctrine()->getManager();
		$usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
    	$idEstudiante     =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();
        
        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
         if($periodo==null){
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Solicita por favor que habiliten un periodo escolar... con tu administrador');
           // return $this->redirect($this->generateUrl('colegio_estudiante_homepage'));
        }
        $periodoEscolar = $periodo->getPeriodoEscolar()->getId();
        $idperiodo = $periodo->getId();
        
        $matricula = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findOneBy(array(
            'periodoEscolar'=>$periodoEscolar,
            'idEstudiante'=>$idEstudiante
        ));
        
        $asignaturas = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->findByIdGrupo($matricula->getIdGrupo());
        

        $trabajos = $em->getRepository('ColegioEstudianteBundle:TrabajoMatricula')->findTrabajosService($matricula->getId(),$periodo);

       
			for($i=0;$i<count($trabajos);$i++) {
				
				$repor = $em->getRepository('ColegioDocenteBundle:Trabajos')->find($trabajos[$i]['id']);
			        	
					if($em->getRepository('ColegioEstudianteBundle:TrabajoMatricula')->findBy(array(
							'matricula'=>$matricula,
                            'trabajo'=>$repor 
						))==null){	

						$reporte = new TrabajoMatricula();
						$hoy = new \DateTime('now');
			            $reporte->setMatricula($matricula);
			        	$reporte->setTrabajo($repor);
			        	$reporte->setEstado(false);
			        	$reporte->setFecha($hoy);
			        	$reporte->setFechaVisita($hoy);
			        	$em->persist($reporte);
			        	$em->flush();
			        }

			}

        $trabajosT = $em->createQuery('SELECT m, t FROM ColegioEstudianteBundle:TrabajoMatricula m
            JOIN m.trabajo t 
            WHERE m.matricula = :matricula AND m.estado = false AND t.periodo = :periodo
        ');     
        $trabajosT->setParameter('matricula',$matricula);
        $trabajosT->setParameter('periodo',$periodo);
        $trabajosRevisados = $trabajosT->getResult();
        
        $total = count($trabajosRevisados);


	   $jsonp = new JsonResponse($total);
       //$jsonp->setCallback('myCallback');
       return $jsonp;
	}

	public function trabajosTotalAction()
	{
		$em = $this->getDoctrine()->getManager();
		$usuarioActivo    =  $this->get('security.context')->getToken()->getUser();
    	$idEstudiante     =  $usuarioActivo->getId();
        $idColegio = $usuarioActivo->getIdColegio();

        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
         if($periodo==null){
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Solicita por favor que habiliten un periodo escolar... con tu administrador');
            return $this->redirect($this->generateUrl('colegio_estudiante_homepage'));
        }
        $periodoEscolar = $periodo->getPeriodoEscolar()->getId();
        $idperiodo = $periodo->getId();
        
        $matricula = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findOneBy(array(
            'periodoEscolar'=>$periodoEscolar,
            'idEstudiante'=>$idEstudiante
        ));


 
        $trabajosRevisados = $em->getRepository('ColegioEstudianteBundle:TrabajoMatricula')->findTrabajosTotal($matricula,$periodo);

        $jsonp = new JsonResponse($trabajosRevisados);
       //$jsonp->setCallback('myCallback');
       return $jsonp;
	}

    public function verificarTareaAction($trabajo,$matricula)
    {
         $em = $this->getDoctrine()->getManager();
         
         $trabajoM = $em->getRepository('ColegioDocenteBundle:Trabajos')->find($trabajo);
         $matriculaM = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->find($matricula);

         $trabajoMatricula = $em->getRepository('ColegioEstudianteBundle:TrabajoMatricula')->findOneBy(array(
            'trabajo'=>$trabajoM,
            'matricula'=>$matriculaM
         ));

         if($trabajoMatricula->getEstado()){

            $response = new Response("ya se ha revisado");
         
         }else{
             $hoy = new \DateTime('now');

             $trabajoMatricula->setFechaVisita($hoy);
             $trabajoMatricula->setEstado(true);

             $em->flush();
             $response = new Response("OK Actualizada y revisada la Tarea");
        }
       
       return $response;

    }

    public function usuarioAction($username)
    {
       $em = $this->getDoctrine()->getManager();
       $em = $this->getDoctrine()->getManager();
       $estudianteQuery = $em->createQuery('SELECT e.primerNombre,e.primerApellido,e.salt FROM ColegioEstudianteBundle:Estudiante e
             WHERE e.email = :username');
       $estudianteQuery->setParameter('username',$username);
       $estudiante = $estudianteQuery->getResult();
       $jsonp = new JsonResponse($estudiante);
       //$jsonp->setCallback('myCallback');
       return $jsonp;
    }
}