<?php

namespace Colegio\EstudianteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * NotaLogro
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\EstudianteBundle\Entity\NotaLogroRepository")
 */
class NotaLogro
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\EstudianteBundle\Entity\GrupoEstudiante")
     */
    private $matricula;
    
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\BoletinBundle\Entity\Logro", inversedBy="notalogro")
     */
    private $logro;

    /**
     *@Assert\Range(
     *      min = "0",
     *      max = "5",
     *      minMessage = "Mínimo 0",
     *      maxMessage = "No puedes Poner una nota mayor a 5",
     *      invalidMessage = "Debe ser un número"
     * )
     * @ORM\Column(name="calificacion", type="float")
     */
    private $calificacion;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

   /**
     * @var boolean
     *
     * @ORM\Column(name="fortaleza", type="boolean", nullable=true)
     */
    private $fortaleza;

    /**
     * @var boolean
     *
     * @ORM\Column(name="debilidad", type="boolean", nullable=true)
     */
    private $debilidad;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set matricula
     *
     * @param \Colegio\EstudianteBundle\Entity\GrupoEstudiante $matricula
     * @return NotaLogro
     */
    public function setMatricula(\Colegio\EstudianteBundle\Entity\GrupoEstudiante $matricula)
    {
        $this->matricula = $matricula;
    
        return $this;
    }

    /**
     * Get matricula
     *
     * @return \Colegio\EstudianteBundle\Entity\GrupoEstudiante 
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * Set logro
     *
     * @param \Colegio\BoletinBundle\Entity\Logro $logro
     * @return NotaLogro
     */
    public function setLogro(\Colegio\BoletinBundle\Entity\Logro $logro)
    {
        $this->logro = $logro;
    
        return $this;
    }

    /**
     * Get logro
     *
     * @return \Colegio\BoletinBundle\Entity\Logro
     */
    public function getLogro()
    {
        return $this->logro;
    }

    /**
     * Set calificacion
     *
     * @param string $calificacion
     * @return NotaLogro
     */
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;
    
        return $this;
    }

    /**
     * Get calificacion
     *
     * @return string 
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return NotaLogro
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    public function __toString()
    {
    	return $this->getMatricula() . "-" . $this->getLogro() . " " . $this->getCalificacion();
    }

    /**
     * Set fortaleza
     *
     * @param boolean $fortaleza
     * @return NotaLogro
     */
    public function setFortaleza($fortaleza)
    {
        $this->fortaleza = $fortaleza;
    
        return $this;
    }

    /**
     * Get fortaleza
     *
     * @return boolean 
     */
    public function getFortaleza()
    {
        return $this->fortaleza;
    }

    /**
     * Set debilidad
     *
     * @param boolean $debilidad
     * @return NotaLogro
     */
    public function setDebilidad($debilidad)
    {
        $this->debilidad = $debilidad;
    
        return $this;
    }

    /**
     * Get debilidad
     *
     * @return boolean 
     */
    public function getDebilidad()
    {
        return $this->debilidad;
    }
}