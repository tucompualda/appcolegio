<?php

namespace Colegio\EstudianteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
/**
 * Estudiante
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Estudiante implements AdvancedUserInterface,  \Serializable
{
    
    function eraseCredentials()
    {
    }
    function getRoles()
    {
        return array('ROLE_ESTUDIANTE');
    }
    
    function getUsername()
    {
        return $this->getEmail();
    }
    public function serialize()
    {
       return serialize($this->getId());
    }
 
    public function unserialize($data)
    {
        $this->id = unserialize($data);
    } 
    public function isAccountNonExpired()
    {
            return true;
    }

    public function isAccountNonLocked()
    {
            return true;
    }

    public function isCredentialsNonExpired()
    {
            return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255)
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="primerNombre", type="string", length=255)
     */
    private $primerNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="segundoNombre", type="string", length=255, nullable=true)
     */
    private $segundoNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="primerApellido", type="string", length=255)
     */
    private $primerApellido;

    /**
     * @var string
     *
     * @ORM\Column(name="segundoApellido", type="string", length=255, nullable=true)
     */
    private $segundoApellido;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="nui", type="string", length=255, nullable=true)
     */
    private $nui;

    /**
     * @var string
     *
     * @ORM\Column(name="nuip", type="string", length=255)
     */
    private $nuip;

    /**
     * @var string
     *
     * @ORM\Column(name="numeroDocumento", type="string", length=255)
     */
    private $numeroDocumento;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Sede")
     */
    private $idSede;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Usuarios\UsuariosBundle\Entity\TipoIdentificacion")
     */
    private $idTipoIdentificacion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isActive", type="boolean")
     */
    private $isActive;

        /**
     * @var boolean
     *
     * @ORM\Column(name="pension", type="boolean")
     */
    private $pension;


    /**
     * @ORM\OneToMany(targetEntity="Colegio\EstudianteBundle\Entity\GrupoEstudiante", mappedBy="idEstudiante", cascade={"persist","remove"})
     * @Assert\Valid()
     */
    private $gruposEstudiantes;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Colegio", inversedBy="docentes", cascade={"persist","remove"})
     */
    private $idColegio;
    
  /**
     * @var string
     *
     * @ORM\Column(name="nombreCompletoMadre", type="string", length=255, nullable=true)
     */
    private $nombreCompletoMadre;

  /**
     * @var string
     *
     * @ORM\Column(name="telefonoMadre", type="string", length=255, nullable=true)
     */
    private $telefonoMadre;

  /**
     * @var string
     *
     * @ORM\Column(name="nombreCompletoPadre", type="string", length=255, nullable=true)
     */
    private $nombreCompletoPadre;

  /**
     * @var string
     *
     * @ORM\Column(name="telefonoPadre", type="string", length=255, nullable=true)
     */
    private $telefonoPadre;



    /**
     * @var string
     * @ORM\Column(name="detalle", type="text", nullable=true)
     */
    private $alergias;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Estudiante
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return Estudiante
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set primerNombre
     *
     * @param string $primerNombre
     * @return Estudiante
     */
    public function setPrimerNombre($primerNombre)
    {
        $this->primerNombre = $primerNombre;
    
        return $this;
    }

    /**
     * Get primerNombre
     *
     * @return string 
     */
    public function getPrimerNombre()
    {
        return $this->primerNombre;
    }

    /**
     * Set segundoNombre
     *
     * @param string $segundoNombre
     * @return Estudiante
     */
    public function setSegundoNombre($segundoNombre)
    {
        $this->segundoNombre = $segundoNombre;
    
        return $this;
    }

    /**
     * Get segundoNombre
     *
     * @return string 
     */
    public function getSegundoNombre()
    {
        return $this->segundoNombre;
    }

    /**
     * Set primerApellido
     *
     * @param string $primerApellido
     * @return Estudiante
     */
    public function setPrimerApellido($primerApellido)
    {
        $this->primerApellido = $primerApellido;
    
        return $this;
    }

    /**
     * Get primerApellido
     *
     * @return string 
     */
    public function getPrimerApellido()
    {
        return $this->primerApellido;
    }

    /**
     * Set segundoApellido
     *
     * @param string $segundoApellido
     * @return Estudiante
     */
    public function setSegundoApellido($segundoApellido)
    {
        $this->segundoApellido = $segundoApellido;
    
        return $this;
    }

    /**
     * Get segundoApellido
     *
     * @return string 
     */
    public function getSegundoApellido()
    {
        return $this->segundoApellido;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Estudiante
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    
        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Estudiante
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set nui
     *
     * @param string $nui
     * @return Estudiante
     */
    public function setNui($nui)
    {
        $this->nui = $nui;
    
        return $this;
    }

    /**
     * Get nui
     *
     * @return string 
     */
    public function getNui()
    {
        return $this->nui;
    }

    /**
     * Set nuip
     *
     * @param string $nuip
     * @return Estudiante
     */
    public function setNuip($nuip)
    {
        $this->nuip = $nuip;
    
        return $this;
    }

    /**
     * Get nuip
     *
     * @return string 
     */
    public function getNuip()
    {
        return $this->nuip;
    }

    /**
     * Set numeroDocumento
     *
     * @param string $numeroDocumento
     * @return Estudiante
     */
    public function setNumeroDocumento($numeroDocumento)
    {
        $this->numeroDocumento = $numeroDocumento;
    
        return $this;
    }

    /**
     * Get numeroDocumento
     *
     * @return string 
     */
    public function getNumeroDocumento()
    {
        return $this->numeroDocumento;
    }

    /**
     * Set idSede
     *
     * @param string $idSede
     * @return Estudiante
     */
    public function setIdSede(\Colegio\AdminBundle\Entity\Sede $idSede)
    {
        $this->idSede = $idSede;
    
        return $this;
    }

    /**
     * Get idSede
     *
     * @return string 
     */
    public function getIdSede()
    {
        return $this->idSede;
    }

    /**
     * Set idTipoIdentificacion
     *
     * @param string $idTipoIdentificacion
     * @return Estudiante
     */
    public function setIdTipoIdentificacion(\Usuarios\UsuariosBundle\Entity\TipoIdentificacion $idTipoIdentificacion)
    {
        $this->idTipoIdentificacion = $idTipoIdentificacion;
    
        return $this;
    }

    /**
     * Get idTipoIdentificacion
     *
     * @return string 
     */
    public function getIdTipoIdentificacion()
    {
        return $this->idTipoIdentificacion;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Estudiante
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
    
     /**
     * Set idColegio
     *
     * @param string $idColegio
     * @return Estudiante
     */
    public function setIdColegio(\Colegio\AdminBundle\Entity\Colegio $idColegio)
    {
        $this->idColegio = $idColegio;
    
        return $this;
    }

    /**
     * Get idColegio
     *
     * @return string 
     */
    public function getIdColegio()
    {
        return $this->idColegio;
    }

    
    public function __toString() 
    {
        return $this->getPrimerApellido()." ".  $this->getSegundoApellido().", ".$this->getPrimerNombre()." ".$this->segundoNombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->gruposEstudiantes = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set nombreCompletoMadre
     *
     * @param string $nombreCompletoMadre
     * @return Estudiante
     */
    public function setNombreCompletoMadre($nombreCompletoMadre)
    {
        $this->nombreCompletoMadre = $nombreCompletoMadre;
    
        return $this;
    }

    /**
     * Get nombreCompletoMadre
     *
     * @return string 
     */
    public function getNombreCompletoMadre()
    {
        return $this->nombreCompletoMadre;
    }

    /**
     * Set telefonoMadre
     *
     * @param string $telefonoMadre
     * @return Estudiante
     */
    public function setTelefonoMadre($telefonoMadre)
    {
        $this->telefonoMadre = $telefonoMadre;
    
        return $this;
    }

    /**
     * Get telefonoMadre
     *
     * @return string 
     */
    public function getTelefonoMadre()
    {
        return $this->telefonoMadre;
    }

    /**
     * Set nombreCompletoPadre
     *
     * @param string $nombreCompletoPadre
     * @return Estudiante
     */
    public function setNombreCompletoPadre($nombreCompletoPadre)
    {
        $this->nombreCompletoPadre = $nombreCompletoPadre;
    
        return $this;
    }

    /**
     * Get nombreCompletoPadre
     *
     * @return string 
     */
    public function getNombreCompletoPadre()
    {
        return $this->nombreCompletoPadre;
    }

    /**
     * Set telefonoPadre
     *
     * @param string $telefonoPadre
     * @return Estudiante
     */
    public function setTelefonoPadre($telefonoPadre)
    {
        $this->telefonoPadre = $telefonoPadre;
    
        return $this;
    }

    /**
     * Get telefonoPadre
     *
     * @return string 
     */
    public function getTelefonoPadre()
    {
        return $this->telefonoPadre;
    }

    /**
     * Set alergias
     *
     * @param string $alergias
     * @return Estudiante
     */
    public function setAlergias($alergias)
    {
        $this->alergias = $alergias;
    
        return $this;
    }

    /**
     * Get alergias
     *
     * @return string 
     */
    public function getAlergias()
    {
        return $this->alergias;
    }

    /**
     * Add gruposEstudiantes
     *
     * @param \Colegio\EstudianteBundle\Entity\GrupoEstudiante $gruposEstudiantes
     * @return Estudiante
     */
    public function addGruposEstudiante(\Colegio\EstudianteBundle\Entity\GrupoEstudiante $gruposEstudiantes)
    {
        $this->gruposEstudiantes[] = $gruposEstudiantes;
    
        return $this;
    }

    /**
     * Remove gruposEstudiantes
     *
     * @param \Colegio\EstudianteBundle\Entity\GrupoEstudiante $gruposEstudiantes
     */
    public function removeGruposEstudiante(\Colegio\EstudianteBundle\Entity\GrupoEstudiante $gruposEstudiantes)
    {
        $this->gruposEstudiantes->removeElement($gruposEstudiantes);
    }

    /**
     * Get gruposEstudiantes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGruposEstudiantes()
    {
        return $this->gruposEstudiantes;
    }

    /**
     * Set pension
     *
     * @param boolean $pension
     * @return Estudiante
     */
    public function setPension($pension)
    {
        $this->pension = $pension;
    
        return $this;
    }

    /**
     * Get pension
     *
     * @return boolean 
     */
    public function getPension()
    {
        return $this->pension;
    }
}