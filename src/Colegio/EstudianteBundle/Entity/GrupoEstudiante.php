<?php

namespace Colegio\EstudianteBundle\Entity;

use Colegio\GrupoBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;


/**
 * GrupoEstudiante
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\EstudianteBundle\Entity\GrupoEstudianteRepository")
 */
class GrupoEstudiante
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Colegio\EstudianteBundle\Entity\Estudiante", inversedBy="gruposEstudiantes")
     */
    private $idEstudiante;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\GrupoBundle\Entity\Grupo", inversedBy="grupoEstudiantes")
     */
    private $idGrupo;

   /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaMatricula", type="datetime")
     */
    private $fechaMatricula;

    
   /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaInicio", type="datetime")
     */
    private $fechaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaFin", type="datetime")
     */
    private $fechaFin;

    /**
     * @ORM\OneToMany(targetEntity="Colegio\EstudianteBundle\Entity\Nota", mappedBy="matricula", cascade={"persist","remove"})
     * @Assert\Valid()    
     */
    private $notas;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Colegio")
     */
    private $colegio;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\PeriodoEscolar")
     */
    private $periodoEscolar;
    
    /**
     * @ORM\OneToMany(targetEntity="Colegio\DocenteBundle\Entity\Observacion", mappedBy="estudiante", cascade={"persist"})
     * @Assert\Valid()    
     */
    private $observaciones;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isActive", type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="aprobado", type="boolean", nullable=true)
     */
    private $aprobado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaAprobacion", type="datetime", nullable=true)
     */
    private $fechaAprobacion;

    /**
     * @var string
     * @ORM\Column(name="descripcionAprobacion", type="text", nullable=true)
     */
    private $descripcionAprobacion;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEstudiante
     *
     * @param string $idEstudiante
     * @return GrupoEstudiante
     */
    public function setIdEstudiante(\Colegio\EstudianteBundle\Entity\Estudiante $idEstudiante)
    {
        $this->idEstudiante = $idEstudiante;
    
        return $this;
    }

    /**
     * Get idEstudiante
     *
     * @return \Colegio\EstudianteBundle\Entity\Estudiante
     */
    public function getIdEstudiante()
    {
        return $this->idEstudiante;
    }

    /**
     * Set idGrupo
     *
     * @param string $idGrupo
     * @return GrupoEstudiante
     */
    public function setIdGrupo(\Colegio\GrupoBundle\Entity\Grupo $idGrupo)
    {
        $this->idGrupo = $idGrupo;
    
        return $this;
    }

    /**
     * Get idGrupo
     *
     * @return string 
     */
    public function getIdGrupo()
    {
        return $this->idGrupo;
    }

    /**
     * Set fechaMatricula
     *
     * @param \DateTime $fechaInicio
     * @return GrupoEstudiante
     */
    public function setFechaMatricula($fechaMatricula)
    {
        $this->fechaMatricula = $fechaMatricula;
    
        return $this;
    }

    /**
     * Get fechaMatricula
     *
     * @return \DateTime 
     */
    public function getFechaMatricula()
    {
        return $this->fechaMatricula;
    }
    
    
    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return GrupoEstudiante
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    
        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return GrupoEstudiante
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    
        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set notas
     *
     * @return \Colegio\EstudianteBundle\Entity\Nota
     */
    public function setNotas($notas)
    {
        $this->notas = $notas;
    }

    /**
     * Get notas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotas()
    {
        return $this->notas;
    }

  /**
     * Set colegio
     *
     * @param string $colegio
     * @return GrupoEstudiante
     */
    public function setColegio(\Colegio\AdminBundle\Entity\Colegio $colegio)
    {
        $this->colegio = $colegio;
    
        return $this;
    }

    /**
     * Get colegio
     *
     * @return string 
     */
    public function getColegio()
    {
        return $this->colegio;
    }
    
     /**
     * Set periodoEscolar
     *
     * @param string $periodoEscolar
     * @return GrupoEstudiante
     */
    public function setPeriodoEscolar(\Colegio\AdminBundle\Entity\PeriodoEscolar $periodoEscolar)
    {
        $this->periodoEscolar = $periodoEscolar;
    
        return $this;
    }

    /**
     * Get periodoEscolar
     *
     * @return string 
     */
    public function getPeriodoEscolar()
    {
        return $this->periodoEscolar;
    }
        
    public function __toString() 
    {
        return $this->getIdEstudiante()."-";
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->observaciones = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add notas
     *
     * @param \Colegio\EstudianteBundle\Entity\Nota $notas
     * @return GrupoEstudiante
     */
    public function addNota(\Colegio\EstudianteBundle\Entity\Nota $notas)
    {
        $this->notas[] = $notas;
    
        return $this;
    }

    /**
     * Remove notas
     *
     * @param \Colegio\EstudianteBundle\Entity\Nota $notas
     */
    public function removeNota(\Colegio\EstudianteBundle\Entity\Nota $notas)
    {
        $this->notas->removeElement($notas);
    }

    /**
     * Add observaciones
     *
     * @param \Colegio\DocenteBundle\Entity\Observacion $observaciones
     * @return GrupoEstudiante
     */
    public function addObservacione(\Colegio\DocenteBundle\Entity\Observacion $observaciones)
    {
        $this->observaciones[] = $observaciones;
    
        return $this;
    }

    /**
     * Remove observaciones
     *
     * @param \Colegio\DocenteBundle\Entity\Observacion $observaciones
     */
    public function removeObservacione(\Colegio\DocenteBundle\Entity\Observacion $observaciones)
    {
        $this->observaciones->removeElement($observaciones);
    }

    /**
     * Get observaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return GrupoEstudiante
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set aprobado
     *
     * @param boolean $aprobado
     * @return GrupoEstudiante
     */
    public function setAprobado($aprobado)
    {
        $this->aprobado = $aprobado;
    
        return $this;
    }

    /**
     * Get aprobado
     *
     * @return boolean 
     */
    public function getAprobado()
    {
        return $this->aprobado;
    }

    /**
     * Set fechaAprobacion
     *
     * @param \DateTime $fechaAprobacion
     * @return GrupoEstudiante
     */
    public function setFechaAprobacion($fechaAprobacion)
    {
        $this->fechaAprobacion = $fechaAprobacion;
    
        return $this;
    }

    /**
     * Get fechaAprobacion
     *
     * @return \DateTime 
     */
    public function getFechaAprobacion()
    {
        return $this->fechaAprobacion;
    }

    /**
     * Set descripcionAprobacion
     *
     * @param string $descripcionAprobacion
     * @return GrupoEstudiante
     */
    public function setDescripcionAprobacion($descripcionAprobacion)
    {
        $this->descripcionAprobacion = $descripcionAprobacion;
    
        return $this;
    }

    /**
     * Get descripcionAprobacion
     *
     * @return string 
     */
    public function getDescripcionAprobacion()
    {
        return $this->descripcionAprobacion;
    }
}