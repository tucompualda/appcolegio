<?php

namespace Colegio\EstudianteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Nota
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\EstudianteBundle\Entity\NotaRepository")
 */
class Nota
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;

     /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\EstudianteBundle\Entity\GrupoEstudiante", inversedBy="notas")
     */
    private $matricula;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\BoletinBundle\Entity\GrupoBoletin", inversedBy="notas")
     */
    private $grupoBoletin;

    /**
     *@Assert\Range(
     *      min = "0",
     *      max = "5",
     *      minMessage = "Mínimo 0",
     *      maxMessage = "No puedes Poner una nota mayor a 5",
     *      invalidMessage = "Debe ser un número"
     * )
     * @ORM\Column(name="calificacion", type="float")
     */
    private $calificacion;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
    *
     * @ORM\Column(name="fallas", type="float")
     */
    private $fallas;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set matricula
     *
     * @param \Colegio\EstudianteBundle\Entity\GrupoEstudiante $matricula
     * @return Nota
     */
    public function setMatricula(\Colegio\EstudianteBundle\Entity\GrupoEstudiante $matricula)
    {
        $this->matricula = $matricula;
    
        return $this;
    }

    /**
     * Get matricula
     *
     * @return \Colegio\EstudianteBundle\Entity\GrupoEstudiante 
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * Set grupoBoletin
     *
     * @param \Colegio\BoletinBundle\Entity\GrupoBoletin $grupoBoletin
     * @return Nota
     */
    public function setGrupoBoletin(\Colegio\BoletinBundle\Entity\GrupoBoletin $grupoBoletin)
    {
        $this->grupoBoletin = $grupoBoletin;
    
        return $this;
    }

    /**
     * Get grupoBoletin
     *
     * @return \Colegio\BoletinBundle\Entity\GrupoBoletin
     */
    public function getGrupoBoletin()
    {
        return $this->grupoBoletin;
    }

    /**
     * Set calificacion
     *
     * @param string $calificacion
     * @return Nota
     */
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;
    
        return $this;
    }

    /**
     * Get calificacion
     *
     * @return string 
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Nota
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Nota
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }


     /**
     * Set fallas
     *
     * @param string $fallas
     * @return Nota
     */
    public function setFallas($fallas)
    {
        $this->fallas = $fallas;
    
        return $this;
    }

    /**
     * Get fallas
     *
     * @return string 
     */
    public function getFallas()
    {
        return $this->fallas;
    }

    
    //Método para Cargar un valor por defecto
    public function __toString() 
    {
        return $this->getCalificacion()." ";
    }
}
