<?php

namespace Colegio\EstudianteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class NotaType extends AbstractType
{
	public function __construct($matricula,$asignatura)
	{
		$this->matricula = $matricula;
		$this->asignatura = $asignatura;
	}
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$self = $this;
        $builder
        
            ->add('calificacion','number',array(
				
				'attr'=>array('min'=>0,'max'=>5,'step'=>0.1,'class'=>'form-control'),
				
            ))
            ->add('descripcion','textarea',array(
					'attr'=>array('class'=>'form-control','rows'=>'3'),
					'required'=>false,
					
			))
			->add('grupoBoletin','entity',array(
                'class' => 'ColegioBoletinBundle:GrupoBoletin',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('u')
                                ->where('u.grupoAsignatura = :asignatura')
                                ->setParameter('asignatura', $self->asignatura);
                },
                 'label'=>'Boletin',
                 'required'=> true,
            ))
			->add('matricula','entity',array(
                'class' => 'ColegioEstudianteBundle:GrupoEstudiante',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('u')
                                ->where('u.id = :matricula')
                                ->setParameter('matricula', $self->matricula);
                },
                 'label'=>'Estudiante',
                 'required'=> true,
            ))
            ->add('fallas')
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\EstudianteBundle\Entity\Nota'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_estudiantebundle_nota';
    }
}
