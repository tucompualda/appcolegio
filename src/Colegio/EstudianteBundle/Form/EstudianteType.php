<?php

namespace Colegio\EstudianteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class EstudianteType extends AbstractType
{
    public function __construct($colegio)
    {
        $this->colegio = $colegio;
    } 
     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $self = $this; 
        $builder
             ->add('idSede','entity',array(
                'class' => 'ColegioAdminBundle:Sede',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('u')
                                ->where('u.idDetalleColegio = :sede')
                                ->setParameter('sede', $self->colegio);
                },
                 'label'=>'Sede',
                 'empty_value'=>'Escoge una sede',
                 'required'=> true,
            ))
            ->add('idTipoIdentificacion',null,array(
                'label'=>'Tipo de Documento'
            ))
            ->add('numeroDocumento')
            ->add('primerNombre')
            ->add('segundoNombre')
            ->add('primerApellido')
            ->add('segundoApellido')
            ->add('telefono')
            ->add('email','email',array(
                'label'=>'Email/usuario para accesar *(requerido)'
            ))
            ->add('nui','email',array(
                'label'=>'Email contacto',
                'required'=>false,
                'attr'=>array('placeholder'=>'Mismo usuario en caso de no tener a la mano')
            ))
            ->add('nuip')
            
            ->add('password','password',array(
                    'required'=>false,
                    ))
            ->add('isActive',null,array(
                       'required'=>false,
                       'attr'=>array('class'=>'switch-left switch-success'),
                       'label'=>'Estado'
                ))
            ->add('pension',null,array(
                       'required'=>false,
                       'attr'=>array('class'=>'switch-left switch-success'),
                       'label'=>'Estado Pensión'
                ))
            ->add('nombreCompletoMadre')
            ->add('telefonoMadre')
            ->add('nombreCompletoPadre')
            ->add('telefonoPadre')
            ->add('alergias')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\EstudianteBundle\Entity\Estudiante'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_estudiantebundle_estudiante';
    }
}
