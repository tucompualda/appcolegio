<?php

namespace Colegio\EstudianteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class GrupoEstudianteType extends AbstractType
{
    public function __construct($sede) 
    {

        $this->sede = $sede;
        $this->hoy = new \DateTime('now');  
    }
    
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $self = $this;
        $builder
           /* ->add('periodoEscolar','entity',array(
                'class'=>'ColegioAdminBundle:PeriodoEscolar',
                'query_builder'=>function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('e')
                                ->where('e.colegio = :colegio')
                                ->setParameter('colegio',$self->sede);
                }
            ))*/
            ->add('idEstudiante','entity',array(
                'class'=>'ColegioEstudianteBundle:Estudiante',
                'query_builder'=>function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('e')
                                ->where('e.idSede = :sede AND e.isActive = true')
                                ->orderBy('e.primerApellido','asc')
                                ->setParameter('sede',$self->sede);
                        
                },
               
                'label'=>'Selecciona un estudiante',
                'empty_value'=>'Selecciona',
                
            ))
            ->add('idGrupo','entity',array(
                'class'=>'ColegioGrupoBundle:Grupo',
                'label'=>'Grupo',
                'empty_value'=>'Selecciona',
                'query_builder'=> function(EntityRepository $er) use($self){
                       return $er->createQueryBuilder('u')
                               ->where('u.idSede = :idSede  AND u.fechaInicio <= :hoy AND u.fechaFin >= :hoy')
                               ->setParameter('idSede', $self->sede)
                               ->setParameter('hoy',$self->hoy);
                }
            ))
                ->add('fechaInicio',null,array(
                        'widget'=>'single_text',
                        'format' => 'yyyy-MM-dd',
                    ))
                ->add('fechaFin',null,array(
                        'widget'=>'single_text',
                        'format' => 'yyyy-MM-dd',
                    ));
        
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\EstudianteBundle\Entity\GrupoEstudiante'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_estudiantebundle_grupoestudiante';
    }
}
