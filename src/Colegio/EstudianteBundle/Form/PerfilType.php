<?php

namespace Colegio\EstudianteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class PerfilType extends AbstractType
{

     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
            //->add('numeroDocumento')
        $builder
            ->add('telefono')
            //->add('email')
            ->add('nui','email',array(
                    'label'=>'Email',
                    'required'=>true,
                    'attr'=>array('type'=>'email'),

                ))
            //->add('nuip')
            
            ->add('password','password',array(
                    'required'=>false,
                    ))
            ->add('nombreCompletoMadre')
            ->add('telefonoMadre')
            ->add('nombreCompletoPadre')
            ->add('telefonoPadre')
            ->add('alergias')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\EstudianteBundle\Entity\Estudiante'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_estudiantebundle_perfil';
    }
}
