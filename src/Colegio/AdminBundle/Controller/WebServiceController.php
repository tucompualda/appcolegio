<?php

namespace Colegio\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Colegio\BoletinBundle\Entity\Boletin;
use Colegio\BoletinBundle\Entity\GrupoBoletin;
use Colegio\BoletinBundle\Entity\Logro;

/**
 * Description of WebServiceController
 *
 * @author flia
 */
class WebServiceController extends Controller
{
    
    public function periodosAction($colegio)
    {
       $em = $this->getDoctrine()->getManager();
       $entity = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodosTotal($colegio);
       
       $jsonp = new JsonResponse($entity);
       //$jsonp->setCallback('myCallback');
       return $jsonp;
    }

    /**
    * usado para crear boletines desde el admin en la parte de los grupos
    */
    public function crearLogrosAction($grupoasignatura,$boletinrecibido,$periodo,$categoria,$logro)
    {
       $em = $this->getDoctrine()->getManager();
       $usuarioActivo = $this->obtenerUsuario();
       $colegio = $this->obtenerColegio($usuarioActivo);
       //$periodo = $this->obtenerPeriodo($colegio);

       $periodoReal = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodo);

       $grupoBoletin = $em->getRepository('ColegioBoletinBundle:GrupoBoletin')->findOneBy(array(
          'grupoAsignatura'=>$grupoasignatura,
          'periodo'=>$periodo,
          'colegio'=>$colegio
        ));

       //$boletinReal = $em->getRepository('ColegioBoletinBundle:Boletin')->find($boletinrecibido);
       //$logros = $em->getRepository('ColegioBoletinBundle:Logro')->findByIdBoletin($boletinReal);

       $categoriaReal = $em->getRepository('ColegioBoletinBundle:CategoriaLogro')->find($categoria);

       $hoy = new \DateTime('now');
       //$asignatura = $boletinReal->getAsignatura();
       $descripcion = "Insertado dinamicamente";
       $grupoAsignaturaReal = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->find($grupoasignatura);
       $asignatura = $grupoAsignaturaReal->getIdAsignatura();

    if($grupoBoletin==null){
          
           $boletin = new Boletin();
           $boletin->setAsignatura($asignatura);
           $boletin->setDescripcion($descripcion);
           $boletin->setFechaInicio($hoy);
           $boletin->setFechaFin($hoy);
           $boletin->setActivo(true);
           $boletin->setColegio($colegio);
           $boletin->setPeriodo($periodoReal);

           $em->persist($boletin);
           $em->flush();

           $grupoBoletin = new GrupoBoletin();
           $grupoBoletin->setBoletin($boletin);
           $grupoBoletin->setGrupoAsignatura($grupoAsignaturaReal);
           $grupoBoletin->setPeriodo($periodoReal);
           $grupoBoletin->setColegio($colegio);

           $em->persist($grupoBoletin);
           $em->flush();

           $logros = new Logro();
           $logros->setIdBoletin($boletin);
           $logros->setCategoria($categoriaReal);
           $logros->setDescripcion($logro);
     
            $em->persist($logros);
            $em->flush();

     }else{
           $boletin = $grupoBoletin->getBoletin();


           $logros = new Logro();
           $logros->setIdBoletin($boletin);
           $logros->setCategoria($categoriaReal);
           $logros->setDescripcion($logro);
     
            $em->persist($logros);
            $em->flush();
     }

       $jsonp = new Response("ok");
       //$jsonp->setCallback('myCallback');
       return $jsonp;
     
    }

    /**
    * Usado para traer los boletines compatibles con una determinada materia con el fin de importar
    */
    public function consultaBoletinesAction($asignatura,$nivel)
    {
       $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();

      /// asignatura recibido es el id del grupoAsignatura
       $asignaturaReal =  $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->find($asignatura);
       $idAsignatura = $asignaturaReal->getIdAsignatura()->getId();
       $entity = $em->getRepository('ColegioBoletinBundle:Boletin')->consultaBoletines($idAsignatura,$idColegio,$nivel);
       
       $jsonp = new JsonResponse($entity);
       //$jsonp->setCallback('myCallback');
       return $jsonp;
    }
    
    /**
    * importar un boletin
    */
    public function importarBoletinAction($grupoasignatura,$boletinrecibido,$periodo)
    {
       $em = $this->getDoctrine()->getManager();
       $usuarioActivo = $this->obtenerUsuario();
       $colegio = $this->obtenerColegio($usuarioActivo);
       //$periodo = $this->obtenerPeriodo($colegio);

       $periodoReal = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodo);

       $boletinReal = $em->getRepository('ColegioBoletinBundle:Boletin')->find($boletinrecibido);
       $logros = $em->getRepository('ColegioBoletinBundle:Logro')->findByIdBoletin($boletinReal);

       $hoy = new \DateTime('now');
       $asignatura = $boletinReal->getAsignatura();
       $descripcion = "Insertado dinamicamente";
       $grupoAsignaturaReal = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->find($grupoasignatura);


     $boletin = new Boletin();
     $boletin->setAsignatura($asignatura);
     $boletin->setDescripcion($descripcion);
     $boletin->setFechaInicio($hoy);
     $boletin->setFechaFin($hoy);
     $boletin->setActivo(true);
     $boletin->setColegio($colegio);
     $boletin->setPeriodo($periodoReal);

     $em->persist($boletin);
     $em->flush();

     $grupoBoletin = new GrupoBoletin();
     $grupoBoletin->setBoletin($boletin);
     $grupoBoletin->setGrupoAsignatura($grupoAsignaturaReal);
     $grupoBoletin->setPeriodo($periodoReal);
     $grupoBoletin->setColegio($colegio);

     $em->persist($grupoBoletin);
     $em->flush();

     foreach ($logros as $key) {
       
       $descripcionLogro = $key->getDescripcion();
       $categoriaLogro = $key->getCategoria();

       $logrosNuevo = new Logro();
       $logrosNuevo->setIdBoletin($boletin);
       $logrosNuevo->setCategoria($categoriaLogro);
       $logrosNuevo->setDescripcion($descripcionLogro);
     
        $em->persist($logrosNuevo);
        $em->flush();
     }

     $mensaje = "OK insertado";
     $respuesta = new Response($mensaje);
     return $respuesta;

    }

  public function obtenerPeriodo($idColegio)
  {
    $em = $this->getDoctrine()->getManager();
    $hoy = new \DateTime('now');
        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($idColegio);
        
        if($periodo==null){
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Solicita por favor que habiliten un periodo escolar... con tu administrador');
            return $this->redirect($this->generateUrl('colegio_docente_homepage'));
        }
        $idperiodo = $periodo->getId();
    return $idperiodo;
  }


  public function obtenerColegio($usuarioActivo)
  {
        $idColegio = $usuarioActivo->getIdColegio();
        return $idColegio;
  }

  public function obtenerUsuario(){
    $usuarioActivo = $this->get('security.context')->getToken()->getUser();
    return $usuarioActivo;    
  }
    
    public function notasAction($trabajo,$matricula,$calificacion,$estado,$grupoAsignatura)
    {
      
       if($calificacion > 5){          
           $respuesta = "<div class='alert alert-block'><i class='fa fa-exclamation-triangle'></i> No se pueden valores mayores a 5 </div>";
       }else{ 
           
          $em = $this->getDoctrine()->getManager();
          $hoy = new \DateTime('now');
          $gAsig = $em->getRepository('ColegioGrupoBundle:GrupoAsignatura')->find($grupoAsignatura);

          $conTrabajo = $em->getRepository('ColegioDocenteBundle:Trabajos')->find($trabajo);

          $grupoEstudiante = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->find($matricula);
          $trabajoFin = $em->getRepository('ColegioDocenteBundle:Trabajos')->find($trabajo);
          $inscripcion = $em->getRepository('ColegioBoletinBundle:InscripcionAsignatura')->findOneBy(array(
               'matricula'        => $grupoEstudiante,
               'grupoAsignatura'  => $gAsig
           ));

          if($inscripcion == null){
              $inscripcionAsignatura = new \Colegio\BoletinBundle\Entity\InscripcionAsignatura();
              $inscripcionAsignatura->setFechaInscripcion($hoy);
              $inscripcionAsignatura->setGrupoAsignatura($gAsig);
              $inscripcionAsignatura->setMatricula($grupoEstudiante);
              $em->persist($inscripcionAsignatura);
              $em->flush();
              $inscripcion = $inscripcionAsignatura;
          }

         $consultaNota = $em->getRepository('ColegioDocenteBundle:NotaTrabajo')->findOneBy(array(
             'trabajo'=> $conTrabajo,
             'inscripcionAsignatura'=>$inscripcion
         ));

         if($consultaNota==null){
             $nota = new \Colegio\DocenteBundle\Entity\NotaTrabajo();
               $nota->setCalificacion($calificacion);
               $nota->setEstado($estado);
               $nota->setFechaCalificacion($hoy);
               $nota->setTrabajo($trabajoFin);
               $nota->setInscripcionAsignatura($inscripcion);

               $em->persist($nota);
               $em->flush();
               $respuesta = "<i class='fa fa-check-circle'></i>";//insertado
         }else{
             $consultaNota->setCalificacion($calificacion);
             $em->flush();
             $respuesta = "<i class='fa fa-refresh fa-spin'></i>"; //actualizado
         }
       }       
            
      
      // $jsonp = new JsonResponse($entity);
       //$jsonp->setCallback('myCallback');
       
       $response = new Response($respuesta);
       return $response;
    }
    
    public function cargarAction($trabajo, $inscripcionAsignatura)
    {
        $respuesta = "Cargando Notas...";
        $response = new Response($respuesta);
       return $response;
    }
    
    public function totalPorcentajeAction($periodo)
    {
         $em = $this->getDoctrine()->getManager();
         $per = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodo);
         $total = $em->getRepository('ColegioAdminBundle:PorcentajeTrabajo')->findPorcentajesTotal($per);
         
         $fin = 0;
         foreach ($total as $resultado){
             $fin = $resultado['total'];
         }
         
         $response = new Response($fin);
         return $response;
    }
    
    public function porcentajesAction($tipotrabajo,$periodo,$porcentaje)
    {
       $hoy = new \DateTime('now');
       $em = $this->getDoctrine()->getManager();
       $tipo = $em->getRepository('ColegioAdminBundle:TipoTrabajo')->find($tipotrabajo);
       $per = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodo);
       
        $porcentajeTrabajos = $em->getRepository('ColegioAdminBundle:PorcentajeTrabajo')->findOneBy(array(
                'tipoTrabajo'=>$tipo,
                'periodo'    =>$per
            ));
       
       if($porcentaje>100){
           $respuesta = "<div class='alert alert-error'>
                                <i class='icon-remove-sign'></i> No se pueden valores mayores a 100%
                        </div>";
       }else{
           
           

            $porcentajeNuevo = new \Colegio\AdminBundle\Entity\PorcentajeTrabajo();


                     if($porcentajeTrabajos == null){
                          $porcentajeNuevo->setEstado(true);
                          $porcentajeNuevo->setFechaCreacion($hoy);
                          $porcentajeNuevo->setFechaModificacion($hoy);
                          $porcentajeNuevo->setPeriodo($per);
                          $porcentajeNuevo->setPorcentaje($porcentaje);
                          $porcentajeNuevo->setTipoTrabajo($tipo);
                          $em->persist($porcentajeNuevo);
                          $em->flush();


                        $respuesta = "<i class='icon-ok-circle'></i> Agregado con éxito!"; 

                        }else{

                            $porcentajeTrabajos->setFechaModificacion($hoy);
                            $porcentajeTrabajos->setPorcentaje($porcentaje);
                            $em->flush();

                           $respuesta = "<i class='icon-ok-circle'></i> Actualizando!";
                     }
       }
       
       $response = new Response($respuesta);
       return $response;
        
    }
    
    public function actualizarEstandarAction($periodo,$valor)
    {
         $em = $this->getDoctrine()->getManager();
         $periodoRecogido = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodo);
         
         $periodoRecogido->setPorcentajeEstandar($valor);
         $em->flush();
         $respuesta = "OK actualizado Estandar";
         $response = new Response($respuesta);
         return $response;
        
    }
    
     public function actualizarEstablecerAction($periodo,$valor)
    {
         $em = $this->getDoctrine()->getManager();
         $periodoRecogido = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodo);
         
         $periodoRecogido->setEstablecer($valor);
         $em->flush();
         $respuesta = "OK actualizado Establecer";
         $response = new Response($respuesta);
         return $response;
        
    }

    public function actualizarBoletinDocenteAction($periodo,$valor)
    {
         $em = $this->getDoctrine()->getManager();
         $periodoRecogido = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodo);
         
         $periodoRecogido->setBoletinDocente($valor);
         $em->flush();
         $respuesta = "OK actualizado Estandar";
         $response = new Response($respuesta);
         return $response;
        
    }

    public function fortalezasAction($periodo,$valor)
    {
         $em = $this->getDoctrine()->getManager();
         $periodoRecogido = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodo);
         
         $periodoRecogido->setFortaleza($valor);
         $em->flush();
         $respuesta = "OK actualizado Estandar";
         $response = new Response($respuesta);
         return $response;
        
    }

    public function debilidadesAction($periodo,$valor)
    {
         $em = $this->getDoctrine()->getManager();
         $periodoRecogido = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodo);
         
         $periodoRecogido->setDebilidad($valor);
         $em->flush();
         $respuesta = "OK actualizado Estandar";
         $response = new Response($respuesta);
         return $response;
        
    }
    
    public function itemNormaAction($parametro,$item,$limite,$inferior,$descripcionCualitativa,$notaCualitativa)
    {
        $em = $this->getDoctrine()->getManager();
        
        $itemVerificar = $em->getRepository('ColegioAdminBundle:ParametrosItems')->findOneBy(array(
            'parametro'=>$parametro,
            'item'=>$item
        ));
        
        $itemreal = $em->getRepository('ColegioAdminBundle:Item')->find($item);
        $parametroReal = $em->getRepository('ColegioAdminBundle:Parametros')->find($parametro);
        
        if($itemVerificar == null){
            // crear registro
            $nuevoRegistro = new \Colegio\AdminBundle\Entity\ParametrosItems();
            $nuevoRegistro->setItem($itemreal);
            if($limite == "null"){

            }else{
                $nuevoRegistro->setLimiteSuperior($limite);
            }

            if($inferior == "null"){

            }else{
                $nuevoRegistro->setLimiteInferior($inferior);
            }

            if($descripcionCualitativa == "null"){

           }else{
             $nuevoRegistro->setDescripcionCualitativa($descripcionCualitativa);
           }

           if($notaCualitativa == "null"){

           }else{
             $nuevoRegistro->setNotaCualitativa($notaCualitativa);
           }
            
            $nuevoRegistro->setParametro($parametroReal);
            
            $em->persist($nuevoRegistro);
            $em->flush();
            $respuesta = "OK Insertado";
            
        }else{
            //actualizar limite
           if($limite == "null"){

            }else{
               $itemVerificar->setLimiteSuperior($limite);
            }

           if($inferior == "null"){

            }else{
             $itemVerificar->setLimiteInferior($inferior);
           }

           if($descripcionCualitativa == "null"){

           }else{
             $itemVerificar->setDescripcionCualitativa($descripcionCualitativa);
           }

           if($notaCualitativa == "null"){

           }else{
             $itemVerificar->setNotaCualitativa($notaCualitativa);
           }

            $em->flush();
            $respuesta = "OK actualizado";
        }
        
         
         $response = new Response($respuesta);
         return $response;
    }
   


   ////////////// estadisticas //////////////////
    public function trabajosperiodoAction($periodo)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->obtenerUsuario();
        $colegio = $this->obtenerColegio($usuarioActivo);

        $trabajos = $em->getRepository('ColegioDocenteBundle:Trabajos')->findTrabajosPeriodo($periodo);
        $response = new JsonResponse($trabajos);
        return $response;
    }
    
     public function trabajosperiodoconsolidadoAction($periodo,$tipotrabajo)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->obtenerUsuario();
        $colegio = $this->obtenerColegio($usuarioActivo);

        $notas = $em->getRepository('ColegioDocenteBundle:NotaTrabajo')->notasperiodotrabajo($periodo,$tipotrabajo);
        $rows = array();

        foreach ($notas as $r) {
              $row[0] = $r['escala'];
              $row[1] = $r['total'];
              array_push($rows,$row);
        }
        $response = new Response();
        $response->setContent(json_encode($rows, JSON_NUMERIC_CHECK));
        $response->headers->set('Content-Type', 'application/json');
        
         
        return $response;
    }
    

    /**
    * Cambiar Fecha de tareas
    */
    public function cambiofechaAction($tarea,$fecha)
    {
       $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();

      /// asignatura recibido es el id del grupoAsignatura
       $trabajo =  $em->getRepository('ColegioDocenteBundle:Trabajos')->find($tarea);
       $fechaModificada = new \DateTime($fecha);
       $trabajo->setFechaEntrega($fechaModificada);
       $em->flush();
       $jsonp = new JsonResponse($fechaModificada);
       //$jsonp->setCallback('myCallback');
       return $jsonp;
    }
    /**
    * Calcular promedios acumulados
    */
    public function calcularPromediosAcumuladosAction($periodoRecibido)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();

        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findOneBy(array(
            'id'=>$periodoRecibido,
            'colegio'=>$idColegio
        ));

        $periodoEscolar = $periodo->getPeriodoEscolar()->getId();
        
        $notasQuery = $em->createQuery("
            SELECT 
              AVG(n.calificacion) as PromedioAcumulado,
              m.id as matricula,              
              ga.id as grupoAsignatura,
              ia.id as InscripcionAsignatura
            FROM ColegioEstudianteBundle:Nota n
            INNER JOIN n.grupoBoletin gb 
            INNER JOIN gb.grupoAsignatura ga
            INNER JOIN n.matricula m 
            INNER JOIN ga.inscripcionAsignatura ia 
            INNER JOIN gb.periodo p 
            INNER JOIN m.idEstudiante e 
            INNER JOIN ga.idAsignatura a         
            WHERE p.periodoEscolar = :periodo AND gb.colegio = :colegio
            AND p.fechaFin <= :fechaFin AND ia.matricula = m.id AND ga.isActive = true
            GROUP BY m.id,ga.id
        ");
        $notasQuery->setParameter('periodo',$periodoEscolar);
        $notasQuery->setParameter('colegio',$idColegio);
        $notasQuery->setParameter('fechaFin',$periodo->getFechaFin());

        $notas = $notasQuery->getArrayResult();
        foreach ($notas as $key) {
          $inscripcionAsignatura = $em->getRepository('ColegioBoletinBundle:InscripcionAsignatura')->find($key['InscripcionAsignatura']);
          $inscripcionAsignatura->setAcumulado($key['PromedioAcumulado']); 
          $inscripcionAsignatura->setDescripcionAcumulado("Último de ".$periodo);  
        }

        $em->flush();
        $jsonp = new JsonResponse("Actualizado");
        return $jsonp;
    }

    public function cerrarPeriodoAction($periodo,$estado)
    {   

        $estadoPeriodo = null;
        if($estado == 'cerrar'){
          $estadoPeriodo = true;
        }else if($estado == 'abrir'){
          $estadoPeriodo = false;
        }

        $em = $this->getDoctrine()->getManager();
        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodo);
        $periodo->setCerrarPeriodo($estadoPeriodo);
        $em->flush();
        $jsonp = new JsonResponse("Periodo cerrado correctamente");
        return $jsonp;
    }

    public function promoverEstudiantesAction($periodoEscolar,$periodoActual,$parcial)
    {
        $mensajeError = "";
        $em = $this->getDoctrine()->getManager();
        $periodo = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->find($periodoEscolar);

        $validarPeriodos = $this->validarPeriodosCerradosAction($periodoEscolar);
        if($validarPeriodos == 0){
          $mensajeError = "Validados los períodos cerrados";
        }else{
          $mensajeError = ("Hay ".$validarPeriodos." período(s) sin cerrar");
        }

        $totalTrabajos = $this->totalTrabajosAction($periodoActual);
        $mensajeError = $mensajeError." --- Total trabajos: ".$totalTrabajos;

        $totalEstudiantes = $this->totalEstudiantesAction($periodoEscolar);
        $mensajeError = $mensajeError. " -- Total matrículas: ".$totalEstudiantes;


        $totalPlanillas = $this->totalGruposAsignaturaAction($periodoActual);
        $mensajeError = $mensajeError. " -- Total planillas: ".$totalPlanillas;

        $totalNotas = $this->totalCalificacionesAction($periodoActual);
        $mensajeError = $mensajeError. " -- Total calificaciones planillas: ".$totalNotas;

        $parametroQuery = $em->createQuery('
          SELECT pa,pe FROM ColegioAdminBundle:Parametros pa
          INNER JOIN pa.periodoEscolar pe
          WHERE pa.periodoEscolar = :periodoEscolar
        ');
        $parametroQuery->setParameter('periodoEscolar',$periodoEscolar);
        $parametro = $parametroQuery->getOneOrNullResult();

        if($parcial == "true"){
                       
            $matriculasQuery = $em->createQuery('SELECT g FROM ColegioEstudianteBundle:GrupoEstudiante g'
                    . ' WHERE g.periodoEscolar = :periodoEscolar AND g.isActive = true AND (g.aprobado = false OR g.aprobado IS NULL)');
            $matriculasQuery->setParameter('periodoEscolar',$periodoEscolar);
            $matriculas = $matriculasQuery->getResult();
            
        }else{
             $matriculas = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findBy(
                array(
                  'periodoEscolar'=>$periodoEscolar,
                  'isActive'=>true
                )
              );
        }
       

        $erroresNotaMinima = 0;
        foreach ($matriculas as $key) {
          $this->validarEstudianteAction($key->getId(),$parametro->getNotaMinima());
        }
        //var_dump($parametro->getNotaMinima());
        $jsonp = new JsonResponse($mensajeError);
        return $jsonp;
    }

    /**
    * Proceso para validar la matrícula de un sólo estudiante
    */
    public function validarEstudianteAction($matricula,$notaMinima)
    {
        $em = $this->getDoctrine()->getManager();         
        $matriQuery = "";

      
        $matriQuery = $em->createQuery('
            SELECT g FROM ColegioEstudianteBundle:GrupoEstudiante g
            WHERE g.id = :matricula
        ');
        

        $matriQuery->setParameter('matricula',$matricula);
        $matriQuery->setMaxResults(1);
        $matri = $matriQuery->getOneOrNullResult();

        $totalInscripcionesAsignaturasQuery = $em->createQuery('
            SELECT i,ga FROM ColegioBoletinBundle:InscripcionAsignatura i
            INNER JOIN i.grupoAsignatura ga
            WHERE ga.isActive = true AND i.matricula = :matricula
         ');

         $totalInscripcionesAsignaturasQuery->setParameter('matricula',$matricula);
         $totalInscripcionesAsignaturas = $totalInscripcionesAsignaturasQuery->getResult();

       
       $errores = 0;
       $mensajeError = "";
       foreach ($totalInscripcionesAsignaturas as $key) {
          if(number_format($key->getAcumulado(),1) < $notaMinima){
            $errores = $errores + 1;
            $mensajeError = $mensajeError." ".$key->getGrupoAsignatura()." nota acumulada: ".number_format($key->getAcumulado(),1).". (Mínimo: ". $notaMinima .") ";
          }
       }

       if($errores == 0){
          $matri->setAprobado(true);
          $matri->setFechaAprobacion(new \DateTime());
          $matri->setDescripcionAprobacion("¡Felicitaciones has sido promovido!");
       }else if($errores >= 1){
          $matri->setAprobado(false);
          $matri->setFechaAprobacion(new \DateTime());
          $matri->setDescripcionAprobacion($mensajeError);
       }
       $em->flush();

       $jsonp = new JsonResponse($errores);
       return $jsonp;
    }

    public function totalTrabajosAction($periodoActual)
    {
        $em = $this->getDoctrine()->getManager();
        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodoActual);
        $trabajos = $em->getRepository('ColegioDocenteBundle:Trabajos')->findBy(array(
          'periodo' => $periodo,
          'estado'  => true
        ));
        $total = count($trabajos);
        return $total;
    }

    public function totalEstudiantesAction($periodoEscolar)
    {
        $em = $this->getDoctrine()->getManager();
        $matriculas = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->findBy(
          array(
            'periodoEscolar'=>$periodoEscolar,
            'isActive'=>true
          )
        );

        $total = count($matriculas);
        return $total;
    }

    public function totalGruposAsignaturaAction($periodoActual)
    {
        $em = $this->getDoctrine()->getManager();
        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodoActual);
        $gruposAsignaturaQuery = $em->createQuery('
            SELECT ga.id AS total 
            FROM ColegioDocenteBundle:Trabajos t 
            INNER JOIN t.grupoAsignatura ga
            WHERE t.estado = true AND t.periodo = :periodo AND ga.isActive = true
            GROUP BY ga.id
        ');
        $gruposAsignaturaQuery->setParameter('periodo',$periodoActual);

        $gruposAsignatura = $gruposAsignaturaQuery->getResult();
        $total = count($gruposAsignatura);
        return $total;
    }

    public function totalCalificacionesAction($periodoActual)
    {
        $em = $this->getDoctrine()->getManager();
        $calificacionesQuery = $em->createQuery('
          SELECT nt.id
          FROM
          ColegioDocenteBundle:NotaTrabajo nt
          INNER JOIN nt.trabajo t 
          WHERE t.estado = true AND t.periodo = :periodo
          ');
        $calificacionesQuery->setParameter('periodo',$periodoActual);

        $total = count($calificacionesQuery->getResult());
        return $total;
    }

    public function validarPeriodosCerradosAction($periodoEscolar)
    {
        $em = $this->getDoctrine()->getManager();
        $periodos = $em->getRepository('ColegioBoletinBundle:Periodo')->findBy(
          array(
            'periodoEscolar'=>$periodoEscolar
          )
        );
        $errores = 0;
        foreach ($periodos as $key) {
          if($key->getCerrarPeriodo() == true){

          }else{
            $errores = $errores + 1;
          }
        }

        return $errores;
    }

}
