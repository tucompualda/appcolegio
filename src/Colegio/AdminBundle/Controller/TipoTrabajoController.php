<?php

namespace Colegio\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\AdminBundle\Entity\TipoTrabajo;
use Colegio\AdminBundle\Form\TipoTrabajoType;

/**
 * TipoTrabajo controller.
 *
 */
class TipoTrabajoController extends Controller
{

    /**
     * Lists all TipoTrabajo entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $entities = $em->getRepository('ColegioAdminBundle:TipoTrabajo')->findByColegio($idColegio);

        return $this->render('ColegioAdminBundle:TipoTrabajo:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TipoTrabajo entity.
     *
     */
    public function createAction(Request $request)
    {
    	$usuarioActivo = $this->get('security.context')->getToken()->getUser();
    	$idColegio = $usuarioActivo->getIdColegio();
        $entity = new TipoTrabajo();
        $hoy = new \DateTime('now');
        $entity->setFechaCreacion($hoy);
        $entity->setColegio($idColegio);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipotrabajo_show', array('id' => $entity->getId())));
        }

        return $this->render('ColegioAdminBundle:TipoTrabajo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a TipoTrabajo entity.
    *
    * @param TipoTrabajo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(TipoTrabajo $entity)
    {
    	$usuarioActivo = $this->get('security.context')->getToken()->getUser();
    	$idColegio = $usuarioActivo->getIdColegio();
        $form = $this->createForm(new TipoTrabajoType($idColegio), $entity, array(
            'action' => $this->generateUrl('tipotrabajo_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoTrabajo entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoTrabajo();
        $form   = $this->createCreateForm($entity);

        return $this->render('ColegioAdminBundle:TipoTrabajo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TipoTrabajo entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:TipoTrabajo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoTrabajo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioAdminBundle:TipoTrabajo:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing TipoTrabajo entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:TipoTrabajo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoTrabajo entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioAdminBundle:TipoTrabajo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TipoTrabajo entity.
    *
    * @param TipoTrabajo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoTrabajo $entity)
    {
    	$usuarioActivo = $this->get('security.context')->getToken()->getUser();
    	$idColegio = $usuarioActivo->getIdColegio();
    	$form = $this->createForm(new TipoTrabajoType($idColegio), $entity, array(
            'action' => $this->generateUrl('tipotrabajo_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing TipoTrabajo entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:TipoTrabajo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoTrabajo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tipotrabajo_edit', array('id' => $id)));
        }

        return $this->render('ColegioAdminBundle:TipoTrabajo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TipoTrabajo entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioAdminBundle:TipoTrabajo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoTrabajo entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipotrabajo'));
    }

    /**
     * Creates a form to delete a TipoTrabajo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipotrabajo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
