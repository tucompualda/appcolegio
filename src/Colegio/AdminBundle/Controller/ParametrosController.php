<?php

namespace Colegio\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\AdminBundle\Entity\Parametros;
use Colegio\AdminBundle\Form\ParametrosType;

/**
 * Parametros controller.
 *
 */
class ParametrosController extends Controller
{

    /**
     * Lists all Parametros entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();

        $entities = $em->getRepository('ColegioAdminBundle:Parametros')->findByColegio($idColegio);

        return $this->render('ColegioAdminBundle:Parametros:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Parametros entity.
     *
     */
    public function createAction(Request $request)
    {
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $entity = new Parametros();
        $entity->setColegio($idColegio);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');
            return $this->redirect($this->generateUrl('parametros_show', array('id' => $entity->getId())));
        }

        return $this->render('ColegioAdminBundle:Parametros:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Parametros entity.
    *
    * @param Parametros $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Parametros $entity)
    {
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $entity->setNotaMinima(3);
        $form = $this->createForm(new ParametrosType($idColegio), $entity, array(
            'action' => $this->generateUrl('parametros_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Parametros entity.
     *
     */
    public function newAction()
    {
        $entity = new Parametros();
        $form   = $this->createCreateForm($entity);

        return $this->render('ColegioAdminBundle:Parametros:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Parametros entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $entity = $em->getRepository('ColegioAdminBundle:Parametros')->findOneBy(array(
                        'id'     =>$id,
                        'colegio'=>$idColegio,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametros entity.');
        }
        
        if (!$entity->getNorma()) {
           // throw $this->createNotFoundException('No se ha definido el valor de la Norma.');
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Por favor selecciona el valor y ACTUALIZA la Norma en los Parametros!');
            return $this->redirect($this->generateUrl('parametros_edit', array('id' => $id)));
        }
        
        $norma = $em->getRepository('ColegioAdminBundle:Norma')->find($entity->getNorma());
        
        $items = $em->getRepository('ColegioAdminBundle:Item')->findByNorma($norma);
        
        $valoresItems = $em->getRepository('ColegioAdminBundle:ParametrosItems')->findByParametro($entity->getId());

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioAdminBundle:Parametros:show.html.twig', array(
            'entity'            => $entity,
            'items'             => $items,
            'parametrosPrueba'  => $entity,
            'itemsValores'      => $valoresItems,  
            'delete_form'       => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Parametros entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:Parametros')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametros entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioAdminBundle:Parametros:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Parametros entity.
    *
    * @param Parametros $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Parametros $entity)
    {
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $form = $this->createForm(new ParametrosType($idColegio), $entity, array(
            'action' => $this->generateUrl('parametros_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Parametros entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:Parametros')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametros entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');
            return $this->redirect($this->generateUrl('parametros_edit', array('id' => $id)));
        }

        return $this->render('ColegioAdminBundle:Parametros:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Parametros entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioAdminBundle:Parametros')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Parametros entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');
        }

        return $this->redirect($this->generateUrl('parametros'));
    }

    /**
     * Creates a form to delete a Parametros entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('parametros_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
