<?php

namespace Colegio\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\AdminBundle\Entity\PorcentajeTrabajo;
use Colegio\AdminBundle\Form\PorcentajeTrabajoType;

/**
 * PorcentajeTrabajo controller.
 *
 */
class PorcentajeTrabajoController extends Controller
{

    /**
     * Lists all PorcentajeTrabajo entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $entities = $em->getRepository('ColegioAdminBundle:PorcentajeTrabajo')->findTipoTrabajo($idColegio);

        return $this->render('ColegioAdminBundle:PorcentajeTrabajo:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new PorcentajeTrabajo entity.
     *
     */
    public function createAction(Request $request,$periodo)
    {
    	$usuarioActivo = $this->get('security.context')->getToken()->getUser();
    	$idColegio = $usuarioActivo->getIdColegio();
        $entity = new PorcentajeTrabajo();
        $entity->setEstado(true);
        $hoy = new \DateTime('now');
        $entity->setFechaCreacion($hoy);
        $entity->setFechaModificacion($hoy);
        $form = $this->createCreateForm($entity,$periodo);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

             return $this->redirect($this->generateUrl('periodo_show', array('id' => $periodo)));
        }

        return $this->render('ColegioAdminBundle:PorcentajeTrabajo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a PorcentajeTrabajo entity.
    *
    * @param PorcentajeTrabajo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(PorcentajeTrabajo $entity,$periodo)
    {
        $em = $this->getDoctrine()->getManager();
    	$usuarioActivo = $this->get('security.context')->getToken()->getUser();
    	$idColegio = $usuarioActivo->getIdColegio();
        $periodoActual = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodo);
        $entity->setPeriodo($periodoActual);
        $form = $this->createForm(new PorcentajeTrabajoType($idColegio,$periodo), $entity, array(
            'action' => $this->generateUrl('porcentaje_create',array('periodo'=>$periodo)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new PorcentajeTrabajo entity.
     *
     */
    public function newAction($periodo)
    {
        $entity = new PorcentajeTrabajo();
        $form   = $this->createCreateForm($entity,$periodo);

        return $this->render('ColegioAdminBundle:PorcentajeTrabajo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PorcentajeTrabajo entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:PorcentajeTrabajo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PorcentajeTrabajo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioAdminBundle:PorcentajeTrabajo:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing PorcentajeTrabajo entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:PorcentajeTrabajo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PorcentajeTrabajo entity.');
        }
        $hoy = new \DateTime('now');
		$entity->setFechaModificacion($hoy);
        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioAdminBundle:PorcentajeTrabajo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a PorcentajeTrabajo entity.
    *
    * @param PorcentajeTrabajo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(PorcentajeTrabajo $entity)
    {
    	$usuarioActivo = $this->get('security.context')->getToken()->getUser();
    	$idColegio = $usuarioActivo->getIdColegio();
        $form = $this->createForm(new PorcentajeTrabajoType($idColegio), $entity, array(
            'action' => $this->generateUrl('porcentaje_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing PorcentajeTrabajo entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:PorcentajeTrabajo')->find($id);
        $hoy = new \DateTime('now');
		$entity->setFechaModificacion($hoy);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PorcentajeTrabajo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('porcentaje_edit', array('id' => $id)));
        }

        return $this->render('ColegioAdminBundle:PorcentajeTrabajo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a PorcentajeTrabajo entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioAdminBundle:PorcentajeTrabajo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PorcentajeTrabajo entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('porcentaje'));
    }

    /**
     * Creates a form to delete a PorcentajeTrabajo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('porcentaje_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
