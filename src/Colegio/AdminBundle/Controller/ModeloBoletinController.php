<?php

namespace Colegio\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\AdminBundle\Entity\ModeloBoletin;
use Colegio\AdminBundle\Form\ModeloBoletinType;

/**
 * ModeloBoletin controller.
 *
 */
class ModeloBoletinController extends Controller
{

    /**
     * Lists all ModeloBoletin entities.
     *sdas
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $entities = $em->getRepository('ColegioAdminBundle:ModeloBoletin')->findByColegio($idColegio);

        return $this->render('ColegioAdminBundle:ModeloBoletin:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new ModeloBoletin entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new ModeloBoletin();
        
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $idUsuario = $usuarioActivo->getId();
        $em = $this->getDoctrine()->getManager();
        $entity = new ModeloBoletin();
        $hoy = new \DateTime('now');
        $entity->SetFechaCreacion($hoy);
		$entity->setColegio($idColegio);
		$entity->SetFechaEdicion($hoy);
		$entity->SetCreadoPor($usuarioActivo);
		$entity->SetEditadoPor($usuarioActivo);
		$entity->SetEstado(true);		
        
        
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');  

            return $this->redirect($this->generateUrl('modeloboletin_show', array('id' => $entity->getId())));
        }

        return $this->render('ColegioAdminBundle:ModeloBoletin:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a ModeloBoletin entity.
    *
    * @param ModeloBoletin $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(ModeloBoletin $entity)
    {
        $form = $this->createForm(new ModeloBoletinType(), $entity, array(
            'action' => $this->generateUrl('modeloboletin_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new ModeloBoletin entity.
     *
     */
    public function newAction()
    {
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $idUsuario = $usuarioActivo->getId();
        $em = $this->getDoctrine()->getManager();
        $entity = new ModeloBoletin();
        $hoy = new \DateTime('now');
        $entity->SetFechaCreacion($hoy);
		$entity->setColegio($idColegio);
		$entity->SetFechaEdicion($hoy);
		$entity->SetCreadoPor($usuarioActivo);
		$entity->SetEditadoPor($usuarioActivo);
		$entity->SetEstado(true);		
        $form   = $this->createCreateForm($entity);

        return $this->render('ColegioAdminBundle:ModeloBoletin:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ModeloBoletin entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
	    $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $entity = $em->getRepository('ColegioAdminBundle:ModeloBoletin')->findOneBy(array(
        
					'id'=>$id,
					'colegio'=>$idColegio,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ModeloBoletin entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioAdminBundle:ModeloBoletin:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing ModeloBoletin entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $entity = $em->getRepository('ColegioAdminBundle:ModeloBoletin')->findOneBy(array(
        			'id'=>$id,
					'colegio'=>$idColegio,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ModeloBoletin entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioAdminBundle:ModeloBoletin:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ModeloBoletin entity.
    *
    * @param ModeloBoletin $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ModeloBoletin $entity)
    {
        $form = $this->createForm(new ModeloBoletinType(), $entity, array(
            'action' => $this->generateUrl('modeloboletin_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing ModeloBoletin entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
	    $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $hoy = new \DateTime('now');

        $entity = $em->getRepository('ColegioAdminBundle:ModeloBoletin')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ModeloBoletin entity.');
        }
		
		$entity->setFechaEdicion($hoy);
		$entity->setEditadoPor($usuarioActivo);
		
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
			$this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');  
            return $this->redirect($this->generateUrl('modeloboletin_edit', array('id' => $id)));
        }

        return $this->render('ColegioAdminBundle:ModeloBoletin:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ModeloBoletin entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioAdminBundle:ModeloBoletin')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ModeloBoletin entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');  
        }

        return $this->redirect($this->generateUrl('modeloboletin'));
    }

    /**
     * Creates a form to delete a ModeloBoletin entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('modeloboletin_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
