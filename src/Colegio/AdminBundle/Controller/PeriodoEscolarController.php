<?php

namespace Colegio\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\AdminBundle\Entity\PeriodoEscolar;
use Colegio\AdminBundle\Form\PeriodoEscolarType;

/**
 * PeriodoEscolar controller.
 *
 */
class PeriodoEscolarController extends Controller
{

    /**
     * Lists all PeriodoEscolar entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();


        $entities = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->findByColegio($idColegio);

        return $this->render('ColegioAdminBundle:PeriodoEscolar:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new PeriodoEscolar entity.
     *
     */
    public function createAction(Request $request)
    {
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();

        $entity = new PeriodoEscolar();
        $entity->setColegio($idColegio);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');

            return $this->redirect($this->generateUrl('periodoescolar_show', array('id' => $entity->getId())));
        }

        return $this->render('ColegioAdminBundle:PeriodoEscolar:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a PeriodoEscolar entity.
    *
    * @param PeriodoEscolar $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(PeriodoEscolar $entity)
    {
        $form = $this->createForm(new PeriodoEscolarType(), $entity, array(
            'action' => $this->generateUrl('periodoescolar_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new PeriodoEscolar entity.
     *
     */
    public function newAction()
    {
        $entity = new PeriodoEscolar();
        $form   = $this->createCreateForm($entity);

        return $this->render('ColegioAdminBundle:PeriodoEscolar:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PeriodoEscolar entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();

        $entity = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->findOneBy(array(
                        'id'     =>$id,
                        'colegio'=>$idColegio,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PeriodoEscolar entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioAdminBundle:PeriodoEscolar:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing PeriodoEscolar entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PeriodoEscolar entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioAdminBundle:PeriodoEscolar:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a PeriodoEscolar entity.
    *
    * @param PeriodoEscolar $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(PeriodoEscolar $entity)
    {
        $form = $this->createForm(new PeriodoEscolarType(), $entity, array(
            'action' => $this->generateUrl('periodoescolar_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing PeriodoEscolar entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PeriodoEscolar entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');

            return $this->redirect($this->generateUrl('periodoescolar_edit', array('id' => $id)));
        }

        return $this->render('ColegioAdminBundle:PeriodoEscolar:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a PeriodoEscolar entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PeriodoEscolar entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Bien hecho!');
        }

        return $this->redirect($this->generateUrl('periodoescolar'));
    }

    /**
     * Creates a form to delete a PeriodoEscolar entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('periodoescolar_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
