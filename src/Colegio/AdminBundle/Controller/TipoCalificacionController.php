<?php

namespace Colegio\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\AdminBundle\Entity\TipoCalificacion;
use Colegio\AdminBundle\Form\TipoCalificacionType;

/**
 * TipoCalificacion controller.
 *
 */
class TipoCalificacionController extends Controller
{

    /**
     * Lists all TipoCalificacion entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ColegioAdminBundle:TipoCalificacion')->findAll();

        return $this->render('ColegioAdminBundle:TipoCalificacion:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TipoCalificacion entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TipoCalificacion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipocalificacion_show', array('id' => $entity->getId())));
        }

        return $this->render('ColegioAdminBundle:TipoCalificacion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a TipoCalificacion entity.
    *
    * @param TipoCalificacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(TipoCalificacion $entity)
    {
        $form = $this->createForm(new TipoCalificacionType(), $entity, array(
            'action' => $this->generateUrl('tipocalificacion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoCalificacion entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoCalificacion();
        $form   = $this->createCreateForm($entity);

        return $this->render('ColegioAdminBundle:TipoCalificacion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TipoCalificacion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:TipoCalificacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoCalificacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioAdminBundle:TipoCalificacion:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing TipoCalificacion entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:TipoCalificacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoCalificacion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioAdminBundle:TipoCalificacion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TipoCalificacion entity.
    *
    * @param TipoCalificacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoCalificacion $entity)
    {
        $form = $this->createForm(new TipoCalificacionType(), $entity, array(
            'action' => $this->generateUrl('tipocalificacion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing TipoCalificacion entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:TipoCalificacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoCalificacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tipocalificacion_edit', array('id' => $id)));
        }

        return $this->render('ColegioAdminBundle:TipoCalificacion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TipoCalificacion entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioAdminBundle:TipoCalificacion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoCalificacion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipocalificacion'));
    }

    /**
     * Creates a form to delete a TipoCalificacion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipocalificacion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
