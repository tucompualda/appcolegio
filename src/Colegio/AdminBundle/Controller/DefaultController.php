<?php

namespace Colegio\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Colegio\AdminBundle\Entity\Colegio;
use Colegio\AdminBundle\Entity\detalleColegio;
use Colegio\AdminBundle\Entity\Direccion;
use Colegio\AdminBundle\Form\detalleColegioType;
use Colegio\AdminBundle\Form\reporteType;
use Colegio\EstudianteBundle\Form\NoticasType;


use Ps\PdfBundle\Annotation\Pdf;

ini_set ("memory_limit", "1024M");

class DefaultController extends Controller
{
    public function indexAction()
    {
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $em = $this->getDoctrine()->getManager();

        //obtenemos el parámetro actual (fecha o rango de fechas), basados en el fecha de hoy
        $hoy = new \DateTime('now');        
        $parametro = $em->getRepository('ColegioAdminBundle:Parametros')->findParametro($hoy,$idColegio);

        if(!$parametro){
                return $this->redirect($this->generateUrl('parametros'));
        }
        
        
        $normaId = $parametro->getNorma();
        
        if(!$normaId){
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Por favor selecciona el valor y ACTUALIZA la Norma en los Parametros!');
            return $this->redirect($this->generateUrl('parametros_edit', array('id' => $parametro->getId())));
        }else{
            //////////////////// norma con Escala propia para este año ///////////////////////
                $norma = $em->getRepository('ColegioAdminBundle:Norma')->find($normaId);
                 
                $items = $em->getRepository('ColegioAdminBundle:Item')->findItemValor($norma,$parametro);
        }

        $periodo = null;
        foreach($parametro as $datos){
            $periodo = $datos->getPeriodoEscolar()->getId();
        }

        //consultamos las notas de la Entidad Nota
        $notas = $em->getRepository('ColegioEstudianteBundle:Nota')->findCalificaciones($idColegio,$periodo);

         //Consultamos las notas de NotasLogros
        $notasLogros = $em->getRepository('ColegioEstudianteBundle:NotaLogro')->findNotaLogro($idColegio,$periodo);

    
        $bajoLogros = $em->getRepository('ColegioEstudianteBundle:NotaLogro')->findNotaLogroPromedio($idColegio,$periodo);
 
        $bajo = $em->getRepository('ColegioEstudianteBundle:Nota')->findPromedioEstudiante($idColegio,$periodo);

        $promedioBajo        = 0;
        $promedioBasico      = 0;
        $promedioAlto        = 0;
        $promedioSuperior    = 0;

        $promedioLogroBajo        = 0;
        $promedioLogroBasico      = 0;
        $promedioLogroAlto        = 0;
        $promedioLogroSuperior    = 0;
      

        return $this->render('ColegioAdminBundle:Default:index.html.twig',array(
                    'notas'                 => $notas,
                    'bajo'                  => $bajo,
                    'notasLogros'           => $notasLogros,
                    'bajoLogros'            => $bajoLogros,
                    'promedioBajo'          => $promedioBajo,
                    'promedioBasico'        => $promedioBasico,
                    'promedioAlto'          => $promedioAlto,
                    'promedioSuperior'      => $promedioSuperior,
                    'promedioLogroBajo'     => $promedioLogroBajo,
                    'promedioLogroBasico'   => $promedioLogroBasico,
                    'promedioLogroAlto'     => $promedioLogroAlto,
                    'promedioLogroSuperior' => $promedioLogroSuperior,
                    'items'                 => $items,
                   
            ));
    }
    
    public function configAction()
    {
        return $this->render('ColegioAdminBundle:Default:configuracion.html.twig');
    }

    public function tareasDetalleAction($id)
    {
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $em = $this->getDoctrine()->getManager();
        
        $hoy = new \DateTime('now');        
        $parametro = $em->getRepository('ColegioAdminBundle:Parametros')->findParametro($hoy,$idColegio);
        
        $estudiantes = $em->getRepository('ColegioEstudianteBundle:TrabajoMatricula')->findBy(
            array('trabajo'=>$id, 'estado'=>true),
            array('fecha'=>'ASC')
        );

        $tareasQuery = $em->createQuery('
                SELECT 
                    t,g,p,h,pt,w,j,d 
                FROM ColegioDocenteBundle:Trabajos t
                    JOIN t.grupoAsignatura g 
                    JOIN t.periodo p 
                    JOIN p.periodoEscolar pe
                    JOIN t.tipoTrabajo h 
                    JOIN h.porcentajeTrabajo pt 
                    JOIN pt.tipoTrabajo w 
                    JOIN g.idGrupo j 
                    JOIN g.idDocente d
                WHERE g.colegio = :colegio AND pe.id = :periodoescolar AND t.id = :trabajo
                ORDER BY p.id DESC
            ');
        $tareasQuery->setParameter('colegio',$idColegio);
        $tareasQuery->setParameter('periodoescolar',$parametro->getPeriodoEscolar()->getId());
        $tareasQuery->setParameter('trabajo',$id);
        $tareasQuery->setMaxResults(1);
        $tareas = $tareasQuery->getOneOrNullResult();

        return $this->render('ColegioAdminBundle:Default:tareasadminDetalle.html.twig',array(
            'entity' => $tareas,
            'estudiantes'=>$estudiantes
        ));
    }

    public function tareasAction()
    {
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $em = $this->getDoctrine()->getManager();

        $hoy = new \DateTime('now');        
        $parametro = $em->getRepository('ColegioAdminBundle:Parametros')->findParametro($hoy,$idColegio);

        $tareasQuery = $em->createQuery('
                SELECT 
                    t,g,p,h,pt,w,j,d 
                FROM ColegioDocenteBundle:Trabajos t
                    JOIN t.grupoAsignatura g 
                    JOIN t.periodo p 
                    JOIN p.periodoEscolar pe
                    JOIN t.tipoTrabajo h 
                    JOIN h.porcentajeTrabajo pt 
                    JOIN pt.tipoTrabajo w 
                    JOIN g.idGrupo j 
                    JOIN g.idDocente d
                WHERE g.colegio = :colegio AND pe.id = :periodoescolar
                ORDER BY p.id DESC
            ');
        $tareasQuery->setParameter('colegio',$idColegio);
        $tareasQuery->setParameter('periodoescolar',$parametro->getPeriodoEscolar()->getId());
        
        $tareas = $tareasQuery->getResult();
        return $this->render('ColegioAdminBundle:Default:tareasadmin.html.twig',array(
            'tareas' => $tareas
        ));
    }
    
    public function perfilAction()
    {
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $em = $this->getDoctrine()->getManager();
        
        $entities = $em->getRepository('ColegioAdminBundle:Colegio')->findOneById($idColegio);
        $sedes = $em->getRepository('ColegioAdminBundle:Sede')->findColegio($idColegio);  
        
        return $this->render('ColegioAdminBundle:Colegio:index.html.twig', array(
            'entities' => $entities,
            'sedes' => $sedes,
        ));
    }
    
    public function reporteAction(Request $request)
    {
	    $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $sede = $em->getRepository('ColegioAdminBundle:Sede')->findColegio($idColegio);
        
    	$format = 'pdf';
        $defaultData = array('message' => 'Type your message here');
        $form = $this->createForm(new reporteType($idColegio),$defaultData,array(
            'action' => $this->generateUrl('ColegioAdminBundle_reporte'),
            'method' => 'POST',    
        ));
        $form->add('submit', 'submit', array('label' => 'Generar Reporte'));
        
        $datos = $request->request->all();
        
        $periodo = "";
        foreach( $datos as $otros )
        {
        //capturo los tres datos para manejarlos independientemente y armar el Boletin
        // En realidad es el Id del estudiante
        $formato = $otros['formato'];
        $periodo = $otros['periodo'];
        $format  = "html";
        $tamano  = $otros['tamano'];
        }	

        $periodoEscolar = "";
        $periodoBuscado = $em->getRepository('ColegioBoletinBundle:Periodo')->findBy(array('id'=>$periodo));
        foreach ( $periodoBuscado as $periodoB )
        {
        $periodoEscolar = $periodoB->getPeriodoEscolar();
        }
       
        
        $matriculas = $em->createQuery('SELECT m,e,p FROM ColegioEstudianteBundle:GrupoEstudiante m '
                .' JOIN m.idEstudiante e JOIN m.periodoEscolar p '
                .' WHERE m.periodoEscolar = :periodoEscolar AND m.colegio = :colegio');
        $matriculas->setParameter('periodoEscolar',$periodoEscolar);
        $matriculas->setParameter('colegio',$idColegio);
        $resultado= $matriculas->getResult();

        if ( $request->isMethod('POST') ) 
        {
        $form->bind($request);
        $data = $form->getData();
        }
        
        return $this->render('ColegioAdminBundle:Default:reporte.html.twig',array(
        'form'     => $form->createView(),
        'datos'    => $datos,
        'matriculas' => $resultado,
        'formato'  => $format,                      
        ));
    }

     
    public function generarAction( Request $request )
    {
        $format = $this->get('request')->get('_format');
        $matricula = $this->get('request')->get('matricula');
        $formato = $this->get('request')->get('formato');
        $periodo = $this->get('request')->get('periodo');

        //$periodoescolar = $this->get('request')->get('periodoescolar');
        $texto = $this->get('request')->get('tamano');
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $em = $this->getDoctrine()->getManager();

        $colegiodatos = $em->createQuery('SELECT c,d FROM ColegioAdminBundle:Colegio c JOIN c.detalle d WHERE c.id = :idColegio' );
        $colegiodatos->SetParameter('idColegio',$idColegio);
        $colegiodatos->SetMaxResults(1);

        $colegio = $colegiodatos->getResult();
        
        foreach( $colegio as $cole ){
            $logo = $cole->getDetalle()->getWebPath();
        }
        
        $periodoSolicitado = $em->getRepository( 'ColegioBoletinBundle:Periodo' )->find( $periodo ); 
        $perEscolarId = $periodoSolicitado->getPeriodoEscolar();
        $perEscolar = $em->getRepository( 'ColegioAdminBundle:PeriodoEscolar' )->find( $perEscolarId );

        $fechaInicial = $perEscolar->getFechaInicio();
        $fechaFinal = $perEscolar->getFechaFin(); 

        $estudiante = $em->getRepository('ColegioEstudianteBundle:Estudiante')->findOneBy(array(
            'id'        => $matricula,
            'idColegio' => $idColegio,
        ));		
        $formatico  = $em->getRepository( 'ColegioAdminBundle:ModeloBoletin' )->findOneById($formato);

        $fechaInicialPeriodo = $fechaInicial;
        $fechaFinalPeriodo = $fechaFinal;

        $query = $em->createQuery('
            SELECT g 
            FROM ColegioEstudianteBundle:GrupoEstudiante g 
            WHERE g.idEstudiante = :estudiantes AND g.fechaMatricula BETWEEN ?1 AND ?2');
        $query->SetParameter( 1,$fechaInicialPeriodo );
        $query->SetParameter( 2,$fechaFinalPeriodo );                 
        $query->SetParameter( 'estudiantes', $matricula );
        $query->SetMaxResults( 1 );
        $matriactual = $query->getOneOrNullResult();

        $matrifinal = $matriactual->getId();

        //////////// CONSULTAMOS LAS CALIFICACIONES ///////////////////

        $boletinquery = $em->createQuery( 'SELECT n,l,b FROM ColegioEstudianteBundle:NotaLogro n JOIN n.logro l JOIN l.idBoletin b JOIN b.asignatura a WHERE n.matricula = :matricula AND b.periodo = :periodo ORDER BY a.nombre ASC');
        $boletinquery->SetParameter( 'matricula',$matrifinal );
        $boletinquery->SetParameter( 'periodo',$periodo );
        $boletin = $boletinquery->getResult();

        /////////// fin calificaciones ////////////////////////////////                

        $materiaquery = $em->createQuery('SELECT a.nombre as mmateria,n,l,b,a,AVG(n.calificacion) AS total,m  
            FROM ColegioEstudianteBundle:NotaLogro n 
            JOIN n.logro l JOIN l.idBoletin b JOIN b.asignatura a JOIN n.matricula m 
            WHERE n.matricula = :matricula AND b.periodo = :periodo 
            GROUP BY b.asignatura ORDER BY a.nombre ASC');
        $materiaquery->SetParameter('matricula',$matrifinal);
        $materiaquery->SetParameter('periodo',$periodo);
        $materia = $materiaquery->getResult();
        $observaciones = $em->getRepository('ColegioDocenteBundle:Observacion')->findObservacionBoletin($periodo,$matrifinal);


        $inscripcionesAsignaturasQuery = $em->createQuery('
            SELECT i,m,e,ga,a FROM ColegioBoletinBundle:InscripcionAsignatura i
            INNER JOIN i.matricula m
            INNER JOIN m.idEstudiante e
            INNER JOIN i.grupoAsignatura ga
            INNER JOIN ga.idAsignatura a
            WHERE e.id = :estudiante AND m.id = :matricula AND ga.isActive = true
            ORDER BY a.nombre ASC
        ');
        $inscripcionesAsignaturasQuery->setParameter('estudiante',$estudiante->getId());
        $inscripcionesAsignaturasQuery->setParameter('matricula',$matrifinal);
        $inscripcionesAsignatura = $inscripcionesAsignaturasQuery->getResult();

        $periodoEscolar = $perEscolarId;

        $hoy = new \DateTime('now');
        $parametros = $em->getRepository('ColegioAdminBundle:Parametros')->findParametro($hoy,$colegio);

         //////////// escala del colegio //////////////////
        $normaId = $parametros->getNorma();
        
        if(!$normaId)
        {
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Hay un error, pide al administrador que habilite la norma para el año lectivo!');
            return $this->redirect($this->generateUrl('colegio_estudiante_homepage'));
        }
        else
        {
            $norma = $em->getRepository('ColegioAdminBundle:Norma')->find($normaId);
            $items = $em->getRepository('ColegioAdminBundle:Item')->findItemValor($norma,$parametros);
        }

    $niveles = $em->getRepository('ColegioGrupoBundle:Nivel')->findAll();
    $respuesta = "";    
    if($format == "html"){
         $respuesta = $this->render(sprintf('ColegioAdminBundle:Default:bolet.html.twig', $format),array(
                'estudiante'  => $estudiante,
                'matricula'   => $matricula,
                'formato'     => $formatico,
                'principal'   => $matriactual,
                'periodo'     => $periodoSolicitado,
                'colegio'     => $colegio,
                'logo'        => $logo,
                'boletin'     => $boletin,
                'materias'    => $materia,
                'observaciones' => $observaciones,
                'tamano'      => $texto,
                'inscripcionesAsignatura'=>$inscripcionesAsignatura,
                'items'       => $items,
                'niveles'     => $niveles
        ));     
    }
    elseif($format == "pdf")
    {
          $html =  $this->renderView('ColegioAdminBundle:Default:bolet.html.twig',array(
        
            'estudiante'  => $estudiante,
            'matricula'   => $matricula,
            'formato'     => $formatico,
            'principal'   => $matriactual,
            'periodo'     => $periodoSolicitado,
            'colegio'     => $colegio,
            'logo'        => $logo,
            'boletin'     => $boletin,
            'materias'    => $materia,
            'observaciones' => $observaciones,
            'tamano'      => $texto,
            'inscripcionesAsignatura'=>$inscripcionesAsignatura,
            'items'       => $items,
            'niveles'     => $niveles            
        ));
        
        $respuesta = new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html,array(
                        'lowquality' => true,
                        'encoding' => 'utf-8',
                        'page-size' => 'Letter',
                        'images' => true,
                        'image-dpi' => 100,
                        'margin-top' => 15
                )),
                200,
                array(
                    'Content-Type'          => 'application/pdf',
                    'Content-Disposition'   => 'attachment; filename="'.$estudiante->getNumeroDocumento().'-'.$estudiante.'-'.$periodoSolicitado.'.pdf"',
                    
                )
            );
    }
    else
    {
       
    }
    return $respuesta;
       
    }

    public function generarPdf()
    {
        $pageUrl = $this->generateUrl('ColegioAdminBundle_generar', array(
            
        ), true); // use absolute path!

        return new Response(
            $this->get('knp_snappy.pdf')->getOutput($pageUrl),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="file.pdf"'
            )
        );
    }

    public function listadosAction($formato,$grupo,$usuario,$documento,$sede,$encabezado,$salto)
    {

        $usuarioActivo = $this->get( 'security.context' )->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();
        $em = $this->getDoctrine()->getManager();
        $sede = $em->getRepository('ColegioAdminBundle:Sede')->find($sede);
        $hoy = new \DateTime("now");
        $parametro = $em->getRepository('ColegioAdminBundle:Parametros')->findParametro($hoy,$idColegio);

        if($grupo == "*"){
            $gruposQuery = $em->createQuery('SELECT g,d,s,n FROM ColegioGrupoBundle:Grupo g 
                JOIN g.idDocenteResponsable d
                JOIN g.idSede s JOIN g.idNivel n
                WHERE g.colegio = :colegio AND s.id = :id AND g.fechaInicio <= :hoy AND g.fechaFin >= :hoy ORDER BY n.orden ASC
            ');
            $gruposQuery->setParameter('colegio', $idColegio);
            $gruposQuery->setParameter('id', $sede);
            $gruposQuery->setParameter('hoy',$hoy);

            $grupos = $gruposQuery->getResult();
            $estudiantesQuery = $em->createQuery('SELECT g,e FROM ColegioEstudianteBundle:GrupoEstudiante g 
                JOIN g.idEstudiante e
                WHERE g.colegio = :idColegio ORDER BY e.primerApellido ASC
            ');
            $estudiantesQuery->setParameter('idColegio',$idColegio);
            $estudiantes = $estudiantesQuery->getResult();

            $nombreArchivo = "todos-".$sede;
        
        }else{
            $grupos = $em->getRepository('ColegioGrupoBundle:Grupo')->findBy(array(
                'id'=>$grupo
            ));

            foreach ($grupos as $key) {
                $grupoNombre = $key->getNombre();
            }
            $estudiantesQuery = $em->createQuery('SELECT g,e FROM ColegioEstudianteBundle:GrupoEstudiante g 
                JOIN g.idEstudiante e
                WHERE g.idGrupo = :grupos ORDER BY e.primerApellido ASC
            ');
            $estudiantesQuery->setParameter('grupos',$grupos);
            $estudiantes = $estudiantesQuery->getResult();
            $nombreArchivo = $grupoNombre."-".$sede;

          
        }

        $colegiodatos = $em->createQuery( 'SELECT c,d FROM ColegioAdminBundle:Colegio c JOIN c.detalle d WHERE c.id = :idColegio' );
        $colegiodatos->SetParameter( 'idColegio',$idColegio );
        $colegiodatos->SetMaxResults( 1 );

        $colegio = $colegiodatos->getResult();
        
        foreach( $colegio as $cole ){
            $logo = $cole->getDetalle()->getWebPath();
            $nombreColegio = $cole->getNombre();
        }






            $respuesta = "";    
            if($formato == "html")
            {
                 $respuesta = $this->render(sprintf('ColegioGrupoBundle:Grupo:listado.html.twig'),array(

                    'logo'=>$logo,
                    'grupos'=>$grupos,
                    'estudiantes'=>$estudiantes,
                    'usuario'=>$usuario,
                    'documento'=>$documento,
                    'encabezado'=>$encabezado,
                    'salto'=>$salto,
                    'nombreColegio'=>$nombreColegio,
                    'sede'=>$sede,
                    'parametro'=>$parametro

                ));     
            }
            elseif($formato == "pdf")
            {
                  $html =  $this->renderView('ColegioGrupoBundle:Grupo:listado.html.twig',array(
                
                  'logo'=>$logo,
                  'grupos'=>$grupos,
                  'estudiantes'=>$estudiantes,
                  'usuario'=>$usuario,
                  'documento'=>$documento,
                  'encabezado'=>$encabezado,
                  'salto'=>$salto,
                  'nombreColegio'=>$nombreColegio,
                  'sede'=>$sede,
                  'parametro'=>$parametro

            ));
                
                $respuesta = new Response(
                        $this->get('knp_snappy.pdf')->getOutputFromHtml($html,array(
                                'lowquality' => true,
                                'encoding' => 'utf-8',
                                'page-size' => 'Letter',
                                'images' => true,
                                'image-dpi' => 100,
                                'margin-top' => 15
                        )),
                        200,
                        array(
                            'Content-Type'          => 'application/pdf',
                            'Content-Disposition'   => 'attachment; filename="Listado_'.$nombreArchivo.'.pdf"',
                            
                        )
                    );
            }
           
    return $respuesta;
    }
    
    public function cargaAction()
    {
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $colegio = $usuarioActivo->getIdColegio();
        $em = $this->getDoctrine()->getManager();

        $sedes = $em->getRepository('ColegioAdminBundle:Sede')->findColegio($colegio);
        $periodos = $em->getRepository('ColegioBoletinBundle:Periodo')->findBy(
            array('colegio'=>$colegio),
            array('id'=>'DESC'));

        $periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($colegio);
        
         if($periodo==null){
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Crea un Periodo');
            return $this->redirect($this->generateUrl('periodo'));
        }
        $idperiodo = $periodo->getId();
        $idperiodoEscolar = $periodo->getPeriodoEscolar()->getId();

        $periodosEscolares = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->findByColegio($colegio);

        return $this->render('ColegioAdminBundle:Default:carga.html.twig',array(
                'sedes'=>$sedes,
                'periodosTotal'=>$periodos,
                'idPeriodoActual'=>$idperiodo,
                'periodosEscolares'=>$periodosEscolares,
                'idPeriodoEscolar'=>$idperiodoEscolar
            ));
    }

    public function boletinBaseAction($_format,$matricula,$formato,$periodo,$tamano)
    {
        ////////////////////////////////////////////
        $formatoImpresion = $_format;
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $colegio = $usuarioActivo->getIdColegio();
        $em = $this->getDoctrine()->getManager();

        $sedes = $em->getRepository('ColegioAdminBundle:Sede')->findColegio($colegio);
    
        $formatico = $em->getRepository( 'ColegioAdminBundle:ModeloBoletin' )->findOneById($formato);
       //$periodo = $em->getRepository('ColegioBoletinBundle:Periodo')->findPeriodos($colegio);
      
        $colegiodatos = $em->createQuery( 'SELECT c,d FROM ColegioAdminBundle:Colegio c JOIN c.detalle d WHERE c.id = :idColegio' );
        $colegiodatos->SetParameter( 'idColegio',$colegio );
        $colegiodatos->SetMaxResults( 1 );

        $colegio = $colegiodatos->getResult();
        
        foreach( $colegio as $cole )
        {
        $logo = $cole->getDetalle()->getWebPath();
        }


         if($periodo==null){
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Crea un Periodo');
            return $this->redirect($this->generateUrl('periodo'));
        }
        //$idperiodo = $periodo->getId();
        //$idperiodoEscolar = $periodo->getPeriodoEscolar()->getId();
        
        
        $periodoRecibido = $em->getRepository('ColegioBoletinBundle:Periodo')->find($periodo);
        /// Fecha periodoInicialActual
        $fechaInicial = $periodoRecibido->getFechaInicio();
        $periodosQuery = $em->createQuery(''
                . 'SELECT p FROM ColegioBoletinBundle:Periodo p WHERE '
                . 'p.colegio = :colegio AND p.periodoEscolar = :periodoEscolar AND p.fechaInicio <= :fechaInicial '
                . 'ORDER BY p.id DESC');
        
        $periodosQuery->setParameter('colegio',$colegio);       
        $periodosQuery->setParameter('periodoEscolar',$periodoRecibido->getPeriodoEscolar());
        $periodosQuery->setParameter('fechaInicial',$fechaInicial);
        
        $periodos = $periodosQuery->getResult();
       
        $notasAcumuladasQuery = $em->createQuery(''
                . 'SELECT n,gb,ga,pe,p,AVG(n.calificacion) AS promedio, gb.id as jairo, ga.id as nata '
                . 'FROM ColegioEstudianteBundle:Nota n '
                . 'JOIN n.grupoBoletin gb JOIN gb.periodo p JOIN p.periodoEscolar pe '
                . 'JOIN gb.grupoAsignatura ga '
                . 'WHERE n.matricula = :matricula AND pe.id = :periodoEscolar AND p.fechaInicio <= :fechaInicial AND ga.isActive = true '
                . 'GROUP BY gb.grupoAsignatura');
        $notasAcumuladasQuery->setParameter('matricula',$matricula);
        $notasAcumuladasQuery->setParameter('fechaInicial',$fechaInicial);
        $notasAcumuladasQuery->setParameter('periodoEscolar',$periodoRecibido->getPeriodoEscolar());
        
        $notasAcumuladas = $notasAcumuladasQuery->getResult();

        $notasAcumuladasTotalQuery = $em->createQuery(''
                . 'SELECT n,gb,ga,pe,p,AVG(n.calificacion) AS promedio, gb.id as jairo, ga.id as nata '
                . 'FROM ColegioEstudianteBundle:Nota n '
                . 'JOIN n.grupoBoletin gb JOIN gb.periodo p JOIN p.periodoEscolar pe '
                . 'JOIN gb.grupoAsignatura ga '
                . 'WHERE n.matricula = :matricula AND pe.id = :periodoEscolar AND p.fechaInicio <= :fechaInicial AND ga.isActive = true '
                . '');
        $notasAcumuladasTotalQuery->setParameter('matricula',$matricula);
        $notasAcumuladasTotalQuery->setParameter('fechaInicial',$fechaInicial);
        $notasAcumuladasTotalQuery->setParameter('periodoEscolar',$periodoRecibido->getPeriodoEscolar());
        
        $notasAcumuladasTotal = $notasAcumuladasTotalQuery->getResult();



        $periodosEscolares = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->findByColegio($colegio);

        //////////////////////////////////////////

        $matriculaActual = $em->getRepository('ColegioEstudianteBundle:GrupoEstudiante')->find($matricula);
        
        $estudiante = $matriculaActual->getIdEstudiante();

        $grupo = $matriculaActual->getIdGrupo();

        $gruposMatricula = $em->createQuery('
                SELECT ga,g,ge,a,d,gb,b,l,n,nl,p,pe,pa,pi,i 
                FROM ColegioGrupoBundle:GrupoAsignatura ga 
                JOIN ga.idGrupo g 
                LEFT JOIN g.grupoEstudiantes ge 
                JOIN ga.idAsignatura a 
                JOIN ga.idDocente d
                LEFT JOIN ga.gruposBoletines gb 
                JOIN gb.boletin b 
                JOIN gb.periodo p 
                JOIN p.periodoEscolar pe 
                LEFT JOIN pe.parametros pa
                LEFT JOIN pa.parametrositems pi 
                LEFT JOIN pi.item i
                LEFT JOIN b.logros l
                LEFT JOIN gb.notas n LEFT 
                JOIN l.notalogro nl 
                WHERE ge.id = :matriculaActual AND gb.periodo = :periodoActual AND ga.isActive = true 
                AND n.matricula = :matriculaActual 
                ');
        $gruposMatricula->setParameter('matriculaActual',$matriculaActual);
        $gruposMatricula->setParameter('periodoActual',$periodoRecibido);
        $gruposFull = $gruposMatricula->getResult();

        $observacion = $em->getRepository('ColegioDocenteBundle:Observacion')->findOneBy(array(
            'estudiante'=>$matriculaActual,
            'periodo'=>$periodoRecibido
        ));

        ////////// ímportante
        $notasPromedio = $em->createQuery('SELECT n.id,SUM(n.calificacion),
                COUNT(n.id),AVG(n.calificacion) AS promedioEstudiante,gb.id,p.id,m.id as matri,e.id 
                FROM ColegioEstudianteBundle:Nota n
                JOIN n.grupoBoletin gb 
                JOIN gb.periodo p 
                JOIN gb.grupoAsignatura ga
                JOIN n.matricula m JOIN m.idEstudiante e
                JOIN m.idGrupo g
                WHERE gb.periodo = :periodo AND g.id = :grupo AND ga.isActive = true GROUP BY n.matricula
                ORDER BY promedioEstudiante DESC
            ');
        $notasPromedio->setParameter('periodo',$periodoRecibido);
        $notasPromedio->setParameter('grupo',$grupo);

        $notasPromedioFull = $notasPromedio->getResult();
        if(!$observacion==null){
             $notaComportamiento = $observacion->getCalificacion();
        }

        $notastotalPromedio = $em->createQuery('SELECT count(ge.id) AS total 
            FROM ColegioGrupoBundle:Grupo g
            JOIN g.grupoEstudiantes ge
            WHERE ge.idGrupo = :grupo
            GROUP BY ge.idGrupo
        ');
        $notastotalPromedio->setParameter('grupo',$grupo);

        $TotalEstudiantesFull = $notastotalPromedio->getResult();

        $contador = 0;
        $posicion = "";
        $contador2 = 0;
        foreach ( $TotalEstudiantesFull as $key) {
            
           $contador2 = $key['total'];
            
        }



        foreach ( $notasPromedioFull as $key) {
            $contador = $contador +1;
            if($key['matri']==$matricula)
            {
                $posicion = $contador;
            }
           
        }

            $nombreArchivo = "Archivo";       


            $respuesta = "";    
            if($formatoImpresion == "html")
            {
                 $respuesta = $this->render('ColegioAdminBundle:Default:boletinbase.html.twig',array(
                    'grupos'=>$gruposFull,
                    'observacion'=>$observacion,
                    'estudiante'=>$estudiante,
                    'periodo'=>$periodoRecibido,
                    'logo'=>$logo,
                    'formato'=>$formatico,
                    'principal'   => $matriculaActual,
                    'tamano' => $tamano,
                    'posicion'=> $posicion,
                    'contadores'=> $contador2,
                    'notasAcumuladas'=>$notasAcumuladas,
                    'notaAcumuladaFinal'=>$notasAcumuladasTotal
                 ));

            }
            elseif($formatoImpresion == "pdf")
            {
                  $html = $this->renderView('ColegioAdminBundle:Default:boletinbase.html.twig',array(
                        'grupos'=>$gruposFull,
                        'observacion'=>$observacion,
                        'estudiante'=>$estudiante,
                        'periodo'=>$periodoRecibido,
                        'logo'=>$logo,
                        'formato'=>$formatico,
                        'principal'   => $matriculaActual,
                        'tamano' => $tamano,
                        'posicion'=> $posicion,
                        'contadores'=> $contador2
                    ));

                
                $respuesta = new Response(
                        $this->get('knp_snappy.pdf')->getOutputFromHtml($html,array(
                                'lowquality' => true,
                                'encoding' => 'utf-8',
                                'page-size' => 'Letter',
                                'images' => true,
                                'image-dpi' => 100,
                                'margin-top' => 15
                        )),
                        200,
                        array(
                            'Content-Type'          => 'application/pdf',
                            'Content-Disposition'   => 'attachment; filename="Listado_'.$nombreArchivo.'.pdf"',
                            
                        )
                    );
            }
           
        return $respuesta;

    }

    public function planillasAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idDocente = $usuarioActivo->getId();
        $colegio = $usuarioActivo->getIdColegio();
        $hoy = new \DateTime("now");
        $gruposAsignaturaQuery = $em->createQuery('
                SELECT ga,g,a,d FROM ColegioGrupoBundle:GrupoAsignatura ga
                JOIN ga.idGrupo g
                JOIN ga.idAsignatura a
                JOIN ga.idDocente d
                JOIN g.idNivel n
                WHERE ga.colegio = :colegio AND g.fechaInicio <= :hoy 
                AND g.fechaFin >= :hoy AND ga.isActive = true ORDER BY n.orden ASC
        ');
        $gruposAsignaturaQuery->setParameter('colegio',$colegio);
        $gruposAsignaturaQuery->setParameter('hoy',$hoy);
        $gruposAsignatura = $gruposAsignaturaQuery->getResult();
        $periodoAnual = $em->getRepository('ColegioAdminBundle:PeriodoEscolar')->getPeriodo($colegio)->getId();

        $periodos = $em->getRepository('ColegioBoletinBundle:Periodo')->findBy(array(
            'periodoEscolar'=>$periodoAnual,
            'colegio'=>$colegio
        ));

        return $this->render('ColegioAdminBundle:Default:planillas.html.twig',array(
            'periodosTotal'=>$periodos,
            'gruposAsignatura'=>$gruposAsignatura
        ));
    }
}
