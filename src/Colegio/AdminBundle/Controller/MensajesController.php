<?php

namespace Colegio\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Colegio\AdminBundle\Entity\Mensajes;
use Colegio\AdminBundle\Form\MensajesType;

/**
 * Mensajes controller.
 *
 */
class MensajesController extends Controller
{

    /**
     * Lists all Mensajes entities.
     *
     */
    public function indexAction()
    {
      
        $em = $this->getDoctrine()->getManager();
        $usuarioActivo = $this->get('security.context')->getToken()->getUser();
        $idColegio = $usuarioActivo->getIdColegio();

        $entity = $em->createQuery('SELECT m,e,d FROM ColegioAdminBundle:Mensajes m 
            LEFT JOIN m.estudiante e LEFT JOIN m.docente d 
            WHERE e.idColegio = :colegio OR d.idColegio = :colegio ');
        $entity->setParameter('colegio',$idColegio);
        $entities = $entity->getResult();
       
        return $this->render('ColegioAdminBundle:Mensajes:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Mensajes entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Mensajes();
        $hoy = new \DateTime("now");
        $entity->setFechaCreacion($hoy);
        $entity->setFechaModificacion($hoy);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('mensajes_show', array('id' => $entity->getId())));
        }

        return $this->render('ColegioAdminBundle:Mensajes:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Mensajes entity.
    *
    * @param Mensajes $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Mensajes $entity)
    {
        $hoy = new \DateTime("now");
        $entity->setFechaCreacion($hoy);
        $entity->setFechaModificacion($hoy);
        $form = $this->createForm(new MensajesType(), $entity, array(
            'action' => $this->generateUrl('mensajes_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Mensajes entity.
     *
     */
    public function newAction()
    {
        $entity = new Mensajes();
        $form   = $this->createCreateForm($entity);

        return $this->render('ColegioAdminBundle:Mensajes:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Mensajes entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:Mensajes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mensajes entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioAdminBundle:Mensajes:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Mensajes entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:Mensajes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mensajes entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ColegioAdminBundle:Mensajes:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Mensajes entity.
    *
    * @param Mensajes $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Mensajes $entity)
    {
        $form = $this->createForm(new MensajesType(), $entity, array(
            'action' => $this->generateUrl('mensajes_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Mensajes entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ColegioAdminBundle:Mensajes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mensajes entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('mensajes_edit', array('id' => $id)));
        }

        return $this->render('ColegioAdminBundle:Mensajes:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Mensajes entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ColegioAdminBundle:Mensajes')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Mensajes entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('mensajes'));
    }

    /**
     * Creates a form to delete a Mensajes entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('mensajes_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
