<?php

namespace Colegio\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ItemType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('norma')
            ->add('nombre')
            ->add('descripcion')
            ->add('icon','choice',array(
                'choices'=>array(
                    'icon-star'=>'icon-star',
                    'icon-star-empty'=>'icon-star-empty',                    
                    'icon-exclamation-sign'=>'icon-exclamation-sign',
                    'icon-warning-sign'=>'icon-warning-sign',                
                )
            ))
            ->add('span','choice',array(
                'choices'=>array(
                    'label label-success value'=>'label label-success value',
                    'label label-info'=>'label label-info',                    
                    'label label-warning'=>'label label-warning',
                    'label label-important'=>'label label-important',                
                )
            ))
            ->add('iconBoostrap3','choice',array(
                'choices'=>array(
                    'fa fa-starr'=>'fa fa-star',
                    'fa fa-star-half-o'=>'fa fa-star-half-o',                    
                    'fa fa-star-o'=>'fa fa-star-o',
                    'fa fa-warning'=>'fa fa-warning',                
                )
            ))
            ->add('spanBoostrap3','choice',array(
                'choices'=>array(
                    'label label-success value'=>'label label-success value',
                    'label label-primary'=>'label label-primary',                    
                    'label label-warning'=>'label label-warning',
                    'label label-danger'=>'label label-danger',                
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\AdminBundle\Entity\Item'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_adminbundle_item';
    }
}
