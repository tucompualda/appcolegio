<?php

namespace Colegio\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class CircularesType extends AbstractType
{
    public function __construct($colegio) 
    {

        $this->colegio = $colegio;
        $this->hoy = new \DateTime('now');  
    }

        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $self = $this;
        $builder
            //->add('todos')
            ->add('publicado')
            ->add('destinatarios','entity',array(
                'class'=>'ColegioGrupoBundle:Grupo',
                'label'=>'Destinatarios',
                'empty_value'=>'Selecciona',
                'query_builder'=> function(EntityRepository $er) use($self){
                       return $er->createQueryBuilder('u')
                               ->where('u.colegio = :colegio  AND u.fechaInicio <= :hoy AND u.fechaFin >= :hoy')
                               ->setParameter('colegio', $self->colegio)
                               ->setParameter('hoy',$self->hoy);
                },
                'multiple'=>true,
                'required'=>true
            ))
            ->add('titulo')
            ->add('detalle','textarea', array(
                    'required'=>true,
                    'attr' => array(
                        'class' => 'tinymce',
                        'data-theme' => 'advanced', 
                            ),
                    
                )
            )           
            //->add('fecha')
            //->add('fechaVisita')
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\AdminBundle\Entity\Circulares'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_adminbundle_circulares';
    }
}
