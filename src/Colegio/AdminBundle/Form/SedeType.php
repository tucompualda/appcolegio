<?php

namespace Colegio\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class SedeType extends AbstractType
{
    public function __construct($idColegio) 
    {
        $this->idColegio = $idColegio;
    }
    
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $self = $this;
        $builder
            ->add('nombreSede')
            ->add('idDetalleColegio','entity',array(
                'class'=>'ColegioAdminBundle:detalleColegio',
                'query_builder'=>function(EntityRepository $er) use($self){
                    return $er->CreateQueryBuilder('u')
                            ->where('u.idColegio = :idColegio')
                            ->setParameter('idColegio',$self->idColegio);
                },
                 'label'      => 'Mi colegio',
                 'required'   =>true))   
            ->add('estado')
            ->add('rector',new RectorType());
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\AdminBundle\Entity\Sede'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_adminbundle_sede';
    }
}
