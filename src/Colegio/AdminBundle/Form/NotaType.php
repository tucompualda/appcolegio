<?php

namespace Colegio\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;


class NotaType extends AbstractType
{
	public function __construct($idColegio)
	{
		$this->idColegio = $idColegio;
	}
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$self = $this;
        $builder
			->add('matricula','entity',array(
                'class' => 'ColegioEstudianteBundle:GrupoEstudiante',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('u')
                                ->select('u','e')
                                ->leftJoin('u.idEstudiante','e')
                                ->where('u.colegio = :idColegio')
                                ->setParameter('idColegio', $self->idColegio);
                },
                 'label'=>'Grupo Boletin',
                 'required'=> true,
            ))
			->add('grupoBoletin','entity',array(
                'class' => 'ColegioBoletinBundle:GrupoBoletin',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('u')
                                ->select('u','g','p')
                                ->leftJoin('u.grupoAsignatura','g')
                                ->leftJoin('u.periodo','p')
                                ->where('u.colegio = :idColegio')
                                ->setParameter('idColegio', $self->idColegio);
                },
                 'label'=>'Grupo Boletin',
                 'required'=> true,
            ))
            ->add('calificacion','number',array(
				
				'attr'=>array('min'=>0,'max'=>5,'step'=>0.1,'class'=>'form-control'),
				
            ))
            ->add('descripcion','textarea',array(
					'attr'=>array('class'=>'form-control','rows'=>'3'),
					'required'=>true,
					
			))
            ->add('fecha')
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\EstudianteBundle\Entity\Nota'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegioadmin_estudiantebundle_nota';
    }
}
