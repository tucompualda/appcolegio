<?php

namespace Colegio\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class CargaType extends AbstractType
{
    public function __construct($colegio)
    {
        $this->colegio = $colegio;
    } 
     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $self = $this; 
        $builder
             ->add('sede','entity',array(
                'class' => 'ColegioAdminBundle:Sede',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('u')
                                ->select('u','c')
                                ->leftJoin('u.idDetalleColegio','c')
                                ->where('c.idColegio = :sede')
                                ->setParameter('sede', $self->colegio);
                },
                 'label'=>'Sede',
                 
                 'required'=> true,
            ))
            ->add('parametros','entity',array(
                'class' => 'ColegioAdminBundle:Parametros',
                'query_builder' => function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('u')
                                ->where('u.colegio = :sede')
                                ->setParameter('sede', $self->colegio);
                },
                 'label'=>'Año',
                 
                 'required'=> true,
            ))
            
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_estudiantebundle_noticas';
    }
}