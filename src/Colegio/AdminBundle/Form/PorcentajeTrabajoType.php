<?php

namespace Colegio\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class PorcentajeTrabajoType extends AbstractType
{
	public function __construct($idColegio,$periodo)
	{
		$this->idColegio = $idColegio;
                $this->periodo = $periodo;
	}
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$self = $this;
        $builder
           
            ->add('porcentaje','percent',array(
        		'type'=>'integer',
        ))
            
            ->add('tipoTrabajo','entity',array(
                'class'=>'ColegioAdminBundle:TipoTrabajo',
                'query_builder'=>function(EntityRepository $er) use($self){
                    return $er->CreateQueryBuilder('u')
                            ->where('u.colegio = :idColegio')
                            ->setParameter('idColegio',$self->idColegio);
                },
                 'label'      => 'Tipo de Trabajo',
                 'empty_value'=> 'Selecciona',
                 'required'   =>true
            ))
            ->add('periodo','entity',array(
                'class'=>'ColegioBoletinBundle:Periodo',
                'query_builder'=>function(EntityRepository $er) use($self){
                    return $er->CreateQueryBuilder('u')
                            ->where('u.colegio = :idColegio AND u.id = :periodo')
                            
                            ->setParameter('idColegio',$self->idColegio)
                            ->setParameter('periodo',$self->periodo);
                },
                 'label'      => 'Período',
                 
                 'required'   =>true,
                 'attr'=>array('type'=>'hidden')       
            ))      
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\AdminBundle\Entity\PorcentajeTrabajo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_adminbundle_porcentajetrabajo';
    }
}
