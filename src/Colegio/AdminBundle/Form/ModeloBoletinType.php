<?php

namespace Colegio\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ModeloBoletinType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('logros',null,array(
                'required'=>false,
                'label'=>'Habilitar Logros en Boletín'
            ))
            ->add('observaciones',null,array(
                'required'=>false,
                'label'=>'Habilitar Observaciones en Boletín'
            ))
            //->add('fechaCreacion')
            //->add('creadoPor')
            ->add('estado',null,array(
                'required'=>false,
                'label'=>'Boletín Activo'
            ))

            ->add('categoria',null,array(
                'required'=>false,
                'label'=>'Mostrar categoría'
            ))
            ->add('posicion',null,array(
                'required'=>false,
                'label'=>'Mostrar posición del estudiante'
            ))
            ->add('textoExtraSegundoMostrar',null,array(
                'required'=>false,
                'label'=>'Mostrar texto bloque extra'
            ))                
            ->add('bloqueFirmasMostrar',null,array(
                'required'=>false,
                'label'=>'Mostrar firmas'
            ))
            ->add('footerMostrar',null,array(
                'required'=>false,
                'label'=>'Mostrar footer'
            ))
            ->add('encabezado','textarea', array(
                'required'=>'false',
                'attr' => array(
                        'class' => 'tinymce',
                        'data-theme' => 'advanced',
                        'rows'=>10 
                                ),
                        )
            	)          
            ->add('textoExtra','textarea', array(
                'required'=>'false',
                'attr' => array(
                        'class' => 'tinymce',
                        'data-theme' => 'advanced',
                        'rows'=>10 
                                ),
                        )
            	)    

            ->add('titulo','textarea', array(
                'required'=>'false',
                'attr' => array(
                    'class' => 'tinymce',
                    'data-theme' => 'advanced',
                    'rows'=>10 
                        ),

                )
             ) 
            ->add('textoExtraSegundo','textarea', array(
                    'required'=>'false',
                    'attr' => array(
                        'class' => 'tinymce',
                        'data-theme' => 'advanced',
                        'rows'=>10 
                            ),
                    
                    )
                )
            ->add('bloqueFirmas','textarea', array(
                    'required'=>'false',
                    'attr' => array(
                        'class' => 'tinymce',
                        'data-theme' => 'advanced',
                        'rows'=>10 
                            ),
                    
                    )
                ) 
            ->add('footer','textarea', array(
                    'required'=>'false',
                    'attr' => array(
                        'class' => 'tinymce',
                        'data-theme' => 'advanced',
                        'rows'=>10 
                            ),
                    
                    )
                ) 
            //->add('fechaEdicion')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\AdminBundle\Entity\ModeloBoletin'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_adminbundle_modeloboletin';
    }
}
