<?php

namespace Colegio\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;


class reporteType extends AbstractType
{
    public function __construct($idColegio) 
    {
        $this->idColegio = $idColegio;
    }
    
        
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	    $self = $this;
        $builder
          // ->add('ano','integer',array(
            //    'label'=>'Año',
              //  'attr'=>array('type'=>'number','min'=>2014,'max'=>'2020'),
                //'required'=>true
                   //))
                 
           ->add('formato', 'entity',array(
			'class'=>'ColegioAdminBundle:ModeloBoletin',
			'query_builder'=>function(EntityRepository $er) use($self){
                    return $er->CreateQueryBuilder('u')
                            ->where('u.colegio = :idColegio and u.estado = true')
                            ->setParameter('idColegio',$self->idColegio);
                },
                 'label'      => 'Tipo de Informe',
                 'required'   => true,
                 'mapped'     => false,
                 'empty_value'=> 'Selecciona',
                 'attr'      => array('class'=>'selector')       
            ))

          ->add('periodo', 'entity',array(
			'class'=>'ColegioBoletinBundle:Periodo',
			'query_builder'=>function(EntityRepository $er) use($self){
                    return $er->CreateQueryBuilder('u')

                            ->where('u.colegio = :idColegio')
                            ->orderBy('u.id','DESC')
                            ->setParameter('idColegio',$self->idColegio);
                },
                 'label'      => 'Periodo',
                 'required'   =>true,
                 'mapped'=>false,
                 'empty_value'=>'Selecciona',
                 'attr'      => array('class'=>'selector')  
                 ))
          /**->add('_format','choice',array(
                'label'=>'Formato de Salida',
                'choices'=>array('html'=>'HTML','pdf'=>'PDF en Prueba (No recomendado)'),
                'preferred_choices' => array('html'),
                'empty_value' => 'Escoge el Formato',
                'required'=>true,
                 'attr'      => array('class'=>'selector')  
                ))
          /**->add('periodoescolar', 'entity',array(
            'class'=>'ColegioAdminBundle:Parametros',
            'query_builder'=>function(EntityRepository $er) use($self){
                    return $er->CreateQueryBuilder('u')
                            ->select('u','p')
                            ->leftjoin('u.periodoEscolar','p')
                            ->where('u.colegio = :idColegio')
                            ->setParameter('idColegio',$self->idColegio);
                },
                 'label'      => 'Año parámetro',
                 'required'   =>true,
                 'mapped'=>false,
                 'empty_value'=>'Selecciona',
                 ))**/
           ->add('tamano','integer',array(
                'attr'=>array('step'=>'1','min'=>'0','value'=>'10'),
                'required'=>true,
                'label'=>'Tamaño del texto'
            ))
          ;
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_adminbundle_reporte';
    }
}
