<?php

namespace Colegio\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ParametrosType extends AbstractType
{
    public function __construct($colegio)
    {
      
        $this->colegio = $colegio;
        
    } 
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $self = $this; 
        $builder
            ->add('periodoEscolar','entity',array(
                'class'=>'ColegioAdminBundle:PeriodoEscolar',
                'query_builder'=>function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('p')
                                ->where('p.colegio = :colegio')
                                ->setParameter('colegio',$self->colegio);
                },
                'label'=>'Colegio',
            ))
            
            ->add('norma',null,array(
                'empty_value'=>false
            ))
            ->add('notaMinima')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Colegio\AdminBundle\Entity\Parametros'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'colegio_adminbundle_parametros';
    }
}
