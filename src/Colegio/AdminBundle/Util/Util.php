<?php

namespace Colegio\AdminBundle\Util;


class Util
{
    private   $context;
    protected $em;
	
    public function __construct($entityManager) 
    {
	    $this->em=$entityManager;
    }    
   
    public function getPeriodo($id)
    {
       $consulta = $this->em->createQuery('
          SELECT u FROM ColegioAdminBundle:PeriodoEscolar u WHERE u.colegio = :id AND u.estado = true ORDER BY u.id DESC');
        $consulta->setParameter('id', $id);
        $consulta->setMaxResults(1);
        $resultado = $consulta->getResult();
        return $resultado;
    }
}
