/// Script para cargar las estadisticas de los trabajos
$(document).ready(function(){

		$( '#consolidado' ).change(function()
		{
			if( $(this).val() == 2 )
			{
				$('#seccionPeriodoEscolar').show('slide');
				$('#seccionPeriodo').hide('slide');

			}
			else if ( $(this).val() == 1 )
			{
				$('#seccionPeriodoEscolar').hide('slide');
				$('#seccionPeriodo').show('slide');
			}
		})

		$( '#cargar' ).click(function(event){
			event.preventDefault();
			
			if( $.validarEstadistica() == true )
			{
				if( $( '#consolidado' ).val() == "1")
				{
					$.consultarTrabajosPeriodo( $( '#periodo' ).val() );				
				}
			}
		})

		$.validarEstadistica = function()
		{
			var mensaje = false;
			if( $('#estadistica').val() == "0" )
			{
				alert("No debe estar vacio");
				mensaje = false;
			}
			else
			{
				mensaje = true;
			}
			return mensaje;
		}

		

		$.consultarTrabajosPeriodo = function(periodo)
		{

			
			var rutaTrabajosPeriodo2 = rutaTrabajosPeriodo.replace("Periodo",periodo);
			
			tabla.row().remove().draw(false);
			var total = 0;
			$.ajax({
			        url:  rutaTrabajosPeriodo2,
			        dataType: "text",
			        success: function( data ) {
			           
			           var json = $.parseJSON(data);
                                                    for (var i in json) {
                                                                 var subTotal = parseInt(json[i]['total']); 
                                                                         total = total + subTotal;
                                                    }

                                                    for (var i in json) {

                                                             var porcentaje = parseInt(json[i]['total'])*100/total;
                                                                                     var porce = porcentaje.toFixed(1);
                                                                                     tabla.row.add([
                                                                                     "<a href='#' id='"+json[i]['tipoTrabajo']+"' class='tipotrabajo'>"+json[i]['detalle']+"</a>",
                                                                                     json[i]['total'],
                                                                                     porce+" %"
                                                                             ]).draw();
                                                    }
				        $.eventoTipoTrabajo(); 
				   }
                                   
			 });    
	    }
            
            $.eventoTipoTrabajo = function(){
                $( '.tipotrabajo' ).click(function(event){
                	event.preventDefault();
                	console.log("Tipo trabajo " + $(this).attr('id') );
                    $.cargarDatosGrafico( $( '#periodo' ).val(), $(this).attr('id'));
                });
            }
            
            $.eventoTipoTrabajo();
            
            
            $.cargarDatosGrafico = function(periodo,tipotrabajo){
                var rutaTrabajosPeriodoConsolidado2 = rutaTrabajosPeriodoConsolidado.replace("Periodo",periodo);
                var rutaTrabajosPeriodoConsolidado3 = rutaTrabajosPeriodoConsolidado2.replace("TipoTrabajo",tipotrabajo);
                
									 	var options = {
										    chart: {
										        renderTo: 'grafico',
										        plotBackgroundColor: null,
										        plotBorderWidth: null,
										        plotShadow: false
										    },
										    title: {
										        text: 'Porcentaje de Estudiantes'
										    },
										    tooltip: {
										        formatter: function() {
										            return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
										        }
										    },
										    plotOptions: {
										        pie: {
										            allowPointSelect: true,
										            cursor: 'pointer',
										            dataLabels: {
										                enabled: true,
										                color: '#000000',
										                connectorColor: '#000000',
										                formatter: function() {
										                    return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
										                }
										            }
										        }
										    },
										    series: [{
										        type: 'pie',
										        name: 'Browser share',
										        data: []
										    }]
										}

										$.getJSON(rutaTrabajosPeriodoConsolidado3, function(json) {

										    options.series[0].data = json;
										    chart = new Highcharts.Chart(options);
										});
				         
			}
			  
						    
});