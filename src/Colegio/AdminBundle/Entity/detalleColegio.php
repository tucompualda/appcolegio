<?php

namespace Colegio\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * detalleColegio
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\AdminBundle\Entity\detalleColegioRepository")
 * @ORM\HasLifecycleCallbacks
 */
class detalleColegio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\OneToOne(targetEntity="Colegio\AdminBundle\Entity\Colegio", inversedBy="detalle")
     * @ORM\JoinColumn(name="idColegio_id", referencedColumnName="id") 
     */
    private $idColegio;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Jornada")
     */
    private $idJornada;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Rector")
     */
    private $idRector;

    /**
     * @var string
     *
     * @ORM\Column(name="actualYear", type="string", length=255)
     */
    private $actualYear;

    /**
     * @var string
     *
     * @ORM\Column(name="capacidades", type="string", length=255)
     */
    private $capacidades;

    /**
     * @var string
     *
     * @ORM\Column(name="Discapacidades", type="string", length=255)
     */
    private $discapacidades;

    /**
     * @var string
     *
     * @ORM\Column(name="modeloEducativo", type="string", length=255)
     */
    private $modeloEducativo;

    /**
     * @ORM\OneToMany(targetEntity="Colegio\AdminBundle\Entity\Sede", mappedBy="idDetalleColegio", cascade={"persist","remove"})
     * @Assert\Valid()
     */
    private $sede;
    

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @Assert\Image(
     *     minWidth = 80,
     *     maxWidth = 120,
     *     minHeight = 80,
     *     maxHeight = 120
     * )
     */
    private $file;

    private $temp;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->path)) {
            // store the old name to delete after the update
            $this->temp = $this->path;
            $this->path = null;
        } else {
            $this->path = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }


    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // la ruta absoluta del directorio donde se deben
        // guardar los archivos cargados
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // se deshace del __DIR__ para no meter la pata
        // al mostrar el documento/imagen cargada en la vista.
        return 'uploads/documents';
    }

   /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            // haz lo que quieras para generar un nombre único
            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename.'.'.$this->getFile()->guessExtension();
        }
    }
    
   /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // si hay un error al mover el archivo, move() automáticamente
        // envía una excepción. This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->path);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

     /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
    
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sede = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sede
     * 
     * @return \Colegio\AdminBundle\Entity\Sede
     * 
     */
    public function setSede(\Doctrine\Common\Collections\Collection $sede) 
    {
         $this->sede = $sede;
        foreach ($sede as $sedes) {
            $sedes->setIdDetalleColegio($this);
        }
    }
    
    /**
     * Get sede
     * 
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSede() 
    {
        return $this->sede;
    }
    
    /**
     * Set idColegio
     *
     * @param \Colegio\AdminBundle\Entity\Colegio $idColegio
     * @return Colegio
     */
    public function setIdColegio(\Colegio\AdminBundle\Entity\Colegio $idColegio=null)
    {
        $this->idColegio = $idColegio;
    
        return $this;
    }

    /**
     * Get idColegio
     *
     * @return \Colegio\AdminBundle\Entity\Colegio
     */
    public function getIdColegio()
    {
        return $this->idColegio;
    }

    /**
     * Set idJornada
     *
     * @param string $idJornada
     * @return detalleColegio
     */
    public function setIdJornada(\Colegio\AdminBundle\Entity\Jornada $idJornada)
    {
        $this->idJornada = $idJornada;
    
        return $this;
    }

    /**
     * Get idJornada
     *
     * @return string 
     */
    public function getIdJornada()
    {
        return $this->idJornada;
    }

    /**
     * Set idRector
     *
     * @param string $idRector
     * @return detalleColegio
     */
    public function setIdRector(\Colegio\AdminBundle\Entity\Rector $idRector)
    {
        $this->idRector = $idRector;
    
        return $this;
    }

    /**
     * Get idRector
     *
     * @return string 
     */
    public function getIdRector()
    {
        return $this->idRector;
    }

    /**
     * Set actualYear
     *
     * @param string $actualYear
     * @return detalleColegio
     */
    public function setActualYear($actualYear)
    {
        $this->actualYear = $actualYear;
    
        return $this;
    }

    /**
     * Get actualYear
     *
     * @return string 
     */
    public function getActualYear()
    {
        return $this->actualYear;
    }

    /**
     * Set capacidades
     *
     * @param string $capacidades
     * @return detalleColegio
     */
    public function setCapacidades($capacidades)
    {
        $this->capacidades = $capacidades;
    
        return $this;
    }

    /**
     * Get capacidades
     *
     * @return string 
     */
    public function getCapacidades()
    {
        return $this->capacidades;
    }

    /**
     * Set discapacidades
     *
     * @param string $discapacidades
     * @return detalleColegio
     */
    public function setDiscapacidades($discapacidades)
    {
        $this->discapacidades = $discapacidades;
    
        return $this;
    }

    /**
     * Get discapacidades
     *
     * @return string 
     */
    public function getDiscapacidades()
    {
        return $this->discapacidades;
    }

    /**
     * Set modeloEducativo
     *
     * @param string $modeloEducativo
     * @return detalleColegio
     */
    public function setModeloEducativo($modeloEducativo)
    {
        $this->modeloEducativo = $modeloEducativo;
    
        return $this;
    }

    /**
     * Get modeloEducativo
     *
     * @return string 
     */
    public function getModeloEducativo()
    {
        return $this->modeloEducativo;
    }
    
    public function __toString() 
    {
        return $this->getIdColegio()." ".$this->getIdRector();
    }
}
