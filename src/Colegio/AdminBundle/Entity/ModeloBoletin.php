<?php

namespace Colegio\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ModeloBoletin
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\AdminBundle\Entity\ModeloBoletinRepository")
 */
class ModeloBoletin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Colegio")
     */
    private $colegio;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="encabezado", type="text")
     */
    private $encabezado;

    /**
     * @var string
     * 
     * @ORM\Column(name="textoExtra", type="text", nullable=true)
     */
    private $textoExtra;

    /**
     * @var string
     * 
     * @ORM\Column(name="titulo", type="text")
     */
    private $titulo;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="textoExtraSegundo", type="text", nullable=true)
     */
    private $textoExtraSegundo;

   /**
     * @var string
     * 
     * @ORM\Column(name="bloqueFirmas", type="text", nullable=true)
     */
    private $bloqueFirmas;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="footer", type="text", nullable=true)
     */
    private $footer;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="logros", type="boolean", nullable=true)
     */
    private $logros;

    /**
     * @var boolean
     *
     * @ORM\Column(name="observaciones", type="boolean", nullable=true)
     */
    private $observaciones;

    /**
     * @var boolean
     *
     * @ORM\Column(name="categoria", type="boolean", nullable=true)
     */
    private $categoria;

    /**
     * @var boolean
     *
     * @ORM\Column(name="posicion", type="boolean", nullable=true)
     */
    private $posicion;

        /**
     * @var boolean
     *
     * @ORM\Column(name="textoExtraSegundoMostrar", type="boolean", nullable=true)
     */
    private $textoExtraSegundoMostrar;
    
        /**
     * @var boolean
     *
     * @ORM\Column(name="bloqueFirmasMostrar", type="boolean", nullable=true)
     */
    private $bloqueFirmasMostrar;
    
    
        /**
     * @var boolean
     *
     * @ORM\Column(name="footerMostrar", type="boolean", nullable=true)
     */
    private $footerMostrar;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaCreacion", type="datetime")
     */
    private $fechaCreacion;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Usuarios\UsuariosBundle\Entity\Usuarios")
     */
    private $creadoPor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Usuarios\UsuariosBundle\Entity\Usuarios")
     */
    private $editadoPor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaEdicion", type="datetime")
     */
    private $fechaEdicion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return ModeloBoletin
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set colegio
     *
     * @param \Colegio\AdminBundle\Entity\Colegio $colegio
     * @return ModeloBoletin
     */
    public function setColegio(\Colegio\AdminBundle\Entity\Colegio $colegio)
    {
        $this->colegio = $colegio;
    
        return $this;
    }

    /**
     * Get colegio
     *
     * @return \Colegio\AdminBundle\Entity\Colegio
     */
    public function getColegio()
    {
        return $this->colegio;
    }

    /**
     * Set encabezado
     *
     * @param string $encabezado
     * @return ModeloBoletin
     */
    public function setEncabezado($encabezado)
    {
        $this->encabezado = $encabezado;
    
        return $this;
    }

    /**
     * Get encabezado
     *
     * @return string 
     */
    public function getEncabezado()
    {
        return $this->encabezado;
    }

    /**
     * Set textoExtra
     *
     * @param string $textoExtra
     * @return ModeloBoletin
     */
    public function setTextoExtra($textoExtra)
    {
        $this->textoExtra = $textoExtra;
    
        return $this;
    }

    /**
     * Get textoExtra
     *
     * @return string 
     */
    public function getTextoExtra()
    {
        return $this->textoExtra;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return ModeloBoletin
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    
        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set logros
     *
     * @param boolean $logros
     * @return ModeloBoletin
     */
    public function setLogros($logros)
    {
        $this->logros = $logros;
    
        return $this;
    }

    /**
     * Get logros
     *
     * @return boolean 
     */
    public function getLogros()
    {
        return $this->logros;
    }

    /**
     * Set observaciones
     *
     * @param string $observaciones
     * @return ModeloBoletin
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;
    
        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string 
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return ModeloBoletin
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;
    
        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set creadoPor
     *
     * @param \Usuarios\UsuariosBundle\Entity\Usuarios $creadoPor
     * @return ModeloBoletin
     */
    public function setCreadoPor(\Usuarios\UsuariosBundle\Entity\Usuarios $creadoPor)
    {
        $this->creadoPor = $creadoPor;
    
        return $this;
    }

    /**
     * Get creadoPor
     *
     * @return \Usuarios\UsuariosBundle\Entity\Usuarios 
     */
    public function getCreadoPor()
    {
        return $this->creadoPor;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return ModeloBoletin
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set editadoPor
     *
     * @param \Usuarios\UsuariosBundle\Entity\Usuarios $editadoPor
     * @return ModeloBoletin
     */
    public function setEditadoPor(\Usuarios\UsuariosBundle\Entity\Usuarios $editadoPor)
    {
        $this->editadoPor = $editadoPor;
    
        return $this;
    }

    /**
     * Get editadoPor
     *
     * @return \Usuarios\UsuariosBundle\Entity\Usuarios
     */
    public function getEditadoPor()
    {
        return $this->editadoPor;
    }

    /**
     * Set fechaEdicion
     *
     * @param \DateTime $fechaEdicion
     * @return ModeloBoletin
     */
    public function setFechaEdicion($fechaEdicion)
    {
        $this->fechaEdicion = $fechaEdicion;
    
        return $this;
    }

    /**
     * Get fechaEdicion
     *
     * @return \DateTime 
     */
    public function getFechaEdicion()
    {
        return $this->fechaEdicion;
    }
    
    public function __toString()
    {
		return $this->getNombre();
	}

    /**
     * Set categoria
     *
     * @param boolean $categoria
     * @return ModeloBoletin
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    
        return $this;
    }

    /**
     * Get categoria
     *
     * @return boolean 
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set posicion
     *
     * @param boolean $posicion
     * @return ModeloBoletin
     */
    public function setPosicion($posicion)
    {
        $this->posicion = $posicion;
    
        return $this;
    }

    /**
     * Get posicion
     *
     * @return boolean 
     */
    public function getPosicion()
    {
        return $this->posicion;
    }

    /**
     * Set textoExtraSegundo
     *
     * @param string $textoExtraSegundo
     * @return ModeloBoletin
     */
    public function setTextoExtraSegundo($textoExtraSegundo)
    {
        $this->textoExtraSegundo = $textoExtraSegundo;
    
        return $this;
    }

    /**
     * Get textoExtraSegundo
     *
     * @return string 
     */
    public function getTextoExtraSegundo()
    {
        return $this->textoExtraSegundo;
    }

    /**
     * Set bloqueFirmas
     *
     * @param string $bloqueFirmas
     * @return ModeloBoletin
     */
    public function setBloqueFirmas($bloqueFirmas)
    {
        $this->bloqueFirmas = $bloqueFirmas;
    
        return $this;
    }

    /**
     * Get bloqueFirmas
     *
     * @return string 
     */
    public function getBloqueFirmas()
    {
        return $this->bloqueFirmas;
    }

    /**
     * Set footer
     *
     * @param string $footer
     * @return ModeloBoletin
     */
    public function setFooter($footer)
    {
        $this->footer = $footer;
    
        return $this;
    }

    /**
     * Get footer
     *
     * @return string 
     */
    public function getFooter()
    {
        return $this->footer;
    }

    /**
     * Set textoExtraSegundoMostrar
     *
     * @param boolean $textoExtraSegundoMostrar
     * @return ModeloBoletin
     */
    public function setTextoExtraSegundoMostrar($textoExtraSegundoMostrar)
    {
        $this->textoExtraSegundoMostrar = $textoExtraSegundoMostrar;
    
        return $this;
    }

    /**
     * Get textoExtraSegundoMostrar
     *
     * @return boolean 
     */
    public function getTextoExtraSegundoMostrar()
    {
        return $this->textoExtraSegundoMostrar;
    }

    /**
     * Set bloqueFirmasMostrar
     *
     * @param boolean $bloqueFirmasMostrar
     * @return ModeloBoletin
     */
    public function setBloqueFirmasMostrar($bloqueFirmasMostrar)
    {
        $this->bloqueFirmasMostrar = $bloqueFirmasMostrar;
    
        return $this;
    }

    /**
     * Get bloqueFirmasMostrar
     *
     * @return boolean 
     */
    public function getBloqueFirmasMostrar()
    {
        return $this->bloqueFirmasMostrar;
    }

    /**
     * Set footerMostrar
     *
     * @param boolean $footerMostrar
     * @return ModeloBoletin
     */
    public function setFooterMostrar($footerMostrar)
    {
        $this->footerMostrar = $footerMostrar;
    
        return $this;
    }

    /**
     * Get footerMostrar
     *
     * @return boolean 
     */
    public function getFooterMostrar()
    {
        return $this->footerMostrar;
    }
}