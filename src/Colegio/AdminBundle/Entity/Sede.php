<?php

namespace Colegio\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sede
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\AdminBundle\Entity\SedeRepository")
 */
class Sede
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\detalleColegio", inversedBy="sede")
     * @ORM\JoinColumn(name="idDetalleColegio_id", referencedColumnName="id")
     */
    private $idDetalleColegio;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreSede", type="string", length=255)
     */
    private $nombreSede;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado;
    
    /**
     * Campo q llama a Rector
     * @ORM\OneToOne(targetEntity="Rector", mappedBy="idSede", cascade={"persist","remove"})
     */
    private $rector;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idDetalleColegio
     *
     * @param \Colegio\AdminBundle\Entity\detalleColegio $idDetalleColegio
     * @return Sede
     */
    public function setIdDetalleColegio(\Colegio\AdminBundle\Entity\detalleColegio $idDetalleColegio = null)
    {
        $this->idDetalleColegio = $idDetalleColegio;
    
        return $this;
    }

    /**
     * Get idDetalleColegio
     *
     * @return \Colegio\AdminBundle\Entity\detalleColegio
     */
    public function getIdDetalleColegio()
    {
        return $this->idDetalleColegio;
    }

    /**
     * Set nombreSede
     *
     * @param string $nombreSede
     * @return Sede
     */
    public function setNombreSede($nombreSede)
    {
        $this->nombreSede = $nombreSede;
    
        return $this;
    }

    /**
     * Get nombreSede
     *
     * @return string 
     */
    public function getNombreSede()
    {
        return $this->nombreSede;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return Sede
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }
    
     /**
     * Set rector
     * @param \Colegio\AdminBundle\Entity\Rector $rector
     * @return idSede
     */
    public function SetRector(\Colegio\AdminBundle\Entity\Rector $rector = null)
    {
        $this->rector = $rector;
        $rector->setIdSede($this);
        return $this;
    }

    /**
     * Get rector
     * 
     * @return \Colegio\AdminBundle\Entity\Rector
     */
    public function GetRector()
    {
        return $this->rector;
        
    }

    
    
    public function __toString()
    {
        return $this->getNombreSede();
    }
}