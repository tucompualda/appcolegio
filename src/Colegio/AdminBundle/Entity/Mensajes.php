<?php

namespace Colegio\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mensajes
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\AdminBundle\Entity\MensajesRepository")
 */
class Mensajes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\DocenteBundle\Entity\Docente")
     */
    private $docente;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\EstudianteBundle\Entity\Estudiante")
     */
    private $estudiante;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Usuarios\UsuariosBundle\Entity\Usuarios")
     */
    private $administrador;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaCreacion", type="datetime")
     */
    private $fechaCreacion;

    /**
     * @var string
     *
     * @ORM\Column(name="asunto", type="string", length=255)
     */
    private $asunto;

    /**
     * @var string
     *
     * @ORM\Column(name="mensaje", type="text")
     */
    private $mensaje;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaModificacion", type="datetime")
     */
    private $fechaModificacion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado;

    /**
     * @var boolean
     *
     * @ORM\Column(name="borrador", type="boolean")
     */
    private $borrador;  

    /**
     * @var boolean
     *
     * @ORM\Column(name="vistoDocente", type="boolean", nullable=true)
     */
    private $vistoDocente = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="vistoEstudiante", type="boolean", nullable=true)
     */
    private $vistoEstudiante = false;

      /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\DocenteBundle\Entity\Docente")
     */
    private $docenteDestinatario;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\EstudianteBundle\Entity\Estudiante")
     */
    private $estudianteDestinatario;  

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Usuarios\UsuariosBundle\Entity\Usuarios")
     */
    private $administradorDestinatario;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set docente
     *
     * @param string $docente
     * @return Mensajes
     */
    public function setDocente(\Colegio\DocenteBundle\Entity\Docente $docente)
    {
        $this->docente = $docente;
    
        return $this;
    }

    /**
     * Get docente
     *
     * @return string 
     */
    public function getDocente()
    {
        return $this->docente;
    }

    /**
     * Set estudiante
     *
     * @param string $estudiante
     * @return Mensajes
     */
    public function setEstudiante(\Colegio\EstudianteBundle\Entity\Estudiante $estudiante)
    {
        $this->estudiante = $estudiante;
    
        return $this;
    }

    /**
     * Get estudiante
     *
     * @return string 
     */
    public function getEstudiante()
    {
        return $this->estudiante;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return Mensajes
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;
    
        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set asunto
     *
     * @param string $asunto
     * @return Mensajes
     */
    public function setAsunto($asunto)
    {
        $this->asunto = $asunto;
    
        return $this;
    }

    /**
     * Get asunto
     *
     * @return string 
     */
    public function getAsunto()
    {
        return $this->asunto;
    }

    /**
     * Set mensaje
     *
     * @param string $mensaje
     * @return Mensajes
     */
    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;
    
        return $this;
    }

    /**
     * Get mensaje
     *
     * @return string 
     */
    public function getMensaje()
    {
        return $this->mensaje;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     * @return Mensajes
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;
    
        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime 
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return Mensajes
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get borrador
     *
     * @return boolean 
     */
    public function getBorrador()
    {
        return $this->borrador;
    }
    
    /**
     * Set borrador
     *
     * @param boolean $borrador
     * @return Mensajes
     */
    public function setBorrador($borrador)
    {
        $this->borrador = $borrador;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set administrador
     *
     * @param \Usuarios\UsuariosBundle\Entity\Usuarios $administrador
     * @return Mensajes
     */
    public function setAdministrador(\Usuarios\UsuariosBundle\Entity\Usuarios $administrador = null)
    {
        $this->administrador = $administrador;
    
        return $this;
    }

    /**
     * Get administrador
     *
     * @return \Usuarios\UsuariosBundle\Entity\Usuarios 
     */
    public function getAdministrador()
    {
        return $this->administrador;
    }

    /**
     * Set docenteDestinatario
     *
     * @param \Colegio\DocenteBundle\Entity\Docente $docenteDestinatario
     * @return Mensajes
     */
    public function setDocenteDestinatario(\Colegio\DocenteBundle\Entity\Docente $docenteDestinatario = null)
    {
        $this->docenteDestinatario = $docenteDestinatario;
    
        return $this;
    }

    /**
     * Get docenteDestinatario
     *
     * @return \Colegio\DocenteBundle\Entity\Docente 
     */
    public function getDocenteDestinatario()
    {
        return $this->docenteDestinatario;
    }

    /**
     * Set estudianteDestinatario
     *
     * @param \Colegio\EstudianteBundle\Entity\Estudiante $estudianteDestinatario
     * @return Mensajes
     */
    public function setEstudianteDestinatario(\Colegio\EstudianteBundle\Entity\Estudiante $estudianteDestinatario = null)
    {
        $this->estudianteDestinatario = $estudianteDestinatario;
    
        return $this;
    }

    /**
     * Get estudianteDestinatario
     *
     * @return \Colegio\EstudianteBundle\Entity\Estudiante 
     */
    public function getEstudianteDestinatario()
    {
        return $this->estudianteDestinatario;
    }

    /**
     * Set administradorDestinatario
     *
     * @param \Usuarios\UsuariosBundle\Entity\Usuarios $administradorDestinatario
     * @return Mensajes
     */
    public function setAdministradorDestinatario(\Usuarios\UsuariosBundle\Entity\Usuarios $administradorDestinatario = null)
    {
        $this->administradorDestinatario = $administradorDestinatario;
    
        return $this;
    }

    /**
     * Get administradorDestinatario
     *
     * @return \Usuarios\UsuariosBundle\Entity\Usuarios 
     */
    public function getAdministradorDestinatario()
    {
        return $this->administradorDestinatario;
    }

    /**
     * Set vistoDocente
     *
     * @param boolean $vistoDocente
     * @return Mensajes
     */
    public function setVistoDocente($vistoDocente)
    {
        $this->vistoDocente = $vistoDocente;
    
        return $this;
    }

    /**
     * Get vistoDocente
     *
     * @return boolean 
     */
    public function getVistoDocente()
    {
        return $this->vistoDocente;
    }

    /**
     * Set vistoEstudiante
     *
     * @param boolean $vistoEstudiante
     * @return Mensajes
     */
    public function setVistoEstudiante($vistoEstudiante)
    {
        $this->vistoEstudiante = $vistoEstudiante;
    
        return $this;
    }

    /**
     * Get vistoEstudiante
     *
     * @return boolean 
     */
    public function getVistoEstudiante()
    {
        return $this->vistoEstudiante;
    }
}