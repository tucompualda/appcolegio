<?php

namespace Colegio\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Parametros
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\AdminBundle\Entity\ParametrosRepository")
 */
class Parametros
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\PeriodoEscolar", inversedBy="parametros")
     */
    private $periodoEscolar;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Norma")
     */
    private $norma;
    
   /**
     * @var float
     *
     * @ORM\Column(name="notaminima", type="float")
     */
    private $notaMinima;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Colegio")
     */
    private $colegio;

   /**
     * @ORM\OneToMany(targetEntity="Colegio\AdminBundle\Entity\ParametrosItems", mappedBy="parametro", cascade={"persist","remove"})
     * @Assert\Valid()    
     */
    private $parametrositems;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set periodoEscolar
     *
     * @param \Colegio\AdminBundle\Entity\PeriodoEscolar $periodoEscolar
     * @return Parametros
     */
    public function setPeriodoEscolar(\Colegio\AdminBundle\Entity\PeriodoEscolar $periodoEscolar)
    {
        $this->periodoEscolar = $periodoEscolar;
    
        return $this;   
    }

    /**
     * Get periodoEscolar
     *
     * @return \Colegio\AdminBundle\Entity\PeriodoEscolar 
     */
    public function getPeriodoEscolar()
    {
        return $this->periodoEscolar;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Parametros
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    
        return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set colegio
     *
     * @param string $colegio
     * @return Parametros
     */
    public function setColegio(\Colegio\AdminBundle\Entity\Colegio $colegio)
    {
        $this->colegio = $colegio;
    
        return $this;
    }

    /**
     * Get colegio
     *
     * @return \Colegio\AdminBundle\Entity\Colegio 
     */
    public function getColegio()
    {
        return $this->colegio;
    }

    public function __toString()
    {
        return $this->getPeriodoEscolar()."";
    }

    /**
     * Set norma
     *
     * @param \Colegio\AdminBundle\Entity\Norma $norma
     * @return Parametros
     */
    public function setNorma(\Colegio\AdminBundle\Entity\Norma $norma = null)
    {
        $this->norma = $norma;
    
        return $this;
    }

    /**
     * Get norma
     *
     * @return \Colegio\AdminBundle\Entity\Norma 
     */
    public function getNorma()
    {
        return $this->norma;
    }

    /**
     * Set notaMinima
     *
     * @param float $notaMinima
     * @return Parametros
     */
    public function setNotaMinima($notaMinima)
    {
        $this->notaMinima = $notaMinima;
    
        return $this;
    }

    /**
     * Get notaMinima
     *
     * @return float 
     */
    public function getNotaMinima()
    {
        return $this->notaMinima;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->parametrositems = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add parametrositems
     *
     * @param \Colegio\AdminBundle\Entity\ParametrosItems $parametrositems
     * @return Parametros
     */
    public function addParametrositem(\Colegio\AdminBundle\Entity\ParametrosItems $parametrositems)
    {
        $this->parametrositems[] = $parametrositems;
    
        return $this;
    }

    /**
     * Remove parametrositems
     *
     * @param \Colegio\AdminBundle\Entity\ParametrosItems $parametrositems
     */
    public function removeParametrositem(\Colegio\AdminBundle\Entity\ParametrosItems $parametrositems)
    {
        $this->parametrositems->removeElement($parametrositems);
    }

    /**
     * Get parametrositems
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getParametrositems()
    {
        return $this->parametrositems;
    }
}