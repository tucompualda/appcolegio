<?php

namespace Colegio\AdminBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * MensajesRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MensajesRepository extends EntityRepository
{
    public function findMensajes($id)
    {
        //Recibe el parámetro id del docente 
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('
            SELECT u FROM ColegioAdminBundle:Mensajes u  
            WHERE u.docenteDestinatario = :id ORDER BY u.id DESC
            
        ');
        $consulta->setParameter('id', $id);
        return $consulta->getResult();
    }

        public function findMensajesEnviadosDocente($id)
    {
        //Recibe el parámetro id del docente 
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('
            SELECT u FROM ColegioAdminBundle:Mensajes u  
            WHERE u.docente = :id ORDER BY u.id DESC
            
        ');
        $consulta->setParameter('id', $id);
        return $consulta->getResult();
    }

    public function findMensajesDocentes($estudiante,$docente)
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT m.id,d.nombres AS nombreDocente,d.apellidos AS apellidoDocente, m.vistoDocente, m.vistoEstudiante, '
            .' e.id AS identDocenteDestinatario, t.id AS identEstudianteDestinatario, e.nombres AS nombreProfeDestinatario, e.apellidos AS apellidosProfeDestinatario'
            .', m.mensaje,m.asunto, m.fechaModificacion, h.primerNombre AS nombreEstudianteDestinatario, h.segundoNombre AS nombreSegundoEstudianteDestinatario, h.primerApellido AS apellidoEstudianteDestinatario, h.segundoApellido AS apellidoSegundoEstudianteDestinatario '
            .' FROM ColegioAdminBundle:Mensajes m '
            .' LEFT JOIN m.docente d LEFT JOIN m.docenteDestinatario e LEFT JOIN m.estudianteDestinatario t LEFT JOIN m.estudiante h'
            .' WHERE m.docenteDestinatario = :docente AND m.estudiante = :estudiante  OR  m.docente = :docente AND m.estudianteDestinatario  = :estudiante'
            .' ORDER BY m.fechaCreacion DESC ');
        $consulta->setParameter('estudiante',$estudiante);
        $consulta->setParameter('docente',$docente);

        return $consulta->getArrayResult();
    }

    public function findMensajesNuevosDocentes($estudiante,$docente)
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT m.id,d.nombres AS nombreDocente,d.apellidos AS apellidoDocente, m.vistoDocente, m.vistoEstudiante, '
            .' e.id AS identDocenteDestinatario, t.id AS identEstudianteDestinatario, e.nombres AS nombreProfeDestinatario, e.apellidos AS apellidosProfeDestinatario'
            .', m.mensaje,m.asunto, m.fechaModificacion, h.primerNombre AS nombreEstudianteDestinatario, h.segundoNombre AS nombreSegundoEstudianteDestinatario, h.primerApellido AS apellidoEstudianteDestinatario, h.segundoApellido AS apellidoSegundoEstudianteDestinatario '
            .' FROM ColegioAdminBundle:Mensajes m '
            .' LEFT JOIN m.docente d LEFT JOIN m.docenteDestinatario e LEFT JOIN m.estudianteDestinatario t LEFT JOIN m.estudiante h'
            .' WHERE m.vistoDocente = :estado AND m.docente = :docente AND m.estudiante = :estudiante'
            .' ORDER BY m.fechaCreacion DESC ');
        
        $consulta->setParameter('docente',$docente);
        $consulta->setParameter('estudiante',$estudiante);
        $consulta->setParameter('estado',false);
        $consulta->setMaxResults(1);
        return $consulta->getArrayResult();
    }

    public function findTotalMensajesNuevosDocentes($estudiante,$docente)
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT COUNT(m.id) AS total '
            .' FROM ColegioAdminBundle:Mensajes m '
            .' WHERE m.vistoDocente = :estado AND m.docenteDestinatario = :docente'
            .'');
        
        $consulta->setParameter('docente',$docente);
        $consulta->setParameter('estado',false);
        //$consulta->setMaxResults(1);
        return $consulta->getOneOrNullResult();
    }


    public function findMensajesNuevosEstudiantes($estudiante,$docente)
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT m.id,d.nombres AS nombreDocente,d.apellidos AS apellidoDocente, m.vistoDocente, m.vistoEstudiante, '
            .' e.id AS identDocenteDestinatario, t.id AS identEstudianteDestinatario, e.nombres AS nombreProfeDestinatario, e.apellidos AS apellidosProfeDestinatario'
            .', m.mensaje,m.asunto, m.fechaModificacion, h.primerNombre AS nombreEstudianteDestinatario, h.segundoNombre AS nombreSegundoEstudianteDestinatario, h.primerApellido AS apellidoEstudianteDestinatario, h.segundoApellido AS apellidoSegundoEstudianteDestinatario '
            .' FROM ColegioAdminBundle:Mensajes m '
            .' LEFT JOIN m.docente d LEFT JOIN m.docenteDestinatario e LEFT JOIN m.estudianteDestinatario t LEFT JOIN m.estudiante h'
            .' WHERE m.vistoEstudiante = :estado AND m.estudiante = :estudiante AND m.docenteDestinatario = :docente'
            .' ORDER BY m.fechaCreacion DESC ');
        
        $consulta->setParameter('estudiante',$estudiante);
        $consulta->setParameter('docente',$docente);
        $consulta->setParameter('estado',false);
        $consulta->setMaxResults(1);
        return $consulta->getArrayResult();
    }

     public function findTotalMensajesNuevosEstudiantes($estudiante,$docente)
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('SELECT COUNT(m.id) AS total '
            .' FROM ColegioAdminBundle:Mensajes m '
            .' LEFT JOIN m.docente d LEFT JOIN m.docenteDestinatario e LEFT JOIN m.estudianteDestinatario t LEFT JOIN m.estudiante h'
            .' WHERE m.vistoEstudiante = :estado AND m.estudiante = :estudiante'
            .' ORDER BY m.fechaCreacion DESC ');
        
        $consulta->setParameter('estudiante',$estudiante);
        $consulta->setParameter('estado',false);
        
        return $consulta->getOneOrNullResult();
    }

}
