<?php

namespace Colegio\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PorcentajeTrabajo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\AdminBundle\Entity\PorcentajeTrabajoRepository")
 */
class PorcentajeTrabajo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\TipoTrabajo")
     */
    private $tipoTrabajo;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\BoletinBundle\Entity\Periodo")
     */
    private $periodo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaCreacion", type="datetime")
     */
    private $fechaCreacion;

    /**
     * @var float
     *
     * @ORM\Column(name="porcentaje", type="float")
     */
    private $porcentaje;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaModificacion", type="datetime")
     */
    private $fechaModificacion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipoTrabajo
     *
     * @param \Colegio\AdminBundle\Entity\TipoTrabajo $tipoTrabajo
     * @return PorcentajeTrabajo
     */
    public function setTipoTrabajo(\Colegio\AdminBundle\Entity\TipoTrabajo $tipoTrabajo)
    {
        $this->tipoTrabajo = $tipoTrabajo;
    
        return $this;
    }

    /**
     * Get tipoTrabajo
     *
     * @return \Colegio\AdminBundle\Entity\TipoTrabajo 
     */
    public function getTipoTrabajo()
    {
        return $this->tipoTrabajo;
    }

    /**
     * Set periodo
     *
     * @param \Colegio\BoletinBundle\Entity\Periodo $periodo
     * @return PorcentajeTrabajo
     */
    public function setPeriodo(\Colegio\BoletinBundle\Entity\Periodo $periodo)
    {
        $this->periodo = $periodo;
    
        return $this;
    }

    /**
     * Get periodo
     *
     * @return \Colegio\BoletinBundle\Entity\Periodo
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return PorcentajeTrabajo
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;
    
        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set porcentaje
     *
     * @param float $porcentaje
     * @return PorcentajeTrabajo
     */
    public function setPorcentaje($porcentaje)
    {
        $this->porcentaje = $porcentaje;
    
        return $this;
    }

    /**
     * Get porcentaje
     *
     * @return string 
     */
    public function getPorcentaje()
    {
        return $this->porcentaje;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return PorcentajeTrabajo
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     * @return PorcentajeTrabajo
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;
    
        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime 
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }
    
    public function __toString()
    {
    	return $this->getTipoTrabajo()." (". $this->getPorcentaje() ." %)";
    }
}
