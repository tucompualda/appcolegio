<?php

namespace Colegio\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParametrosItems
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\AdminBundle\Entity\ParametrosItemsRepository")
 */
class ParametrosItems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Parametros", inversedBy="parametrositems")
     */
    private $parametro;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Item", inversedBy="parametrositems", cascade={"persist"})
     */
    private $item;

    /**
     * @var float
     *
     * @ORM\Column(name="limiteSuperior", type="float", nullable=true)
     */
    private $limiteSuperior;

    /**
     * @var float
     *
     * @ORM\Column(name="limiteInferior", type="float", nullable=true)
     */
    private $limiteInferior;

   /**
     * @var string
     *
     * @ORM\Column(name="descripcionCualitativa", type="string", length=255, nullable=true)
     */
    private $descripcionCualitativa;

   /**
     * @var string
     *
     * @ORM\Column(name="notaCualitativa", type="string", length=255, nullable=true)
     */
    private $notaCualitativa;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

   

    /**
     * Set limiteSuperior
     *
     * @param float $limiteSuperior
     * @return ParametrosItems
     */
    public function setLimiteSuperior($limiteSuperior)
    {
        $this->limiteSuperior = $limiteSuperior;
    
        return $this;
    }

    /**
     * Get limiteSuperior
     *
     * @return float 
     */
    public function getLimiteSuperior()
    {
        return $this->limiteSuperior;
    }

    /**
     * Set parametro
     *
     * @param \Colegio\AdminBundle\Entity\Parametros $parametro
     * @return ParametrosItems
     */
    public function setParametro(\Colegio\AdminBundle\Entity\Parametros $parametro = null)
    {
        $this->parametro = $parametro;
    
        return $this;
    }

    /**
     * Get parametro
     *
     * @return \Colegio\AdminBundle\Entity\Parametros 
     */
    public function getParametro()
    {
        return $this->parametro;
    }

    /**
     * Set item
     *
     * @param \Colegio\AdminBundle\Entity\Item $item
     * @return ParametrosItems
     */
    public function setItem(\Colegio\AdminBundle\Entity\Item $item = null)
    {
        $this->item = $item;
    
        return $this;
    }

    /**
     * Get item
     *
     * @return \Colegio\AdminBundle\Entity\Item 
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set limiteInferior
     *
     * @param float $limiteInferior
     * @return ParametrosItems
     */
    public function setLimiteInferior($limiteInferior)
    {
        $this->limiteInferior = $limiteInferior;
    
        return $this;
    }

    /**
     * Get limiteInferior
     *
     * @return float 
     */
    public function getLimiteInferior()
    {
        return $this->limiteInferior;
    }

    /**
     * Set descripcionCualitativa
     *
     * @param string $descripcionCualitativa
     * @return ParametrosItems
     */
    public function setDescripcionCualitativa($descripcionCualitativa)
    {
        $this->descripcionCualitativa = $descripcionCualitativa;
    
        return $this;
    }

    /**
     * Get descripcionCualitativa
     *
     * @return string 
     */
    public function getDescripcionCualitativa()
    {
        return $this->descripcionCualitativa;
    }

    /**
     * Set notaCualitativa
     *
     * @param string $notaCualitativa
     * @return ParametrosItems
     */
    public function setNotaCualitativa($notaCualitativa)
    {
        $this->notaCualitativa = $notaCualitativa;
    
        return $this;
    }

    /**
     * Get notaCualitativa
     *
     * @return string 
     */
    public function getNotaCualitativa()
    {
        return $this->notaCualitativa;
    }
}