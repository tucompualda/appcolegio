<?php

namespace Colegio\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MatriculasCirculares
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\AdminBundle\Entity\MatriculasCircularesRepository")
 */
class MatriculasCirculares
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\EstudianteBundle\Entity\GrupoEstudiante")
     */
    private $matricula;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Circulares")
     */
    private $circular;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaVisita", type="datetime")
     */
    private $fechaVisita;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return MatriculasCirculares
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set fechaVisita
     *
     * @param \DateTime $fechaVisita
     * @return MatriculasCirculares
     */
    public function setFechaVisita($fechaVisita)
    {
        $this->fechaVisita = $fechaVisita;
    
        return $this;
    }

    /**
     * Get fechaVisita
     *
     * @return \DateTime 
     */
    public function getFechaVisita()
    {
        return $this->fechaVisita;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return MatriculasCirculares
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set matricula
     *
     * @param \Colegio\EstudianteBundle\Entity\GrupoEstudiante $matricula
     * @return MatriculasCirculares
     */
    public function setMatricula(\Colegio\EstudianteBundle\Entity\GrupoEstudiante $matricula = null)
    {
        $this->matricula = $matricula;
    
        return $this;
    }

    /**
     * Get matricula
     *
     * @return \Colegio\EstudianteBundle\Entity\GrupoEstudiante 
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * Set circular
     *
     * @param \Colegio\AdminBundle\Entity\Circulares $circular
     * @return MatriculasCirculares
     */
    public function setCircular(\Colegio\AdminBundle\Entity\Circulares $circular = null)
    {
        $this->circular = $circular;
    
        return $this;
    }

    /**
     * Get circular
     *
     * @return \Colegio\AdminBundle\Entity\Circulares 
     */
    public function getCircular()
    {
        return $this->circular;
    }
}