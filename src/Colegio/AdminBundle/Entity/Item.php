<?php

namespace Colegio\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Item
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\AdminBundle\Entity\ItemRepository")
 */
class Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Norma")
     */
    private $norma;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="Colegio\AdminBundle\Entity\ParametrosItems", mappedBy="item", cascade={})
     */
    private $parametrositems;
    
    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="text")
     */
    private $icon;
    
    /**
     * @var string
     *
     * @ORM\Column(name="span", type="text")
     */
    private $span;

     /**
     * @var string
     *
     * @ORM\Column(name="iconBoostrap3", type="text")
     */
    private $iconBoostrap3;
    
    /**
     * @var string
     *
     * @ORM\Column(name="spanBoostrap3", type="text")
     */
    private $spanBoostrap3;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Item
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Item
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set norma
     *
     * @param \Colegio\AdminBundle\Entity\Norma $norma
     * @return Item
     */
    public function setNorma(\Colegio\AdminBundle\Entity\Norma $norma = null)
    {
        $this->norma = $norma;
    
        return $this;
    }

    /**
     * Get norma
     *
     * @return \Colegio\AdminBundle\Entity\Norma 
     */
    public function getNorma()
    {
        return $this->norma;
    }
    
    public function __toString() {
        return $this->getNombre();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->parametrositems = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add parametrositems
     *
     * @param \Colegio\AdminBundle\Entity\ParametrosItems $parametrositems
     * @return Item
     */
    public function addParametrositem(\Colegio\AdminBundle\Entity\ParametrosItems $parametrositems)
    {
        $this->parametrositems[] = $parametrositems;
    
        return $this;
    }

    /**
     * Remove parametrositems
     *
     * @param \Colegio\AdminBundle\Entity\ParametrosItems $parametrositems
     */
    public function removeParametrositem(\Colegio\AdminBundle\Entity\ParametrosItems $parametrositems)
    {
        $this->parametrositems->removeElement($parametrositems);
    }

    /**
     * Get parametrositems
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getParametrositems()
    {
        return $this->parametrositems;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return Item
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    
        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set span
     *
     * @param string $span
     * @return Item
     */
    public function setSpan($span)
    {
        $this->span = $span;
    
        return $this;
    }

    /**
     * Get span
     *
     * @return string 
     */
    public function getSpan()
    {
        return $this->span;
    }

    /**
     * Set iconBoostrap3
     *
     * @param string $iconBoostrap3
     * @return Item
     */
    public function setIconBoostrap3($iconBoostrap3)
    {
        $this->iconBoostrap3 = $iconBoostrap3;
    
        return $this;
    }

    /**
     * Get iconBoostrap3
     *
     * @return string 
     */
    public function getIconBoostrap3()
    {
        return $this->iconBoostrap3;
    }

    /**
     * Set spanBoostrap3
     *
     * @param string $spanBoostrap3
     * @return Item
     */
    public function setSpanBoostrap3($spanBoostrap3)
    {
        $this->spanBoostrap3 = $spanBoostrap3;
    
        return $this;
    }

    /**
     * Get spanBoostrap3
     *
     * @return string 
     */
    public function getSpanBoostrap3()
    {
        return $this->spanBoostrap3;
    }
}