<?php

namespace Colegio\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GrupoCircular
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\AdminBundle\Entity\GrupoCircularRepository")
 */
class GrupoCircular
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="circular", type="string", length=255)
     */
    private $circular;

    /**
     * @var string
     *
     * @ORM\Column(name="grupo", type="string", length=255)
     */
    private $grupo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set circular
     *
     * @param string $circular
     * @return GrupoCircular
     */
    public function setCircular($circular)
    {
        $this->circular = $circular;
    
        return $this;
    }

    /**
     * Get circular
     *
     * @return string 
     */
    public function getCircular()
    {
        return $this->circular;
    }

    /**
     * Set grupo
     *
     * @param string $grupo
     * @return GrupoCircular
     */
    public function setGrupo($grupo)
    {
        $this->grupo = $grupo;
    
        return $this;
    }

    /**
     * Get grupo
     *
     * @return string 
     */
    public function getGrupo()
    {
        return $this->grupo;
    }
}
