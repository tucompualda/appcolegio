<?php

namespace Colegio\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Circulares
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\AdminBundle\Entity\CircularesRepository")
 */
class Circulares
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="todos", type="boolean", nullable=true)
     */
    private $todos;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Colegio\GrupoBundle\Entity\Grupo")
     * @ORM\JoinTable(name="destinatario_grupo",
     *          joinColumns={@ORM\JoinColumn(name="circular_id", referencedColumnName="id")},
     *          inverseJoinColumns={@ORM\JoinColumn(name="destinatario_id", referencedColumnName="id")}
     *  )
     */
    private $destinatarios;

    /**
     * @var boolean
     *
     * @ORM\Column(name="publicado", type="boolean", nullable=true)
     */
    private $publicado;



    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="detalle", type="text")
     */
    private $detalle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaVisita", type="datetime")
     */
    private $fechaVisita;

   /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Colegio")
     */
    private $colegio;

     /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\DocenteBundle\Entity\Docente")
     */
    private $docente;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\PeriodoEscolar")
     */
    private $periodoEscolar;

    public function __construct() 
    {
        $this->destinatarios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set todos
     *
     * @param boolean $todos
     * @return Circulares
     */
    public function setTodos($todos)
    {
        $this->todos = $todos;
    
        return $this;
    }

    /**
     * Get todos
     *
     * @return boolean 
     */
    public function getTodos()
    {
        return $this->todos;
    }


    /**
     * Set publicado
     *
     * @param boolean $publicado
     * @return Circulares
     */
    public function setPublicado($publicado)
    {
        $this->publicado = $publicado;
    
        return $this;
    }

    /**
     * Get publicado
     *
     * @return boolean 
     */
    public function getPublicado()
    {
        return $this->publicado;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Circulares
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    
        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set detalle
     *
     * @param string $detalle
     * @return Circulares
     */
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;
    
        return $this;
    }

    /**
     * Get detalle
     *
     * @return string 
     */
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Circulares
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set fechaVisita
     *
     * @param \DateTime $fechaVisita
     * @return Circulares
     */
    public function setFechaVisita($fechaVisita)
    {
        $this->fechaVisita = $fechaVisita;
    
        return $this;
    }

    /**
     * Get fechaVisita
     *
     * @return \DateTime 
     */
    public function getFechaVisita()
    {
        return $this->fechaVisita;
    }

    public function __toString()
    {
        return $this->getTitulo();
    }

    /**
     * Add destinatarios
     *
     * @param \Colegio\GrupoBundle\Entity\Grupo $destinatarios
     * @return Circulares
     */
    public function addDestinatario(\Colegio\GrupoBundle\Entity\Grupo $destinatarios)
    {
        $this->destinatarios[] = $destinatarios;
    
        return $this;
    }

    /**
     * Remove destinatarios
     *
     * @param \Colegio\GrupoBundle\Entity\Grupo $destinatarios
     */
    public function removeDestinatario(\Colegio\GrupoBundle\Entity\Grupo $destinatarios)
    {
        $this->destinatarios->removeElement($destinatarios);
    }

    /**
     * Get destinatarios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDestinatarios()
    {
        return $this->destinatarios;
    }

    /**
     * Set colegio
     *
     * @param \Colegio\AdminBundle\Entity\Colegio $colegio
     * @return Circulares
     */
    public function setColegio(\Colegio\AdminBundle\Entity\Colegio $colegio = null)
    {
        $this->colegio = $colegio;
    
        return $this;
    }

    /**
     * Get colegio
     *
     * @return \Colegio\AdminBundle\Entity\Colegio 
     */
    public function getColegio()
    {
        return $this->colegio;
    }

    /**
     * Set docente
     *
     * @param \Colegio\DocenteBundle\Entity\Docente $docente
     * @return Circulares
     */
    public function setDocente(\Colegio\DocenteBundle\Entity\Docente $docente = null)
    {
        $this->docente = $docente;
    
        return $this;
    }

    /**
     * Get docente
     *
     * @return \Colegio\DocenteBundle\Entity\Docente 
     */
    public function getDocente()
    {
        return $this->docente;
    }

    /**
     * Set periodoEscolar
     *
     * @param \Colegio\AdminBundle\Entity\PeriodoEscolar $periodoEscolar
     * @return Circulares
     */
    public function setPeriodoEscolar(\Colegio\AdminBundle\Entity\PeriodoEscolar $periodoEscolar = null)
    {
        $this->periodoEscolar = $periodoEscolar;
    
        return $this;
    }

    /**
     * Get periodoEscolar
     *
     * @return \Colegio\AdminBundle\Entity\PeriodoEscolar 
     */
    public function getPeriodoEscolar()
    {
        return $this->periodoEscolar;
    }
}