<?php

namespace Colegio\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PeriodoEscolar
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\AdminBundle\Entity\PeriodoEscolarRepository")
 */
class PeriodoEscolar
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaInicio", type="datetime")
     */
    private $fechaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaFin", type="datetime")
     */
    private $fechaFin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Colegio")
     */
    private $colegio;

    /**
     * @ORM\OneToMany(targetEntity="Colegio\AdminBundle\Entity\Parametros", mappedBy="periodoEscolar", cascade={"persist","remove"})
     * @Assert\Valid()    
     */
    private $parametros;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return PeriodoEscolar
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return PeriodoEscolar
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    
        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return PeriodoEscolar
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    
        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return PeriodoEscolar
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

 /**
     * Set colegio
     *
     * @param string $colegio
     * @return PeriodoEscolar
     */
    public function setColegio(\Colegio\AdminBundle\Entity\Colegio $colegio)
    {
        $this->colegio = $colegio;
    
        return $this;
    }

    /**
     * Get colegio
     *
     * @return \Colegio\AdminBundle\Entity\Colegio 
     */
    public function getColegio()
    {
        return $this->colegio;
    }

    public function __toString()
    {
        return $this->getDescripcion();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->parametros = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add parametros
     *
     * @param \Colegio\AdminBundle\Entity\Parametros $parametros
     * @return PeriodoEscolar
     */
    public function addParametro(\Colegio\AdminBundle\Entity\Parametros $parametros)
    {
        $this->parametros[] = $parametros;
    
        return $this;
    }

    /**
     * Remove parametros
     *
     * @param \Colegio\AdminBundle\Entity\Parametros $parametros
     */
    public function removeParametro(\Colegio\AdminBundle\Entity\Parametros $parametros)
    {
        $this->parametros->removeElement($parametros);
    }

    /**
     * Get parametros
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getParametros()
    {
        return $this->parametros;
    }
}