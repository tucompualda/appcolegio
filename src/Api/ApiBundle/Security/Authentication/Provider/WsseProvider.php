<?php

namespace Api\ApiBundle\Security\Authentication\Provider;

use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\NonceExpiredException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Api\ApiBundle\Security\Authentication\Token\WsseUserToken;

class WsseProvider implements AuthenticationProviderInterface
{
    private $userProvider;
    private $cacheDir;

    public function __construct(UserProviderInterface $userProvider, $cacheDir)
    {
        $this->userProvider = $userProvider;
        $this->cacheDir     = $cacheDir;
    }

    public function authenticate(TokenInterface $token)
    {
        $user = $this->userProvider->loadUserByUsername($token->getUsername());
        if(!$user){
            throw new AuthenticationException("Bad credentials... Did you forgot your username ?");
        }
        if ($user && $this->validateDigest($token->digest, $token->nonce, $token->created, $user->getPassword())) {
            $authenticatedToken = new WsseUserToken($user->getRoles());
            $authenticatedToken->setUser($user);

            return $authenticatedToken;
        }
       
       
        throw new AuthenticationException('The WSSE authentication failed my friend POR DIOS SANTOoo ');
    }

    protected function validateDigest($digest, $nonce, $created, $secret)
    {
        // Verifica que el momento de la creación no está en el futuro
        if (strtotime($created) > time()) {
                return false;
        }

        // la marca de tiempo caduca después de 5 minutos
        if (time() - strtotime($created) > 300) {
                return false;
        }

        // Valida que $nonce es única en 5 minutos
        if (file_exists($this->cacheDir.'/'.$nonce) && file_get_contents($this->cacheDir.'/'.$nonce) + 300 > time()) {
            throw new NonceExpiredException('Previously used nonce detected');
        }
        file_put_contents($this->cacheDir.'/'.$nonce, time());

        // Valida secreto
        $expected = base64_encode(sha1(base64_decode($nonce).$created.$secret, true));
        //$encoder = $this->get('security.encoder_factory')->getEncoder($user);
        //$passwordCodificado = $encoder->encodePassword($user->getPassword(),$user->getSalt());
       /** $expected = $encoder->encodePassword(
                sprintf(
                '%s%s%s',
                base64_decode($nonce),
                $created,
                $secret
                ),
                ""
        );**/
        return $digest === $expected;
    }

    public function supports(TokenInterface $token)
    {
        return $token instanceof WsseUserToken;
    }
}