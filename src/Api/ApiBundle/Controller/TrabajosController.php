<?php

namespace Api\ApiBundle\Controller;

use Colegio\DocenteBundle\Entity\Trabajos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class TrabajosController extends Controller
{
    
     /**
     * 
     * @return array
     * @View() 
     */
    public function getAccesoAction() 
    {
       $response = new JsonResponse("Bienvenidos");
       return $response;
    }

  /**
     * 
     * @return array
     * @View() 
     */
    public function getTrabajosAction() 
    {
        //$trabajos = $this->getDoctrine()->getRepository('ColegioDocenteBundle:Trabajos')
        //        ->findAll();
        $em = $this->getDoctrine()->getManager();
        $trabajosQuery = $em->createQuery('SELECT t.id,t.detalle FROM ColegioDocenteBundle:Trabajos t
             WHERE t.estado = true');
        $trabajos = $trabajosQuery->getResult();
        return array('trabajos'=>$trabajos);
    }

     /**
     * 
     * @return array
     * @View() 
     */
    public function getTrabajoAction($trabajo) 
    {
        $em = $this->getDoctrine()->getManager();
        $trabajosQuery = $em->createQuery('SELECT t.id,t.detalle FROM ColegioDocenteBundle:Trabajos t
             WHERE t.estado = true AND t.id = :trabajo');
        $trabajosQuery->setParameter('trabajo',$trabajo);
        $trabajos = $trabajosQuery->getResult();
        return array('trabajos'=>$trabajos);
    }

    /**
     * 
     * @param Trabajos $trabajo
     * @return array
     * @View()
     * @ParamConverter("trabajo", class="ColegioDocenteBundle:Trabajos")
     */
    public function getTrabajilloAction(Trabajos $trabajo)
    {
        return array('trabajo'=>$trabajo);
    }
}
