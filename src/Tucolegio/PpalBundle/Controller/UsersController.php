<?php

namespace Tucolegio\PpalBundle\Controller;

use Colegio\EstudianteBundle\Entity\Estudiante;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


class UsersController extends Controller
{
    
   /**
     * @return array
     * @View() 
     */
    public function getUserAction($username) 
    {
        //$trabajos = $this->getDoctrine()->getRepository('ColegioDocenteBundle:Trabajos')
        //        ->findAll();
        $em = $this->getDoctrine()->getManager();
        $estudianteQuery = $em->createQuery('SELECT e.primerNombre,e.primerApellido,e.salt FROM ColegioEstudianteBundle:Estudiante e
             WHERE e.email = :username');
        $estudianteQuery->setParameter('username',$username);
        $estudiante = $estudianteQuery->getArrayResult();
        return array('estudiante'=>$estudiante);
    }
}