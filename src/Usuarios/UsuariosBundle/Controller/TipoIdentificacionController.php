<?php

namespace Usuarios\UsuariosBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Usuarios\UsuariosBundle\Entity\TipoIdentificacion;
use Usuarios\UsuariosBundle\Form\TipoIdentificacionType;

/**
 * TipoIdentificacion controller.
 *
 * @Route("/tipoidentificacion")
 */
class TipoIdentificacionController extends Controller
{

    /**
     * Lists all TipoIdentificacion entities.
     *
     * @Route("/", name="tipoidentificacion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UsuariosUsuariosBundle:TipoIdentificacion')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new TipoIdentificacion entity.
     *
     * @Route("/", name="tipoidentificacion_create")
     * @Method("POST")
     * @Template("UsuariosUsuariosBundle:TipoIdentificacion:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new TipoIdentificacion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipoidentificacion_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a TipoIdentificacion entity.
    *
    * @param TipoIdentificacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(TipoIdentificacion $entity)
    {
        $form = $this->createForm(new TipoIdentificacionType(), $entity, array(
            'action' => $this->generateUrl('tipoidentificacion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoIdentificacion entity.
     *
     * @Route("/new", name="tipoidentificacion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new TipoIdentificacion();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a TipoIdentificacion entity.
     *
     * @Route("/{id}", name="tipoidentificacion_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UsuariosUsuariosBundle:TipoIdentificacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoIdentificacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing TipoIdentificacion entity.
     *
     * @Route("/{id}/edit", name="tipoidentificacion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UsuariosUsuariosBundle:TipoIdentificacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoIdentificacion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a TipoIdentificacion entity.
    *
    * @param TipoIdentificacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoIdentificacion $entity)
    {
        $form = $this->createForm(new TipoIdentificacionType(), $entity, array(
            'action' => $this->generateUrl('tipoidentificacion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing TipoIdentificacion entity.
     *
     * @Route("/{id}", name="tipoidentificacion_update")
     * @Method("PUT")
     * @Template("UsuariosUsuariosBundle:TipoIdentificacion:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UsuariosUsuariosBundle:TipoIdentificacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoIdentificacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tipoidentificacion_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a TipoIdentificacion entity.
     *
     * @Route("/{id}", name="tipoidentificacion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('UsuariosUsuariosBundle:TipoIdentificacion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoIdentificacion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipoidentificacion'));
    }

    /**
     * Creates a form to delete a TipoIdentificacion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipoidentificacion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
