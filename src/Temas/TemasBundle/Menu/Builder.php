<?php

namespace Temas\TemasBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function superAdminMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'mainnav');
       
        //$menu->setLabelAttribute('class', 'span');

        $menu->addChild('Panel', array('route' => 'usuarios_usuarios_homepage'))->setLabel('Panel')
        ->setAttribute('icon','icon-dashboard');
        $menu->addChild('Mensajes', array('route' => 'circulares_admin'))->setLabel('Mensajes')
        ->setAttribute('icon','icon-folder-close');
            
               
        $menu->addChild('Usuarios', array('route' => 'usuarios'))->setAttribute('icon','icon-user');
        $menu->addChild('Tipo Usuario', array('route' => 'tipousuario'))->setAttribute('icon','icon-bar-chart');
        $menu->addChild('Tipo Identificación', array('route' => 'tipoidentificacion'))->setAttribute('icon','icon-list-alt');
        $menu->addChild('Normas', array('uri' => '#'))->setAttribute('class','dropdown')->setAttribute('icon','icon-list-alt')->setLinkAttribute('class','dropdown-toggle')->setLinkAttribute('data-toggle','dropdown');
        $menu['Normas']->setChildrenAttribute('class', 'dropdown-menu');
        $menu['Normas']->addChild('Norma', array('route'=>'norma'));
        $menu['Normas']->addChild('Item', array('route'=>'item'));

        
        $menu->addChild('Colegio',array('uri'=>'#'))->setAttribute('class','dropdown')->setAttribute('icon','icon-folder-open')->setLinkAttribute('class','dropdown-toggle')->setLinkAttribute('data-toggle','dropdown');
        $menu['Colegio']->setChildrenAttribute('class', 'dropdown-menu');
        $menu['Colegio']->addChild('Crear', array('route'=>'colegio'));
        $menu['Colegio']->addChild('Tipo Colegio', array('route'=>'tipocolegio'));
        $menu['Colegio']->addChild('Niveles', array('route'=>'nivel'));

        $menu->addChild('SocioEconómico',array('uri'=>'#'))->setAttribute('class', 'dropdown')->setAttribute('icon','icon-folder-open')->setLinkAttribute('class','dropdown-toggle')->setLinkAttribute('data-toggle','dropdown');
        $menu['SocioEconómico']->setChildrenAttribute('class', 'dropdown-menu');
        $menu['SocioEconómico']->addChild('Estrato', array('route'=>'estrato'));
        $menu['SocioEconómico']->addChild('Departamento', array('route'=>'departamento'));
        $menu['SocioEconómico']->addChild('Localidad', array('route'=>'localidad'));
        $menu['SocioEconómico']->addChild('Municipio', array('route'=>'municipio'));
        $menu->addChild('Tipo calificación', array('route' => 'tipocalificacion'))->setAttribute('icon','icon-bar-chart');

        return $menu;
    }

    public function colegiosMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'mainnav');
       
        //$menu->setLabelAttribute('class', 'span');

        $menu->addChild('Panel', array('route' => 'colegio_admin_homepage'))->setLabel('Panel')
        ->setAttribute('icon','icon-dashboard')->setAttribute('id','tour3');
        $menu->addChild('Circulares', array('route' => 'circulares'))->setAttribute('icon','shortcut-icon icon-file');
        $menu->addChild('Grupos', array('route' => 'grupo_showbysede'))->setAttribute('icon','icon-list-alt');
        $menu->addChild('Boletines', array('route' => 'boletin'))->setAttribute('icon','icon-bar-chart');
        $menu->addChild('Planillas', array('route' => 'colegio_planillas'))->setAttribute('icon','fa fa-newspaper-o');
        $menu->addChild('Estadística', array('route' => 'colegio_admin_carga'))->setAttribute('icon','fa fa-bell-o');
        $menu->addChild('Estudiantes', array('route' => 'estudiante'))->setAttribute('icon','icon-user'); 
        $menu->addChild('Docentes', array('route' => 'docente'))->setAttribute('icon','icon-pencil');
        $menu->addChild('Materias', array('route' => 'asignatura'))->setAttribute('icon','icon-book')->setAttribute('id','tour4');      
        $menu->addChild('Notas', array('route' => 'nota'))->setAttribute('icon','shortcut-icon icon-file');
        $menu->addChild('Tareas', array('route' => 'colegio_admin_tareas'))->setAttribute('icon','fa fa-tasks');
        $menu->addChild('Mensajes', array('route' => 'mensajes'))->setAttribute('icon','shortcut-icon icon-file');
        return $menu;
    }

    public function ppalMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav');       
        
        $menu->addChild('Inicio', array('route' => 'tucolegio_ppal_homepage'));
        $menu->addChild('Quiénes somos', array('route' => 'tucolegio_conocenos'));
        $menu->addChild('Contáctanos', array('route' => 'tucolegio_contacto'));
        $menu->addChild('Usuarios', array('uri'=>'#'))->setAttribute('class', 'dropdown')->setLinkAttribute('class','dropdown-toggle')->setLinkAttribute('data-toggle','dropdown');
        $menu['Usuarios']->setChildrenAttribute('class', 'dropdown-menu');
        $menu['Usuarios']->addChild('Estudiantes', array('route'=>'colegio_estudiante_homepage'));
        $menu['Usuarios']->addChild('Docentes', array('route'=>'colegio_docente_homepage'));
        $menu['Usuarios']->addChild('Administradores', array('route'=>'colegio_admin_homepage'));
        return $menu;

    }

    public function docenteMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav side-nav');       
        
        $menu->addChild('Panel', array('route' => 'colegio_docente_homepage'))->setAttribute('icon','fa fa-dashboard');
        $menu->addChild('Circulares', array('route' => 'circularesDocente'))->setAttribute('icon','fa fa-plus-square-o');
        
        $menu->addChild('Mis Cursos', array('route' => 'ColegioDocente_misgrupos'))->setAttribute('icon','fa fa-font');        
        $menu->addChild('Tareas', array('route' => 'trabajos'))->setAttribute('icon','fa fa-table');
        
        $menu->addChild('Asignaturas', array('route' => 'ColegioDocente_grupos'))->setAttribute('icon','fa fa-font');
        $menu->addChild('Planillas', array('route' => 'ColegioDocenteBundle_planillas'))->setAttribute('icon','fa fa-font');
        $menu->addChild('Chat (BETA!)', array('route' => 'service_docente_chat'))->setAttribute('icon','fa fa-comments-o');
        $menu->addChild('Recibidos', array('route' => 'ColegioDocenteBundle_mensajes'))->setAttribute('icon','fa fa-font');
        $menu->addChild('Enviados', array('route' => 'ColegioDocenteBundle_mensajesEnviados'))->setAttribute('icon','fa fa-font');
        $menu->addChild('Soporte', array('route' => 'ColegioDocenteBundle_soporte'))->setAttribute('icon','fa fa-question');
        return $menu;
    }

    public function estudianteMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav side-nav');       
        
        $menu->addChild('Panel', array('route' => 'colegio_estudiante_homepage'))->setAttribute('icon','fa fa-dashboard');
        $menu->addChild('Asignaturas', array('route' => 'colegio_estudiante_asignaturas'))->setAttribute('icon','fa fa-font');        

        $menu->addChild('Mi Notas', array('route' => 'colegio_estudiante_notas'))->setAttribute('icon','fa fa-font');        
        $menu->addChild('Mis Tareas',array('uri'=>'javascript:;'))->setAttribute('icon','fa fa-fw fa-arrows-v')->setLinkAttribute('data-target','#demo')->setLinkAttribute('data-toggle','collapse');
        $menu['Mis Tareas']->setChildrenAttribute('id','demo')->setChildrenAttribute('class', '');
        $menu['Mis Tareas']->addChild('Tareas Periodo', array('route'=>'colegio_estudiante_tareas_nuevas'))->setAttribute('icon','fa fa-bookmark-o');
        $menu['Mis Tareas']->addChild('Tareas Calificadas', array('route'=>'colegio_estudiante_tareas'))->setAttribute('icon','fa fa-check-circle-o');
        
        $menu->addChild('Circulares', array('route' => 'colegio_estudiante_circulares'))->setAttribute('icon','fa fa-edit');
        $menu->addChild('Matriculas', array('route' => 'colegio_estudiante_matriculas'))->setAttribute('icon','fa fa-font');
        $menu->addChild('Juegos', array('route' => 'colegio_estudiante_juegos'))->setAttribute('icon','fa fa-gamepad');
        $menu->addChild('Chat (BETA!)', array('route' => 'service_estudiante_chat'))->setAttribute('icon','fa fa-comments-o');
        $menu->addChild('Soporte', array('route' => 'ColegioEstudianteBundle_soporte'))->setAttribute('icon','fa fa-question');
        
       //  $menu->addChild('Otros', array('uri' => '#'))->setAttribute('icon','fa fa-desktop');
        return $menu;
    }

}

