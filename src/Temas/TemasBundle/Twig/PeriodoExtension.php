<?php

namespace Temas\TemasBundle\Twig;
use Symfony\Component\Security\Core\SecurityContext;
use Colegio\AdminBundle\Entity\Colegio;
use Usuarios\UsuariosBundle\Entity\Usuarios;
use Symfony\Component\HttpKernel\KernelInterface;


class PeriodoExtension extends \Twig_Extension
{
	private   $context;
	protected $em;
	
        public function __construct($entityManager, $container, SecurityContext $context) 
       {
             $this->em=$entityManager;
            $this->container=$container;
            $this->context = $context;  //,SecurityContext $context		

            }
	
        public function getGlobals()
        {
		$estado = $this->context->getToken()->getUser();
		if (is_object($usuarioActivo = $this->context->getToken()->getUser())){
				
			$idColegio = $usuarioActivo->getIdColegio();
			}else {
				$idColegio = "%";
			}

		
				
		$hoy = new \DateTime('now');
		$periodoactual = $this->em->createQuery(
			'SELECT p,pe FROM ColegioBoletinBundle:Periodo p 
			INNER JOIN p.periodoEscolar pe
			WHERE p.fechaInicio < :actual  AND p.fechaFin >= :actual AND p.colegio = :colegio'
		)->setParameters(array(
			'actual' => $hoy,
			'colegio'=> $idColegio,
		))->setMaxResults(1);
		
		$periodo = $periodoactual->getResult();
		
        $parametro = $this->em->createQuery(
           'SELECT g,h 
           	FROM ColegioAdminBundle:Parametros g 
           	JOIN g.periodoEscolar h 
           	WHERE g.colegio = :idColegio AND h.estado = true'
        )->setParameter('idColegio',$idColegio)->setMaxResults(1);
        $dato = $parametro->getResult();

        return array(
            'text'    => $hoy,
            'periodos'=> $periodo,
            'idcole'  => $idColegio,
            'dato'    => $dato,
        );
    }
	
	
     public function getName()
    {
        return 'periodo_extension';
    }
}
